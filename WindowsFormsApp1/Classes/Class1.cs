﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class NewEvent : Form
    {
        private Event myNewEvent;
        private List<TimeBlock> unavailableTimes;
        private DateTime currentStartTime;
        private bool boundryFlag;

        public NewEvent(DateTime initialDate)
        {
            InitializeComponent();
            unavailableTimes = new List<TimeBlock>();
            myNewEvent = new Event();
            //eStartTime.Value = new DateTime(initialDate.Year, initialDate.Month, initialDate.Day, 8, 0, 0);
            currentStartTime = new DateTime(initialDate.Year, initialDate.Month, initialDate.Day, 8, 0, 0);
            eStartTime.Value = currentStartTime;
            boundryFlag = false;
            buildBothContactLists();
            eDate.Value = initialDate;
        }

        private void addContactToList(ListView list, Contact c)
        {
            ListViewItem li = new ListViewItem();
            li.Tag = c;
            li.Text = c.getFirstName();
            li.SubItems.Add(c.getLastname());
            list.Items.Add(li);
        }

        private void buildUnatendingContactList()
        {
            eNotAttendingList.Items.Clear();
            foreach (Contact globalContact in Controller.getAllContacts())
            {
                foreach (Contact eventContact in myNewEvent.getMyParticipants())
                    if (globalContact == eventContact)
                        goto next;

                addContactToList(eNotAttendingList, globalContact);
                next:;
            }
        }

        private void buildAttendingContactList()
        {
            eAttendeesList.Items.Clear();
            foreach (Contact c in myNewEvent.getMyParticipants())
            {
                addContactToList(eAttendeesList, c);
            }
        }

        private void buildBothContactLists()
        {
            buildAttendingContactList();
            buildUnatendingContactList();
        }

        private void toggleParticipant_Click(object sender, EventArgs e)
        {
            ListView addTo = eNotAttendingList;
            ListView removeFrom = eAttendeesList;

            if (sender == eAddParticipant)
            {
                addTo = eAttendeesList;
                removeFrom = eNotAttendingList;
            }

            foreach (ListViewItem item in removeFrom.SelectedItems)
            {
                removeFrom.Items.Remove(item);
                addTo.Items.Add(item);
            }

            generateUnavailableTimes();
            if (validDuration())
                setValidStartTime();
        }

        private void generateUnavailableTimes()
        {
            unavailableTimes.Clear();
            List<Contact> tentativelyAttendingContacts = new List<Contact>();
            foreach (ListViewItem item in eAttendeesList.Items)
            {
                tentativelyAttendingContacts.Add(((Contact)item.Tag));
            }
            foreach (Contact activeContact in tentativelyAttendingContacts)
            {
                foreach (Event activeEvent in activeContact.getEventsOn(eDate.Value))
                {
                    List<TimeBlock> conflictTrail = new List<TimeBlock>();
                    foreach (TimeBlock tb in unavailableTimes)
                        if (tb.checkConflict(activeEvent.getTimeBlock()))
                        {
                            conflictTrail.Add(tb);
                            conflictTrail.Add(activeEvent.getTimeBlock());
                        }
                    if (conflictTrail.Count == 0)
                        unavailableTimes.Add(activeEvent.getTimeBlock());
                    else
                        resolveConflictTrail(conflictTrail);
                }
            }
            unavailableTimes.Sort((x, y) => x.getStart().CompareTo(y.getStart()));
        }

        private TimeBlock getNextConflictingTimeBlock(TimeBlock desiredTime)
        {
            foreach (TimeBlock unavailableTime in unavailableTimes)
                if (desiredTime.checkConflict(unavailableTime))
                    return unavailableTime;
            return null;
        }

        private TimeBlock getPreviousConflictingTimeBlock(TimeBlock desiredTime)
        {
            for (int i = unavailableTimes.Count - 1; i >= 0; i--)
                if (desiredTime.checkConflict(unavailableTimes[i]))
                    return unavailableTimes[i];
            return null;
        }

        private void setValidStartTime()
        {
            DateTime start = eStartTime.Value;
            TimeBlock desiredTimeBlock = calculateDesiredTimeBlock(start, true, false);

            foreach (TimeBlock tb in unavailableTimes)
            {
                if (desiredTimeBlock.checkConflict(tb))
                {
                    currentStartTime = new DateTime(0);
                    DateTime dayOfEvent = eDate.Value.Date;
                    DateTime initStartTime = new DateTime(dayOfEvent.Year, dayOfEvent.Month, dayOfEvent.Day, 8, 0, 0);
                    eStartTime.Value = initStartTime;
                    return;
                }
            }

            //current time is valid
        }

        private void startTimeChanged(object sender, EventArgs e)
        {
            bool increased;
            boundryFlag = false;
            if (currentStartTime.Ticks == 0)
                increased = true;
            else
                increased = eStartTime.Value >= currentStartTime;

            eStartTime.ValueChanged -= startTimeChanged;
            DateTime desiredStartTime;

            if (increased)
                desiredStartTime = startTimeIncreased();
            else
                desiredStartTime = startTimeDecreased();

            eStartTime.Value = desiredStartTime;
            currentStartTime = desiredStartTime;
            eStartTime.ValueChanged += startTimeChanged;
        }

        private DateTime startTimeIncreased()
        {
            DateTime desiredStartTime = eStartTime.Value;
            TimeBlock desiredTimeBlock = calculateDesiredTimeBlock(desiredStartTime, true, false);
            TimeBlock conflict;
            conflict = getNextConflictingTimeBlock(desiredTimeBlock);

            while (conflict != null)
            {
                desiredStartTime = conflict.getEnd().AddMinutes(1);
                desiredTimeBlock = calculateDesiredTimeBlock(desiredStartTime, true, false);
                conflict = getNextConflictingTimeBlock(desiredTimeBlock);
            }
            if (!desiredTimeBlock.startDateIsEndDate() || desiredTimeBlock.getStart().Date != eDate.Value.Date)
            {
                if (boundryFlag)
                {
                    //no possible times for event
                    eStartTime.Visible = false;
                    return currentStartTime;
                }
                Console.WriteLine("Reverse");
                boundryFlag = true;
                return startTimeDecreased();
                //return currentStartTime;
            }
            return desiredStartTime;
        }

        private DateTime startTimeDecreased()
        {
            DateTime desiredStartTime = eStartTime.Value;
            TimeBlock desiredTimeBlock = calculateDesiredTimeBlock(desiredStartTime, false, false);
            TimeBlock conflict;
            conflict = getPreviousConflictingTimeBlock(desiredTimeBlock);

            while (conflict != null)
            {
                desiredStartTime = conflict.getStart().AddMinutes(-1);
                desiredTimeBlock = calculateDesiredTimeBlock(desiredStartTime, false, true);
                conflict = getPreviousConflictingTimeBlock(desiredTimeBlock);
            }
            if (!desiredTimeBlock.startDateIsEndDate() || desiredTimeBlock.getStart().Date != eDate.Value.Date)
            {
                if (boundryFlag)
                {
                    //no possible times for event
                    eStartTime.Visible = false;
                    return currentStartTime;
                }
                Console.WriteLine("Reverse");
                boundryFlag = true;
                return startTimeIncreased();
                //return currentStartTime;
            }
            return desiredStartTime;
        }

        private void resolveConflictTrail(List<TimeBlock> conflictTrail)
        {
            DateTime start = conflictTrail[0].getStart();
            DateTime end = conflictTrail[0].getEnd();
            foreach (TimeBlock tb in conflictTrail)
            {
                if (tb.getStart() < start)
                    start = tb.getStart();
                if (tb.getEnd() > end)
                    end = tb.getEnd();
                unavailableTimes.Remove(tb);
            }
            unavailableTimes.Add(new TimeBlock(start, end));
        }

        private TimeBlock calculateDesiredTimeBlock(DateTime selectedTime, bool startToEnd, bool conflictFlag)
        //conflict flag is true when the time is being decreased, and there is a timeblock conflict, 
        //switch the desired start time to be the end time.
        {
            if (startToEnd || !conflictFlag)
            {
                DateTime endTime = new DateTime(selectedTime.Ticks);
                endTime = endTime.AddHours(getDurationHours());
                endTime = endTime.AddMinutes(getDurationMin());
                return new TimeBlock(selectedTime, endTime);
            }
            else
            {
                DateTime startTime = new DateTime(selectedTime.Ticks);
                startTime = startTime.AddHours(-getDurationHours());
                startTime = startTime.AddMinutes(-getDurationMin());
                return new TimeBlock(startTime, selectedTime);
            }
        }

        private int getDurationMin()
        {
            int myDurationMin = 0;
            if (Int32.TryParse(eDurationMinute.Text, out myDurationMin))
            {

            }
            return myDurationMin;
        }

        private int getDurationHours()
        {
            int myDurationHours;
            if (Int32.TryParse(eDurationHour.Text, out myDurationHours))
            {

            }
            return myDurationHours;
        }

        private void durationChanged(object sender, EventArgs e)
        {
            if (validDuration())
            {
                eStartTime.Visible = true;
                eStartTimeLabel.Visible = true;
                setValidStartTime();
                return;
            }
            eStartTime.Visible = false;
            eStartTimeLabel.Visible = false;
        }

        private void dateChanged(object sender, EventArgs e)
        {
            if (validDuration())
                setValidStartTime();
        }

        private bool validDuration()
        {
            if (eDurationHour.Text != "" && eDurationMinute.Text != "")
                if (getDurationHours() > 0 || getDurationMin() > 0)
                {
                    return true;
                }
            return false;
        }
    }
}
