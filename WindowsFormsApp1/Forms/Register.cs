﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Text.RegularExpressions;

namespace WindowsFormsApp1
{
    public partial class Register : Form
    {

        public Register()
        {
            InitializeComponent();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        // The save button that saves the new user information into the database
        private void submitButton_Click(object sender, EventArgs e)
        {
            newContactCreated(); //Function does all the checking and verifacation before going into the database
        }

        private void Register_Load(object sender, EventArgs e)
        {

        }

        //checks to see if the new User picks an username that already exist in the database or not. 
        private bool checkForUserName(String name)
        {
            String checkUserName = null; //a variable to store a username exist or not when the query runs. 
            bool flag = false;
            string connStr;
            connStr = "server=csdatabase.eku.edu;user=tim_hopkins8;database=Hopkins;port=3306;password=Hopkins8;";
            MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection(connStr);

            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                string sql = "SELECT userName FROM groupaccount WHERE userName = @name";
                MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@name", name);
                MySqlDataReader myReader = cmd.ExecuteReader();
                if (myReader.Read())
                {
                    checkUserName = myReader["userName"].ToString();
                }
                myReader.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
            Console.WriteLine("Done.");
            if (String.IsNullOrEmpty(checkUserName))
            {
                flag = true;
            }
            return flag;
        }

        //Function that checks to make sure all the fields are filled in properly before inserting into the dat
        private void newContactCreated()
        {
            errorProvider1.Clear();
            bool flag = true;
            String fName = fNameField.Text;
            String lName = lNameField.Text;
            String email = emailField.Text;
            String phone = phoneField.Text;
            String userName = userNameField.Text;
            String password = passwordField.Text;

            if ((String.IsNullOrEmpty(userName)))
            {
                errorProvider1.SetError(userNameField, "Enter a User name");
                flag = false;
            }
            if (checkForUserName(userName) == false)
            {
                errorProvider1.SetError(userNameField, "Username already exist, Pick another username");
                flag = false;
            }
            if (!IsValidPassword(password))
            {
                errorProvider1.SetError(passwordField, "your password needs to be at least (8 character, 1 uppercase, 1 Special character, and 1 digit)");
                flag = false;
            }
            if ((String.IsNullOrEmpty(fName)))
            {
                errorProvider1.SetError(fNameField, "Enter your First Name");
                flag = false;
            }
            if ((String.IsNullOrEmpty(lName)))
            {
                errorProvider1.SetError(lNameField, "Enter your Last Name");
                flag = false;
            }
            if ((String.IsNullOrEmpty(phone)))
            {
                errorProvider1.SetError(phoneField, "Enter a Phone Number");
                flag = false;
            }
            if (!(String.IsNullOrEmpty(phone)))
            {
                if (phone.Length > 12)
                {
                    errorProvider1.SetError(phoneField, "Invalid phone number, Enter 7 or 10 digit number");
                    flag = false;
                }
                else if (phone.Length >= 10 && phone.Length <= 12 && !IsValidNumber10(phone))
                {
                    errorProvider1.SetError(phoneField, "Invalid phone number");
                    flag = false;
                }
                else if (phone.Length <= 9 && !IsValidNumber7(phone))
                {
                    errorProvider1.SetError(phoneField, "Invalid phone number");
                    flag = false;
                }
            }
            if ((String.IsNullOrEmpty(email)))
            {
                errorProvider1.SetError(emailField, "Enter an email");
                flag = false;
            }
            if (!(String.IsNullOrEmpty(email)))
            {
                if (!IsValidEmail(email))
                {
                    errorProvider1.SetError(emailField, "Invalid Email Address");
                    flag = false;
                }
            }
            if (flag == true)
            {
                insertNewRegistrant(userName, password, fName, lName, getPhoneNumberFormat(), email);
                Close();
            }
        }

        //inserts the new registrant into the database
        private void insertNewRegistrant(String username, String password, String first, String last, String phone, String email)
        {
            string connStr = "server=csdatabase.eku.edu;user=tim_hopkins8;database=Hopkins;port=3306;password=Hopkins8;";
            MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                string sql;

                sql = "INSERT INTO groupcontact(firstName, lastName, phoneNumber, emailAddress) VALUES (@fname, @lname, @number, @email)";
                MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@fname", first);
                cmd.Parameters.AddWithValue("@lname", last);
                cmd.Parameters.AddWithValue("@number", phone);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.ExecuteNonQuery();
                String sqlNest = "(SELECT max(employeeID) FROM groupcontact)";
                sql = "INSERT INTO groupaccount(employeeID, userName, password) VALUES (" + sqlNest + ", @user, @password)";
                MySql.Data.MySqlClient.MySqlCommand cmd1 = new MySql.Data.MySqlClient.MySqlCommand(sql, conn);
                cmd1.Parameters.AddWithValue("@user", username);
                cmd1.Parameters.AddWithValue("@password", password);
                cmd1.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
            Console.WriteLine("Done.");
        }

        //Checking to see if password meets the requirement for >= 8 char, 1 uppercase, 1 special char, 1 #
        bool IsValidPassword(string str)
        {
            return Regex.IsMatch(str, @"(?=.*\d)(?=.*[A-Z])(?=.*\W)\S{8,}$");
        }
        //checks to see if user enter a valid 10 digit number
        bool IsValidNumber10(string str)
        {
            return Regex.IsMatch(str, @"^\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$");
        }
        //checks to see if user enters a valid 7 digit number
        bool IsValidNumber7(string str)
        {
            return Regex.IsMatch(str, @"^\d{3}[\s\-]?\d{4}$");
        }

        //checks to see if user enters a valid email address
        bool IsValidEmail(string str)
        {
            return Regex.IsMatch(str, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        }

        //formats the phone numbers so the output will always have '-' between the numbers
        private string getPhoneNumberFormat()
        {
            String value = "";
            value = phoneField.Text;
            value = new System.Text.RegularExpressions.Regex(@"\D").Replace(value, string.Empty);
            if (value.Length == 7)
                return Convert.ToInt64(value).ToString("###-####");
            if (value.Length == 10)
                return Convert.ToInt64(value).ToString("###-###-####");
            if (value.Length > 10)
                return Convert.ToInt64(value)
                    .ToString("###-###-#### " + new String('#', (value.Length - 10)));
            return value;
        }

    }
}

