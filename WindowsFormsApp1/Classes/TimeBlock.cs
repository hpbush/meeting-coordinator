﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class TimeBlock
    {
        private DateTime start;
        private DateTime end;

        public static TimeBlock createTimeBlock(DateTime start, DateTime end)
        {
            if (start.Date == end.Date)
                return new TimeBlock(start, end);
            return null;
        }

        private TimeBlock(DateTime start, DateTime end)
        {
            this.start = start;
            this.end = end;
        }

        public DateTime getStart()
        {
            return start;
        }

        public DateTime getEnd()
        {
            return end;
        }

        public void setStart(DateTime start)
        {
            this.start = start;
        }

        public void setEnd(DateTime end)
        {
            this.end = end;
        }

        public bool checkConflict(TimeBlock tb) 
        {
            if (tb.getStart() > end || tb.getEnd() < start)
                return false;
            return true;
        }
    }
}
