﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Room: Participant
    {
        private int roomID;
        private String roomName;

        public Room(int num, String roomName)
        {
            roomID = num;
            this.roomName = roomName;
        }

        public void setroomID(int room)
        {
            roomID = room;
        }

        public int getroomID()
        {
            return roomID;
        }

        public void setRoomName(String name)
        {
            roomName = name;
        }

        public String getRoomName()
        {
            return roomName;
        }

        public override string ToString()
        {
            return roomName;
        }
    }
}
