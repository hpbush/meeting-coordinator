﻿namespace WindowsFormsApp1
{
    partial class CalendarHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.eventList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.newEventButton = new System.Windows.Forms.Button();
            this.monthViewButton = new System.Windows.Forms.Button();
            this.logOutButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // calendar
            // 
            this.calendar.Location = new System.Drawing.Point(18, 18);
            this.calendar.MaxSelectionCount = 1;
            this.calendar.Name = "calendar";
            this.calendar.TabIndex = 0;
            this.calendar.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.dateChanged);
            // 
            // eventList
            // 
            this.eventList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.eventList.FullRowSelect = true;
            this.eventList.Location = new System.Drawing.Point(559, 18);
            this.eventList.Name = "eventList";
            this.eventList.Size = new System.Drawing.Size(598, 315);
            this.eventList.TabIndex = 1;
            this.eventList.UseCompatibleStateImageBehavior = false;
            this.eventList.View = System.Windows.Forms.View.Details;
            this.eventList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.selectEvent);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Title";
            this.columnHeader1.Width = 219;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Start Time";
            this.columnHeader2.Width = 175;
            // 
            // newEventButton
            // 
            this.newEventButton.Location = new System.Drawing.Point(907, 414);
            this.newEventButton.Name = "newEventButton";
            this.newEventButton.Size = new System.Drawing.Size(250, 81);
            this.newEventButton.TabIndex = 2;
            this.newEventButton.Text = "New Event";
            this.newEventButton.UseVisualStyleBackColor = true;
            this.newEventButton.Click += new System.EventHandler(this.newEventButton_Click);
            // 
            // monthViewButton
            // 
            this.monthViewButton.Location = new System.Drawing.Point(559, 420);
            this.monthViewButton.Name = "monthViewButton";
            this.monthViewButton.Size = new System.Drawing.Size(250, 75);
            this.monthViewButton.TabIndex = 3;
            this.monthViewButton.Text = "Monthly View";
            this.monthViewButton.UseVisualStyleBackColor = true;
            this.monthViewButton.Click += new System.EventHandler(this.viewMonthlyEvents);
            // 
            // logOutButton
            // 
            this.logOutButton.Location = new System.Drawing.Point(912, 567);
            this.logOutButton.Name = "logOutButton";
            this.logOutButton.Size = new System.Drawing.Size(250, 75);
            this.logOutButton.TabIndex = 4;
            this.logOutButton.Text = "Log out";
            this.logOutButton.UseVisualStyleBackColor = true;
            // 
            // CalendarHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1174, 654);
            this.Controls.Add(this.logOutButton);
            this.Controls.Add(this.monthViewButton);
            this.Controls.Add(this.newEventButton);
            this.Controls.Add(this.eventList);
            this.Controls.Add(this.calendar);
            this.Name = "CalendarHome";
            this.Text = "Calendar Home";
            this.Activated += new System.EventHandler(this.printEventList);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MonthCalendar calendar;
        private System.Windows.Forms.ListView eventList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button newEventButton;
        private System.Windows.Forms.Button monthViewButton;
        private System.Windows.Forms.Button logOutButton;
    }
}