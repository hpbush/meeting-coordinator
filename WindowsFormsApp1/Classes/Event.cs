﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Event
    {
        private int eventID;
        private int roomID;
        private String title;
        private TimeBlock timeBlock;
        private String details;
        private List<Participant> myParticipants;

        public Event(String title, TimeBlock timeBlock, String details)
        {
            this.title = title;
            this.timeBlock = timeBlock;
            this.details = details;
            myParticipants = new List<Participant>();
        }
        public Event()
        {
            myParticipants = new List<Participant>();
        }

        public String getTitle()
        {
            return title;
        }

        public TimeBlock getTimeBlock()
        {
            return timeBlock;
        }

        public String getDetails()
        {
            return details;
        }

        public List<Participant> getMyParticipants()
        {
            return myParticipants;
        }

        public void setTitle(String newTitle)
        {
            title = newTitle;
        }

        public void setEventTimeBlock(TimeBlock newTimeBlock)
        {
            timeBlock = newTimeBlock;
        }

        public void setDetails(String newDetails)
        {
            details = newDetails;
        }

        public void addParticipant(Participant p)
        {
            myParticipants.Add(p);
            // make sure to add the event to the participants calendar as well
        }

        public void deleteParticipant(Participant p)
        {
            myParticipants.Remove(p); 
            // make sure to remove the event from the participants calendar 
        }

        public void setEventID(int id)
        {
            eventID = id;
        }

        public int getEventID()
        {
            return eventID;
        }

        public void setRoomID(int id)
        {
            roomID = id;
        }

        public int getRoomID()
        {
            return roomID;
        }
    }
}
