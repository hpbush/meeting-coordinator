﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class MonthlyView : Form
    {
        public MonthlyView(DateTime myMonth)
        {
            InitializeComponent();
            monthLabel.Text = myMonth.ToString("MMMM") + "'s Events";
            printSelectedMonthsEvents(myMonth);
        }

        private void backToHome(object sender, EventArgs e)
        {
            this.Close();
        }

        private void printSelectedMonthsEvents(DateTime myMonth)
        {
            monthEventList.Items.Clear();
            Contact activeContact = Controller.loggedInAccount.getMyContact();
            List<Event> eventsToPrint = activeContact.getEventsIn(myMonth);

            foreach (Event e in eventsToPrint)
            {
                addEventToList(e);
            }
        }

        private void addEventToList(Event e)
        {
            ListViewItem li = new ListViewItem();
            li.Tag = e;
            li.Text = e.getTitle();
            li.SubItems.Add(e.getTimeBlock().getStart().ToString("h:mm tt"));
            monthEventList.Items.Add(li);
        }

        private void monthEventList_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Controller.openForm(new NewEvent(((Event)monthEventList.SelectedItems[0].Tag)), this);
        }
    }
}
