﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public static class Controller
    {
        public static Account loggedInAccount;
        private static List<Contact> allContacts = new List<Contact>();
        private static List<Event> allEvents = new List<Event>();
        private static List<Room> allRooms = new List<Room>();

        public static void openForm(Form newForm, Form oldForm)
        {
            oldForm.Hide();
            newForm.ShowDialog();
            oldForm.Show();
        }

        public static List<Contact> getAllContacts()
        {
            return allContacts;
        }

        public static List<Room> getAllRooms()
        {
            return allRooms;
        }

        public static void addContact(Contact c)
        {
            allContacts.Add(c);
        }

        public static List<Event> getAllEvents()
        {
            return allEvents;
        }

        public static void addEvent(Event e)
        {
            allEvents.Add(e);
        }

        public static bool logUserIn(String username, String password)
        {

            string connStr = "server=csdatabase.eku.edu;user=tim_hopkins8;database=Hopkins;port=3306;password=Hopkins8;";
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                allContacts.Clear();
                allEvents.Clear();
                allRooms.Clear();

                Console.WriteLine("Connecting to MySQL...");
                conn.Open();

                setContactList(conn);
                setEventList(conn);
                setRoomList(conn);

                if (!setActiveAccount(conn, username, password))
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            conn.Close();

            return true;
        }

        private static bool setActiveAccount(MySqlConnection conn, String username, String password)
        {
            DataTable myTable = new DataTable();

            string sql = "SELECT * FROM groupaccount WHERE userName=@username";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.Parameters.AddWithValue("@username", username);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(cmd);
            myAdapter.Fill(myTable);
            Console.WriteLine("Table is ready.");

            string tableusername = myTable.Rows[0][0].ToString();

            if (username != tableusername)
                return false;

            string tablepassword = myTable.Rows[0][2].ToString();

            if (password != tablepassword)
                return false;

            // valid account
            loggedInAccount = new Account(username, password);
            loggedInAccount.setContact(getContactWithID((int)myTable.Rows[0][1]));
            return true;
        }

        private static void setEventList(MySqlConnection conn)
        {
            DataTable eTable = new DataTable();
            DataTable cTable = new DataTable();

            String sql = "SELECT * FROM groupevent";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(cmd);
            myAdapter.Fill(eTable);
            Console.WriteLine("Table is ready.");

            foreach (DataRow row in eTable.Rows)
            {
                cTable.Clear();
                Event newEvent = new Event();
                newEvent.setEventID((int)row[0]);
                newEvent.setTitle(row[1].ToString());
                DateTime startTime = ((DateTime)row[2]).AddHours(((TimeSpan)row[3]).Hours).AddMinutes(((TimeSpan)row[3]).Minutes);
                DateTime endTime = ((DateTime)row[2]).AddHours(((TimeSpan)row[4]).Hours).AddMinutes(((TimeSpan)row[4]).Minutes);
                newEvent.setEventTimeBlock(TimeBlock.createTimeBlock(startTime, endTime));
                newEvent.setDetails(row[5].ToString());
                newEvent.setRoomID(Convert.ToInt32(row[6]));
                allEvents.Add(newEvent);

                sql = "SELECT * from groupparticipation where eventID = @myevent";
                cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@myevent", row["eventID"]);
                myAdapter = new MySqlDataAdapter(cmd);
                myAdapter.Fill(cTable);
                Console.WriteLine("Table is ready.");

                foreach (DataRow cRow in cTable.Rows)
                {
                    Contact con = getContactWithID((int)cRow["employeeID"]);
                    if (con != null)
                    {
                        newEvent.addParticipant(con);
                        con.setEventInCalendar(newEvent);
                    }
                }
            }
        }

        private static void setRoomList(MySqlConnection conn)
        {
            DataTable eTable = new DataTable();
            DataTable rTable = new DataTable();

            String sql = "SELECT * FROM grouproom";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(cmd);
            myAdapter.Fill(rTable);

            foreach (DataRow row in rTable.Rows)
            {
                int roomID = (int)row["roomID"];
                Room newRoom = new Room(roomID, row["location"].ToString());
                allRooms.Add(newRoom);

                sql = "SELECT * from groupevent where roomID = @roomID";
                cmd = new MySqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@roomID", row["roomID"]);
                myAdapter = new MySqlDataAdapter(cmd);
                eTable.Clear();
                myAdapter.Fill(eTable);

                foreach (DataRow eRow in eTable.Rows)
                {
                    Event e = getEventWithID((int)eRow["eventID"]);
                    if (e != null)
                    {
                        e.setRoomID(roomID);
                        newRoom.setEventInCalendar(e);
                    }
                }
            }
        }

        private static void setContactList(MySqlConnection conn)
        {
            DataTable cTable = new DataTable();

            String sql = "SELECT * FROM groupcontact";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataAdapter myAdapter = new MySqlDataAdapter(cmd);
            myAdapter.Fill(cTable);
            Console.WriteLine("Table is ready.");

            foreach (DataRow row in cTable.Rows)
            {
                Contact newContact = new Contact(row[1].ToString(), row[2].ToString(), row[3].ToString(), row[4].ToString());
                newContact.setContactID(Convert.ToInt32(row[0]));
                allContacts.Add(newContact);
            }
        }

        public static Contact getContactWithID(int id)
        {
            foreach (Contact c in allContacts)
            {
                if (c.getContactID() == id)
                    return c;
            }
            return null;
        }

        public static Event getEventWithID(int id)
        {
            foreach (Event e in allEvents)
            {
                if (e.getEventID() == id)
                    return e;
            }
            return null;
        }

        public static Room getRoomWithID(int id)
        {
            foreach (Room r in allRooms)
            {
                if (r.getroomID() == id)
                    return r;
            }
            return null;
        }

        public static void saveEventToDB(Event e, bool eventEditor)
        {
            string connStr = "server=csdatabase.eku.edu;user=tim_hopkins8;database=Hopkins;port=3306;password=Hopkins8;";
            MySqlConnection conn = new MySqlConnection(connStr);
            try
            {
                Console.WriteLine("Connecting to MySQL...");
                conn.Open();
                String sql = "INSERT into groupevent (title, date, startTime, endTime, details, roomID) VALUES(@title, @date, @sTime, @eTime, @details, @roomID)";
                if (eventEditor)
                    sql = "UPDATE groupevent SET title = @Title, date = @Date, startTime = @sTime, endTime = @eTime, details = @details, roomID = @roomID WHERE eventID = @eventID";

                MySqlCommand cmd = new MySqlCommand(sql, conn);

                if(eventEditor)
                    cmd.Parameters.AddWithValue("@eventID", e.getEventID());

                String date = e.getTimeBlock().getStart().Date.ToString("yyyy-MM-dd");
                String startTime = e.getTimeBlock().getStart().ToString("HH:mm:ss");
                String endTime = e.getTimeBlock().getEnd().ToString("HH:mm:ss");

                cmd.Parameters.AddWithValue("@title", e.getTitle());
                cmd.Parameters.AddWithValue("@date", date);
                cmd.Parameters.AddWithValue("@sTime", startTime);
                cmd.Parameters.AddWithValue("@eTime", endTime);
                cmd.Parameters.AddWithValue("@details", e.getDetails());
                cmd.Parameters.AddWithValue("@roomID", e.getRoomID());
                
                cmd.ExecuteNonQuery();
                int eventID = getLastID(conn);

                if (eventEditor)
                {
                    if (eventEditor)
                    {
                        sql = "DELETE FROM groupparticipation WHERE eventID = @eventID";
                        MySqlCommand cmd2 = new MySqlCommand(sql, conn);
                        cmd2.Parameters.AddWithValue("@eventID", e.getEventID());
                        cmd2.ExecuteNonQuery();
                    }
                }
                else
                {
                    e.setEventID(eventID);
                }
                    

                foreach (Contact c in e.getMyParticipants())
                {
                    sql = "INSERT into groupparticipation (eventID, employeeID) VALUES(@eventID, @employeeID)";
                    MySqlCommand cmd1 = new MySqlCommand(sql, conn);
                    cmd1.Parameters.AddWithValue("@eventID", eventID);
                    cmd1.Parameters.AddWithValue("@employeeID", c.getContactID());
                    cmd1.ExecuteNonQuery();   
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            conn.Close();
        }

        private static int getLastID(MySqlConnection conn)
        {
            int rValue = -1;
            String sql = "SELECT max(eventID) FROM groupevent";

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader myReader = cmd.ExecuteReader();
            if (myReader.Read())
            {
                rValue = (int) myReader["max(eventID)"];
            }
            myReader.Close();
            return rValue;
        }
        public static string DumpDataTable(DataTable table)
        {
            string data = string.Empty;
            StringBuilder sb = new StringBuilder();

            if (null != table && null != table.Rows)
            {
                foreach (DataRow dataRow in table.Rows)
                {
                    foreach (var item in dataRow.ItemArray)
                    {
                        sb.Append(item);
                        sb.Append(',');
                    }
                    sb.AppendLine();
                }

                data = sb.ToString();
            }
            return data;
        }
    }  
}
