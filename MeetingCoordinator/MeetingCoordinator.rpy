I-Logix-RPY-Archive version 8.5.2 Modeler C++ 1159120
{ IProject 
	- _id = GUID b8955b6d-f438-4379-a001-b19a2c3a7ee0;
	- _myState = 8192;
	- _name = "MeetingCoordinator";
	- _objectCreation = "39345872717201715-59321039";
	- _umlDependencyID = "3573";
	- _lastID = 10;
	- _UserColors = { IRPYRawContainer 
		- size = 16;
		- value = 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 16777215; 
	}
	- _defaultSubsystem = { ISubsystemHandle 
		- _m2Class = "ISubsystem";
		- _filename = "Default.sbs";
		- _subsystem = "";
		- _class = "";
		- _name = "Default";
		- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
	}
	- _component = { IHandle 
		- _m2Class = "IComponent";
		- _filename = "DefaultComponent.cmp";
		- _subsystem = "";
		- _class = "";
		- _name = "DefaultComponent";
		- _id = GUID e5399f4e-a77a-4e39-8be0-82402ad42379;
	}
	- Multiplicities = { IRPYRawContainer 
		- size = 4;
		- value = 
		{ IMultiplicityItem 
			- _name = "1";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "*";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "0,1";
			- _count = -1;
		}
		{ IMultiplicityItem 
			- _name = "1..*";
			- _count = -1;
		}
	}
	- Subsystems = { IRPYRawContainer 
		- size = 2;
		- value = 
		{ ISubsystem 
			- fileName = "Default";
			- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
		}
		{ ISubsystem 
			- fileName = "ActivityDiagrams";
			- _id = GUID b5d45e86-6b12-4b6e-afdc-0559107a1e89;
		}
	}
	- Diagrams = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IDiagram 
			- _id = GUID 0c940c39-c48c-4df3-a7fb-aa7e471503f7;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 4;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "Actor";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,26,84,168";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "111,0,107";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Class";
								- Properties = { IRPYRawContainer 
									- size = 8;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,34,84,148";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "121,122,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "0";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Ellipse";
								- Properties = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "FreeText";
								- Properties = { IRPYRawContainer 
									- size = 9;
									- value = 
									{ IProperty 
										- _Name = "Fill.Transparent_Fill";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.Height";
										- _Value = "13";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "HorzAlign";
										- _Value = "0";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.Transparent";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Multiline";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "VertAlign";
										- _Value = "0";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Wordbreak";
										- _Value = "False";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "Model1";
			- _objectCreation = "39351192717201715-64641039";
			- _umlDependencyID = "2243";
			- _lastModifiedTime = "10.27.2017::19:8:44";
			- _graphicChart = { CGIClassChart 
				- _id = GUID d43044e2-f1d9-4983-aa40-cac51cc8a26a;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IDiagram";
					- _id = GUID 0c940c39-c48c-4df3-a7fb-aa7e471503f7;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 7;
				{ CGIClass 
					- _id = GUID 4e4d4876-9ef9-4b3c-95ce-1cf33adb4706;
					- m_type = 78;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "TopLevel";
						- _id = GUID 1d214e90-2b2b-44ac-832f-bc1e6e816445;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "TopLevel";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIFreeText 
					- _id = GUID ad8cdc65-a494-4622-98be-d874e8bc8e2a;
					- m_type = 189;
					- m_pModelObject = { IHandle 
						- _m2Class = "";
					}
					- m_pParent = GUID 4e4d4876-9ef9-4b3c-95ce-1cf33adb4706;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_points = 4 469 117  510 117  510 135  469 135  ;
					- m_text = "";
				}
				{ CGIClass 
					- _id = GUID 568be0e1-b7e5-44be-9e6f-320f80cfba3b;
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Event";
						- _id = GUID bded13db-62b9-4ef6-a11e-475d53860dd9;
					}
					- m_pParent = GUID 4e4d4876-9ef9-4b3c-95ce-1cf33adb4706;
					- m_name = { CGIText 
						- m_str = "Event";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2152;
					- m_transform = 0.0793201 0 0 0.101604 418.841 635.572 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=50%,50%>
<frame name=AttributeListCompartment>
<frame name=PrimitiveOperationListCompartment>";
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 1;
						- value = 
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Event";
							- _name = "Event()";
							- _id = GUID 3eacb536-1f00-4b8e-9498-54e24bd63115;
						}
					}
				}
				{ CGIClass 
					- _id = GUID de88a475-dc70-455a-81f3-478d304548b5;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "GUIInterface";
						- _id = GUID 4aa99777-6edc-45b5-8a77-41fc3f52b011;
					}
					- m_pParent = GUID 4e4d4876-9ef9-4b3c-95ce-1cf33adb4706;
					- m_name = { CGIText 
						- m_str = "GUIInterface";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2152;
					- m_transform = 0.18508 0 0 0.375222 330.63 100.551 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_bFramesetModified = 1;
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=26%,74%>
<frame name=AttributeListCompartment>
<frame name=PrimitiveOperationListCompartment>";
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 15;
						- value = 
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToAddEvent()";
							- _id = GUID 88a53165-9ffd-4208-b3d7-36af27fcd830;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToHome()";
							- _id = GUID 79508c7e-333a-4a2e-8a20-525746a301dd;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToEditEvent()";
							- _id = GUID 6259ec7e-eaf7-4fd4-a8ab-548465d2dd17;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "getEventDetails()";
							- _id = GUID 6cbd1459-9997-4fc3-a927-4fdb1748356f;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "printEvent()";
							- _id = GUID 1f096719-fb8b-4d15-b480-7de4e4bfaf4d;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "displayDeletePopUp()";
							- _id = GUID 40650828-684f-4c4c-a5ee-f892a041d746;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "closeDeletePopUp()";
							- _id = GUID 6e2d3088-84f8-4792-9f4b-65bcb32b24b5;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "printEventsForDay()";
							- _id = GUID 715836ee-6444-4b63-91d5-fd8e18f5f1cc;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToEvent()";
							- _id = GUID a2a23b50-2a4c-4c52-b564-2068d2ab646c;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToMonthView()";
							- _id = GUID f5969ad3-3c58-423f-882f-0ebd4239664c;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "printEventsForMonth()";
							- _id = GUID 4e8e2b03-5e45-47e6-a291-988c40bbb987;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "displayLoginFailure()";
							- _id = GUID dfca098c-fb32-4764-8d56-6c333077b7ca;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToRegister()";
							- _id = GUID 4a719ca7-70b8-4711-907b-86865508a0c3;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "displayRegistrationFail()";
							- _id = GUID e7986dad-17ca-4866-9270-d6fd673b18ad;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToLogin()";
							- _id = GUID f7b437a2-4170-440d-b1f2-5a5de00a2d3c;
						}
					}
				}
				{ CGIClass 
					- _id = GUID dfe4077b-820e-4c39-8f5f-4fc9f0361c68;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Controller";
						- _id = GUID fe66ef5a-2198-4f39-9e99-954c8f86b33b;
					}
					- m_pParent = GUID 4e4d4876-9ef9-4b3c-95ce-1cf33adb4706;
					- m_name = { CGIText 
						- m_str = "Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2152;
					- m_transform = 0.250236 0 0 0.517825 15.4995 116.636 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_bFramesetModified = 1;
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=46%,54%>
<frame name=AttributeListCompartment>
<frame name=PrimitiveOperationListCompartment>";
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 29;
						- value = 
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateParticipantTimeList()";
							- _id = GUID c4270fa0-9401-4359-b655-b541a6442dcc;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "addParticipant()";
							- _id = GUID 3ebd7736-e904-4284-badd-2db9fd91e6b8;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "removeParticipant()";
							- _id = GUID 39b47fda-16b3-4484-82b2-70e44f2faadc;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "selectTime()";
							- _id = GUID 34382fdd-e372-46ef-950f-1f35f1247357;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateListOfAvailableRooms()";
							- _id = GUID 4100d92c-cd8d-41d4-943d-500b3d6359c6;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "selectRoom()";
							- _id = GUID 58674a86-4353-4194-8c0c-e54525bb3834;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "createEvent()";
							- _id = GUID 4abfbcc4-c9be-4e59-866e-83e7b785566e;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "cancelEvent()";
							- _id = GUID 2dff2d12-3cff-4bbe-97af-77fc9fb1ac2b;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "cancelEditEvent()";
							- _id = GUID 91b612bc-4981-4536-8660-9ce7de09a77a;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "getEventDetails()";
							- _id = GUID 88a18313-229f-4a20-9eaf-1599d7582da7;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "deleteEvent()";
							- _id = GUID 4c9a3d68-cf07-49d4-bd98-f7efaf8318e7;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "selectDay()";
							- _id = GUID 3b25f7c5-ee36-4e4b-ab70-e09ca588438a;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "getEventsForMonth()";
							- _id = GUID 6feda786-3214-469a-9746-af669aabd717;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "login()";
							- _id = GUID b8885941-35cd-459d-b161-d8592944a666;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "registerAccount()";
							- _id = GUID 1ce7fabf-7fdb-428f-a70e-767e0963de34;
						}
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "cleanUp()";
							- _id = GUID 5777259d-456a-4843-a218-ec4a09cc0269;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "navToRegister()";
							- _id = GUID 9e93743b-b264-4ce4-bfc2-58305dea801d;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "navToLogin()";
							- _id = GUID 468d579c-5d1c-442a-b4bf-d3fbfcd919c7;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "displayRegistrationFail()";
							- _id = GUID e810178d-540e-4059-8f04-9c99b982a6cd;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "navToHome()";
							- _id = GUID 7e296045-343e-4e98-9ff1-660579373a26;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "displayLoginFailure()";
							- _id = GUID 5de1952a-678e-4282-acc1-aa579e140bea;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "nav()";
							- _id = GUID 3fb0d31b-16a2-43a8-8a74-dd00c263cd78;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "navToMonthView()";
							- _id = GUID 42baf467-16e5-4f1f-a64e-00e2b4733c43;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "navToEvent()";
							- _id = GUID d10cccf4-ae9d-4247-9e0e-a882035be9eb;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "navToEditEvent()";
							- _id = GUID c57051cc-1d57-4eb4-a66c-2dc36c82ba36;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "displayDelete()";
							- _id = GUID 50644661-5a7d-4f8e-8e1f-2211b32a4d6b;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "displayDeletePopUp()";
							- _id = GUID a1aabd83-6a74-4a29-9461-e5e844efdb93;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "closeDeletePopUp()";
							- _id = GUID eba1c7db-3ad6-4d3e-82d1-e23063046a37;
						}
						{ IHandle 
							- _m2Class = "IReception";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "logout()";
							- _id = GUID 6ad47473-c466-47ea-a228-44b5bcd7c826;
						}
					}
				}
				{ CGIClass 
					- _id = GUID 0dbe5480-10a4-4942-9c5e-cb8421ed3555;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Account";
						- _id = GUID 30b8898a-abd4-4b48-b642-2ce3e7830d0a;
					}
					- m_pParent = GUID 4e4d4876-9ef9-4b3c-95ce-1cf33adb4706;
					- m_name = { CGIText 
						- m_str = "Account";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2152;
					- m_transform = 0.129367 0 0 0.101604 549.741 496.572 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=50%,50%>
<frame name=AttributeListCompartment>
<frame name=PrimitiveOperationListCompartment>";
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 1;
						- value = 
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Account";
							- _name = "account()";
							- _id = GUID e0c456f0-ff11-43b2-a962-004829522fd2;
						}
					}
				}
				{ CGIClass 
					- _id = GUID f455a080-65a2-4b6a-be71-f90139540b3b;
					- m_type = 87;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Contact";
						- _id = GUID ae86cfee-b398-4127-84da-82ad5aa80966;
					}
					- m_pParent = GUID 4e4d4876-9ef9-4b3c-95ce-1cf33adb4706;
					- m_name = { CGIText 
						- m_str = "Contact";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2152;
					- m_transform = 0.0793201 0 0 0.101604 538.841 226.572 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 2 329  2 1451  1061 1451  1061 329  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=50%,50%>
<frame name=AttributeListCompartment>
<frame name=PrimitiveOperationListCompartment>";
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 1;
						- value = 
						{ IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Contact";
							- _name = "contact()";
							- _id = GUID 964d3d61-2883-4662-b631-2c146b8b55ee;
						}
					}
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 4e4d4876-9ef9-4b3c-95ce-1cf33adb4706;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
		}
	}
	- MSCS = { IRPYRawContainer 
		- size = 8;
		- value = 
		{ IMSC 
			- _id = GUID 7d50dc77-2f46-48fc-8604-a26a2e6a51c2;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 7;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "CombinedFragment";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,250,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "DataFlow";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "2";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "121,122,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InteractionOperand";
								- Properties = { IRPYRawContainer 
									- size = 8;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,100,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Fill.Transparent_Fill";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "ReplyMessage";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "execution_occurrence";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
					{ IPropertySubject 
						- _Name = "SequenceDiagram";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "General";
								- Properties = { IRPYRawContainer 
									- size = 3;
									- value = 
									{ IProperty 
										- _Name = "ClassCentricMode";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "CleanupRealized";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "RealizeMessages";
										- _Value = "True";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "sequencediagram_AddEvent";
			- _objectCreation = "39351212717201715-64661039";
			- _umlDependencyID = "4148";
			- _lastModifiedTime = "10.26.2017::21:46:20";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 20;
				- m_usingActivationBar = 0;
				- _id = GUID eafb020d-878d-4190-91bf-5d98a09c96f3;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 7d50dc77-2f46-48fc-8604-a26a2e6a51c2;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 83;
				{ CGIBox 
					- _id = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID 175aa9b6-c6d4-4d41-a0a3-fadb91caa539;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "Loop";
					- _id = GUID 616b2d9a-76d6-4e43-ba54-c4b3cce1a2dd;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 8cee516c-3c5d-466c-a14f-57f3acf287c7;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = "interactionOperator_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.06111 0 0 1 0 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 465  540 465  540 655  0 655  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=190>
<frame Id=GUID db550824-f245-49b7-9fed-eb3579882a12>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "Loop";
					- _id = GUID 4e1e02e5-13cd-47ea-9988-be34034c41b2;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 0931eb3b-a0e4-4b6f-9ae5-0f64d4e786d5;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = "interactionOperator_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.06857 0 0 1 -0.822857 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 12 276  537 276  537 439  12 439  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=163>
<frame Id=GUID 4166525c-1ff8-4a4d-8348-86d485b4fc3e>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "Loop";
					- _id = GUID d59fe0d2-c83b-4ee9-bc6e-d142888ba3a4;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 9620bff4-70e1-4d9b-bf7b-b581184a817d;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = "interactionOperator_2";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.0572 0 0 1 0 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 672  542 672  542 907  0 907  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=235>
<frame Id=GUID 8d006304-6d46-452f-bf6c-f6e4c4ec857a>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "Loop";
					- _id = GUID 248efd2b-1663-4983-8835-e7d21dcff0c0;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 46e4c7ff-6e2a-406d-9516-2f5504738a1e;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = "interactionOperator_3";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.0088 0 0 1 0 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 918  568 918  568 1114  0 1114  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=196>
<frame Id=GUID f8d0bda7-847f-45e2-be59-0465ea750cc4>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID 93e8701c-f8f3-402e-95a1-5d49f1c35356;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID b0c2c14b-b1e8-4e4b-b351-f06067e64079;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = "interactionOperator_4";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.27381 0 0 1 -10.9524 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 40 1194  292 1194  292 1324  40 1324  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=130>
<frame Id=GUID 0893e199-f725-48f4-92d1-958b8d1289f3>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID 48b75692-0f37-485a-be92-3776af057f71;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 72db2ed8-d3ad-47c6-8738-f36c1917f92b;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = "interactionOperator_5";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.07143 0 0 1 -3.42857 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 48 1428  538 1428  538 1629  48 1629  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=201>
<frame Id=GUID 3e9744f3-4abf-41e1-a4db-a8293b943627>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID 2d853b9b-a31c-42d5-9d29-fa91b1c54058;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID f7f9d0d6-6aef-4349-8457-284a8abadf26;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = "interactionOperator_6";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 0.684902 0 0 1 15.1247 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 48 1644  505 1644  505 1803  48 1803  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=159>
<frame Id=GUID 76cca8f7-6f83-44e2-b86c-4d13251baaf0>";
				}
				{ CGIMscColumnCR 
					- _id = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_type = 97;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = ":User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392102 53 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = ":Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392101 265 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID f58ac67b-9c09-47d8-8bfd-2a964c67c5ac;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 087006cd-29ee-4e54-8047-e0bff40c7d05;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = ":Event";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392102 371 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = ":Participant";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392102 477 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
					}
					- m_pParent = GUID 18b741bd-757a-4509-9482-5278eef37a04;
					- m_name = { CGIText 
						- m_str = ":GUIInterface";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392102 159 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 6e3ee497-74f0-4bd0-b703-3984d857c53c;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 10d25c72-5a3d-4d48-8120-d5b880b15e96;
					}
					- m_pParent = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 37.5468 42 2397.33 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 8e2a5ca1-2de9-4ee4-a70e-d9a5b7be0c45;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID b62d9a26-25ab-4605-b248-e382818a06df;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 3032ca29-c5e4-45db-8ef5-f0470aac5bb6;
					}
					- m_pParent = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 33690.2 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID c1488532-fbef-409f-9285-6dac105c4613;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 10035bf5-0402-4d82-9c07-0ebe56e2cd39;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID c76bc550-3c2f-404a-9e71-f10c97755fe9;
					}
					- m_pParent = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5035 42 33690.2 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID c1488532-fbef-409f-9285-6dac105c4613;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 95efb6f6-6d6a-4eee-abda-150b42da999a;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 580123f0-098c-4d18-83c9-e8779d8872c3;
					}
					- m_pParent = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 51.7156 42 7319.52 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 5a258d06-ad71-4f88-bfd0-292b49fbf06c;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID c2384519-d501-4e22-9d40-f3378353fb6c;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID cc670276-a419-4ba1-82c9-66f1f3b3bd0e;
					}
					- m_pParent = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 86.4287 42 11629.6 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 14dfc5b7-f916-4991-9853-e899f259a92a;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 4bf5857e-e834-4bd5-87d6-9f7a8b565f39;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID b9030c5c-5a80-40cf-9084-981facd0f3cd;
					}
					- m_pParent = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 13414.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID b256bef2-5db3-4c39-a84b-0cf9e93f8e97;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID bb34dbaa-b109-4d00-9f9c-73729c27eed0;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 99448b53-ead7-4f25-8f14-834d02b488d7;
					}
					- m_pParent = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 88.5541 42 17495.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 0915553f-b724-4ac8-8563-6298247806a4;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 5f32ed5e-d1b4-4704-ac0c-2bfa3e33b0cb;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 464fb5f4-e2db-4e28-a4bc-02a11870404f;
					}
					- m_pParent = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 19612.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 62f6748d-d606-4588-aae4-a37d694992fa;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID e1866489-d675-4170-a391-f35476885f02;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 9fd00dd9-59b4-4267-b053-acbd519ad217;
					}
					- m_pParent = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 75.8024 42 23361.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 0b7484ed-5e5a-40d4-956c-80027aca8630;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID c206dd91-3008-435e-a3f3-594b3ed37d0c;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID b7f7d466-5305-422f-bfa9-9dd58f79f379;
					}
					- m_pParent = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 24891.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID cbe55143-d151-463e-a49b-fd4156e6876b;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 278bd107-c4bc-41ee-985d-20960c76bab6;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID ad8d5ba9-2c16-4695-80f8-6f7db3c70610;
					}
					- m_pParent = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 100.598 42 27901 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID e174bcc5-cebd-4591-8cec-7bd3f13494db;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID ec422c58-fa9f-4cc3-8588-910f7f831e9d;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID ce42c34b-3dd8-43f7-930c-29db34e790cf;
					}
					- m_pParent = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 58.8001 42 36266.2 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID c6549aea-b4cf-42e6-8788-f295414e4f90;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 2ee178cc-4ab0-4173-92d2-ea6073a64035;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID ab974099-8aaa-46fa-8793-e2facbced1d6;
					}
					- m_pParent = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 29.0458 42 42234 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID a53a6e3f-037a-4e72-95be-70788826240b;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 2f2a658e-956f-4221-b3cf-847f04dd3db8;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID e35c0ae4-6ca5-4a58-a82b-3897547e3fb4;
					}
					- m_pParent = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 37.5469 42 2397.34 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 8e2a5ca1-2de9-4ee4-a70e-d9a5b7be0c45;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID f45361eb-f848-4b20-9d6d-948641f9578c;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 01bfdc53-0dc8-4270-89b9-437d873aa975;
					}
					- m_pParent = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 7064.49 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID e1f2992c-3b43-4bd6-9652-e03d3d3a3baa;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID e29947ca-a6be-45f5-982c-29ce77e712d3;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID a43c9990-084c-43dc-a88c-76483c26b43d;
					}
					- m_pParent = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 65.1758 42 7064.49 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID e1f2992c-3b43-4bd6-9652-e03d3d3a3baa;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 042d7774-4c8d-4510-950c-89ec6fb51fcb;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 7fd2ef48-0f4d-49f0-a0b9-d731987a8567;
					}
					- m_pParent = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 92.8047 42 11629.6 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 14dfc5b7-f916-4991-9853-e899f259a92a;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID bead15d1-f691-4268-b3f6-dff3052bf947;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID d872219d-8a2c-4865-8e5a-cb2824494585;
					}
					- m_pParent = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 16959.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 45628705-296b-4a14-9a2b-b15be6dcbbf5;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 439f94ab-491c-432f-9ec7-b4f5a234ddb0;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 3c77c8f9-9374-452f-b16e-196cbe15c5c0;
					}
					- m_pParent = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 109.807 42 16959.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 45628705-296b-4a14-9a2b-b15be6dcbbf5;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID d4068b7b-0f2b-4f19-9db4-27fdee96f439;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID c60a7d91-fcee-40cd-91dd-91a1768c8867;
					}
					- m_pParent = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 23233.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID a60168fc-93f9-417c-8158-961dffcf7d46;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID c4e8c71e-f6e6-41e2-a32c-15bc05aade29;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 0146971d-3044-46e5-9365-e52e401183f0;
					}
					- m_pParent = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 83.5951 42 23233.8 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID a60168fc-93f9-417c-8158-961dffcf7d46;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID ac8b9c1d-d644-4b0c-b1f3-6f676ad574e7;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID b340deba-dc61-4887-94e6-b722abf70da1;
					}
					- m_pParent = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 27518.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 3b902217-3512-4f43-9b1b-d9b67b7e7208;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 9d6dd677-ffe8-4bba-b331-ca1083199cde;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 08fcd42a-32e2-408c-a050-9af8407a4825;
					}
					- m_pParent = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 108.39 42 27518.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 3b902217-3512-4f43-9b1b-d9b67b7e7208;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 1b807a66-fea4-47b0-83dd-d3c66773af7b;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 718ed076-112d-486a-92b3-694cdff1d49b;
					}
					- m_pParent = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 35781.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 1f264e42-e0c5-44cc-b17b-d8c3ea1f53ec;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID a3028015-5ca1-4568-b579-fd653b622352;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID fb52f386-53a8-40f7-a990-dbbd9529aed2;
					}
					- m_pParent = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 108.39 42 35781.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 1f264e42-e0c5-44cc-b17b-d8c3ea1f53ec;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID cea00b0f-91bf-4f88-9c57-aa5c1e85886f;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 9fdebf90-9b2f-436b-879a-0dc9df3c940b;
					}
					- m_pParent = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 41953.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 77de2f95-ce22-499c-bb51-80dcf6225d9d;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 14ca5e17-993c-4d71-a31b-a606c9fedbed;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID a3bda5fc-1eee-41fd-a9a7-eceb7e1cfef7;
					}
					- m_pParent = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 51.7156 42 41953.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 77de2f95-ce22-499c-bb51-80dcf6225d9d;
				}
				{ CGIMscMessage 
					- _id = GUID 3b902217-3512-4f43-9b1b-d9b67b7e7208;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7ad97dfd-5ccb-4d73-a61b-d442bf1e920f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_5";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 27518 ;
					- m_TargetPort = 48 27518 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID ac8b9c1d-d644-4b0c-b1f3-6f676ad574e7;
					- m_pTargetExec = GUID 9d6dd677-ffe8-4bba-b331-ca1083199cde;
				}
				{ CGIMscMessage 
					- _id = GUID 410c5438-5a3f-4bc9-997e-34c96e0883a5;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID be9f6691-0f4c-4668-801a-32ceb5515f25;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "listOfEvents()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 20173 ;
					- m_TargetPort = 48 20173 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 94b0a8f5-bd0e-430e-82ff-e6d920032eb7;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f6e03542-8aca-426f-937f-4d7c6a622181;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "updatePotentialEventTimes()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -84 -8  117 -8  117 10  -84 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 171 639 ;
						- m_nHorizontalSpacing = 50;
						- m_nVerticalSpacing = 32;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 14461 ;
					- m_TargetPort = 48 14461 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID e1f2992c-3b43-4bd6-9652-e03d3d3a3baa;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c3e26e96-db66-4559-b37e-da36af01ae28;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 7064 ;
					- m_TargetPort = 48 7064 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID f45361eb-f848-4b20-9d6d-948641f9578c;
					- m_pTargetExec = GUID e29947ca-a6be-45f5-982c-29ce77e712d3;
				}
				{ CGIMscMessage 
					- _id = GUID d0965448-04c0-461c-ae7f-de4c40a3ad16;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 8359f09b-7b80-4a91-bff3-53ef741c6fee;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "updatePotentialEventTimes()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -84 -8  117 -8  117 10  -84 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 147 851 ;
						- m_nHorizontalSpacing = 26;
						- m_nVerticalSpacing = 43;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 20403 ;
					- m_TargetPort = 48 20403 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 45628705-296b-4a14-9a2b-b15be6dcbbf5;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c17932ab-d1a6-4b5d-afa2-2fa275b3429b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_3";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 16960 ;
					- m_TargetPort = 48 16960 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID bead15d1-f691-4268-b3f6-dff3052bf947;
					- m_pTargetExec = GUID 439f94ab-491c-432f-9ec7-b4f5a234ddb0;
				}
				{ CGIMscMessage 
					- _id = GUID b2f4955a-5116-4be4-b4fe-a9f4072d4b6d;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a1a2e8fa-18f7-4d89-8257-dd2d8096fe40;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "updateAvailableRooms()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -71 -8  100 -8  100 10  -71 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 158 1045 ;
						- m_nHorizontalSpacing = 20;
						- m_nVerticalSpacing = 32;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 25733 ;
					- m_TargetPort = 48 25733 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 0915553f-b724-4ac8-8563-6298247806a4;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 35ac485b-a381-4c1f-8843-5da94f924743;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "removeParticipant()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 17495 ;
					- m_TargetPort = 48 17495 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID bb34dbaa-b109-4d00-9f9c-73729c27eed0;
				}
				{ CGIMscMessage 
					- _id = GUID 692ef746-eebe-4815-8f15-b414dfa3e1f4;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 30d568fc-a34f-463d-9796-6d1bb703d71a;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getCalendar()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 7881 ;
					- m_TargetPort = 48 7881 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 1f264e42-e0c5-44cc-b17b-d8c3ea1f53ec;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID def1be42-edab-476b-9659-15039b66ca90;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_6";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 35781 ;
					- m_TargetPort = 48 35782 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 1b807a66-fea4-47b0-83dd-d3c66773af7b;
					- m_pTargetExec = GUID a3028015-5ca1-4568-b579-fd653b622352;
				}
				{ CGIMscMessage 
					- _id = GUID a53a6e3f-037a-4e72-95be-70788826240b;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a3877942-f571-4a66-8f43-d935364c13ac;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "cancelEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 42234 ;
					- m_TargetPort = 48 42234 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 2ee178cc-4ab0-4173-92d2-ea6073a64035;
				}
				{ CGIMscMessage 
					- _id = GUID 0b7484ed-5e5a-40d4-956c-80027aca8630;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 895439ae-50de-41fa-80fb-036251a84723;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "selectTime()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 23361 ;
					- m_TargetPort = 48 23361 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID e1866489-d675-4170-a391-f35476885f02;
				}
				{ CGIMscMessage 
					- _id = GUID 5a258d06-ad71-4f88-bfd0-292b49fbf06c;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 94ad433b-5e83-4843-8fa9-d6fba0abe864;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "generateParticipantTimeList()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 7320 ;
					- m_TargetPort = 48 7320 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 95efb6f6-6d6a-4eee-abda-150b42da999a;
				}
				{ CGIMscMessage 
					- _id = GUID a60168fc-93f9-417c-8158-961dffcf7d46;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f4d72865-e386-4201-ab52-d274f3ad34f9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_4";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 23234 ;
					- m_TargetPort = 48 23234 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID d4068b7b-0f2b-4f19-9db4-27fdee96f439;
					- m_pTargetExec = GUID c4e8c71e-f6e6-41e2-a32c-15bc05aade29;
				}
				{ CGIMscMessage 
					- _id = GUID aa3869d0-71cf-44c3-bee8-40617234be54;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0dfb176c-e4ba-4e0e-a2a5-130a93b486c3;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "generateListOfAvailableRooms()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 343 986  343 1006  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 23871 ;
					- m_TargetPort = 48 24381 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID c6549aea-b4cf-42e6-8788-f295414e4f90;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c19453bf-42ce-4447-b1de-45c4f8ebbad2;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "createEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 36266 ;
					- m_TargetPort = 48 36266 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID ec422c58-fa9f-4cc3-8588-910f7f831e9d;
				}
				{ CGIMscMessage 
					- _id = GUID c1488532-fbef-409f-9285-6dac105c4613;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4130a873-0f00-4a91-8681-dab6a2048ca3;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 8 -7  80 -7  80 9  8 9  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 93 161 ;
						- m_nVerticalSpacing = -1;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 33690 ;
					- m_TargetPort = 48 33690 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID b62d9a26-25ab-4605-b248-e382818a06df;
					- m_pTargetExec = GUID 10035bf5-0402-4d82-9c07-0ebe56e2cd39;
				}
				{ CGIMscMessage 
					- _id = GUID e174bcc5-cebd-4591-8cec-7bd3f13494db;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 5149bfc8-e8d3-4644-b86a-663a0fc0c7f7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "selectRoom()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 27901 ;
					- m_TargetPort = 48 27901 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 278bd107-c4bc-41ee-985d-20960c76bab6;
				}
				{ CGIMscMessage 
					- _id = GUID 7cd9df5f-87d0-4d56-be7f-0b9a77783a02;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID e6d09e46-c830-4238-b780-a900026958c7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_2";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 11502 ;
					- m_TargetPort = 48 11502 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 1018ae0e-bf10-4067-b198-3d856e264657;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 8bb593d0-78a3-4c20-bcef-caa3847f8f0c;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "updatePotentialEventTimes()      ";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -100 -8  126 -8  126 10  -100 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 169 416 ;
						- m_nHorizontalSpacing = 57;
						- m_nVerticalSpacing = 27;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 8901 ;
					- m_TargetPort = 48 8901 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 14dfc5b7-f916-4991-9853-e899f259a92a;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 2ad6aaf7-4c15-49e3-b5a6-58ea1a0c1ed1;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "addParticipant()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 11630 ;
					- m_TargetPort = 48 11630 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 042d7774-4c8d-4510-950c-89ec6fb51fcb;
					- m_pTargetExec = GUID c2384519-d501-4e22-9d40-f3378353fb6c;
				}
				{ CGIMscMessage 
					- _id = GUID efadcf1b-2bb4-4df0-a8d5-a82cf73f6faf;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID eec725fe-e749-44a5-a341-bcb96e36456a;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "setEventInCalendar()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 37465 ;
					- m_TargetPort = 48 37465 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID bce5d13a-3bb1-4f11-a80f-4d21298fc236;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 06b16727-236a-4b37-b927-d049a070bd7d;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToHome()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 237 1728  237 1752  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 42795 ;
					- m_TargetPort = 48 43407 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 4674ab49-500f-48c2-8fc4-bd58ed4b6a31;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 697fbab2-5258-4e0e-826b-9564e8a9bc7c;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "ListOfEvents()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 8595 ;
					- m_TargetPort = 48 8595 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 77de2f95-ce22-499c-bb51-80dcf6225d9d;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 973fcd73-c97d-494a-aa81-232629493522;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_7";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 41953 ;
					- m_TargetPort = 48 41953 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID cea00b0f-91bf-4f88-9c57-aa5c1e85886f;
					- m_pTargetExec = GUID 14ca5e17-993c-4d71-a31b-a606c9fedbed;
				}
				{ CGIMscMessage 
					- _id = GUID 8aa63650-16f6-4ca8-9a07-271bf1361a22;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7503644a-3b93-4efb-9c62-560f738c2308;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "generateParticipantTimeList()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 343 775  343 799  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 18490 ;
					- m_TargetPort = 48 19102 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3d5f4216-48a0-4576-8132-9a5413ec534f;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 5f87801f-9af1-4fe9-8972-03adb80e25cc;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "Event()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID f58ac67b-9c09-47d8-8bfd-2a964c67c5ac;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 36776 ;
					- m_TargetPort = 48 36776 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 6fed1e4b-a65c-43b3-9d30-9c12bc26424f;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID bd0bad71-bbbc-416e-8673-71699dc50190;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "generateParticipantTimeList()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 343 525  343 556  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 12114 ;
					- m_TargetPort = 48 12905 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 8e2a5ca1-2de9-4ee4-a70e-d9a5b7be0c45;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0489328a-3618-44ce-a479-3e3d77ea3d83;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToEditEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID c8ec04aa-9f3a-439a-8eb2-0796e9203f01;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 2397 ;
					- m_TargetPort = 48 2397 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 2f2a658e-956f-4221-b3cf-847f04dd3db8;
					- m_pTargetExec = GUID 6e3ee497-74f0-4bd0-b703-3984d857c53c;
				}
				{ CGIMscMessage 
					- _id = GUID f7454443-7192-467b-89d5-cfa3c9e482d4;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 34bd5774-6b34-4f10-a8d7-edb9db491ef4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "displayLocationField()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -6 -9  148 -9  148 9  -6 9  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 172 1291 ;
						- m_nHorizontalSpacing = 13;
						- m_nVerticalSpacing = 38;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 30910 ;
					- m_TargetPort = 48 30910 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID cbe55143-d151-463e-a49b-fd4156e6876b;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 48bb309b-1845-448f-9053-e880722372d8;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getCalendar()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 24892 ;
					- m_TargetPort = 48 24891 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID c206dd91-3008-435e-a3f3-594b3ed37d0c;
				}
				{ CGIMscMessage 
					- _id = GUID 62f6748d-d606-4588-aae4-a37d694992fa;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7caff3e8-d387-4d77-a746-991b1898f08a;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getCalendar()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 19612 ;
					- m_TargetPort = 48 19612 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 5f32ed5e-d1b4-4704-ac0c-2bfa3e33b0cb;
				}
				{ CGIMscMessage 
					- _id = GUID b256bef2-5db3-4c39-a84b-0cf9e93f8e97;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 558598bf-2f34-499e-a572-808c109b8352;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getCalendar()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_sourceType = 'F';
					- m_pTarget = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 13415 ;
					- m_TargetPort = 48 13415 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 4bf5857e-e834-4bd5-87d6-9f7a8b565f39;
				}
				{ CGIMscMessage 
					- _id = GUID 3c6409ae-9ea8-40cc-a9ff-81279a27c385;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a5bb02b7-26f4-4260-b1ef-c6088dff1e87;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToHome()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_sourceType = 'F';
					- m_pTarget = GUID 19284369-422c-4b7c-8fed-ac910d7767cd;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 237 1547  237 1580  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 38179 ;
					- m_TargetPort = 48 39020 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3347ef11-4321-46f3-a27f-3336c81c9177;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4bb9e947-90e2-4253-9d57-c9b39aa602e8;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "listOfEvents()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 25580 ;
					- m_TargetPort = 48 25580 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID e12a44ad-ff36-4f82-8ad2-04157a6a2a7d;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 984f3a37-8680-47b2-9fee-16ac1bf6bee3;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "listOfEvents()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID af983783-f2a9-48df-a0d0-1eb7f529b7eb;
					- m_sourceType = 'F';
					- m_pTarget = GUID f9cc375e-0131-48b8-a646-eefa98159847;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 14205 ;
					- m_TargetPort = 48 14206 ;
					- m_bLeft = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID db550824-f245-49b7-9fed-eb3579882a12;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID c7a2559d-d6bd-4dc7-ae90-25a259e296d9;
					}
					- m_pParent = GUID 616b2d9a-76d6-4e43-ba54-c4b3cce1a2dd;
					- m_name = { CGIText 
						- m_str = "Adding Participants";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 465  540 465  540 655  0 655  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 4166525c-1ff8-4a4d-8348-86d485b4fc3e;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 47d05b3d-1fde-42b8-b363-ed8e0b8d794c;
					}
					- m_pParent = GUID 4e1e02e5-13cd-47ea-9988-be34034c41b2;
					- m_name = { CGIText 
						- m_str = "change event date";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 12 276  537 276  537 439  12 439  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 8d006304-6d46-452f-bf6c-f6e4c4ec857a;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 1fb69e26-e91d-4506-97ff-7d95b6d3cff4;
					}
					- m_pParent = GUID d59fe0d2-c83b-4ee9-bc6e-d142888ba3a4;
					- m_name = { CGIText 
						- m_str = "Removing Participants";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 672  542 672  542 907  0 907  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID f8d0bda7-847f-45e2-be59-0465ea750cc4;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID c6ebf480-d107-4959-8dd6-85731364e97c;
					}
					- m_pParent = GUID 248efd2b-1663-4983-8835-e7d21dcff0c0;
					- m_name = { CGIText 
						- m_str = "Selecting Event Time";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 918  568 918  568 1114  0 1114  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 0893e199-f725-48f4-92d1-958b8d1289f3;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 971b455b-2cbf-43c8-b65b-e7c84a965825;
					}
					- m_pParent = GUID 93e8701c-f8f3-402e-95a1-5d49f1c35356;
					- m_name = { CGIText 
						- m_str = "room == \"other\"";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -6 -9  116 -9  116 9  -6 9  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 93 1207 ;
						- m_nHorizontalSpacing = -52;
						- m_nVerticalSpacing = 4;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 40 1194  292 1194  292 1324  40 1324  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 3e9744f3-4abf-41e1-a4db-a8293b943627;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 61754bd1-e4b3-45bf-9c6f-e6e0fef0adec;
					}
					- m_pParent = GUID 48b75692-0f37-485a-be92-3776af057f71;
					- m_name = { CGIText 
						- m_str = "user clicks save";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 48 1428  538 1428  538 1629  48 1629  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 76cca8f7-6f83-44e2-b86c-4d13251baaf0;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 57381dce-c33f-478b-adb4-1f768390adac;
					}
					- m_pParent = GUID 2d853b9b-a31c-42d5-9d29-fa91b1c54058;
					- m_name = { CGIText 
						- m_str = "user clicks cancel";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 48 1644  505 1644  505 1803  48 1803  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 18b741bd-757a-4509-9482-5278eef37a04;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID 175aa9b6-c6d4-4d41-a0a3-fadb91caa539;
				- _objectCreation = "39351232717201715-64681039";
				- _umlDependencyID = "1696";
				- ClassifierRoles = { IRPYRawContainer 
					- size = 5;
					- value = 
					{ IClassifierRole 
						- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						- _myState = 2048;
						- _objectCreation = "39351252717201715-64701039";
						- _umlDependencyID = "1691";
						- m_eRoleType = ACTOR;
						- m_pBase = { IHandle 
							- _m2Class = "IActor";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "User";
							- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						- _myState = 2048;
						- _objectCreation = "39351272717201715-64721039";
						- _umlDependencyID = "1695";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Controller";
							- _id = GUID fe66ef5a-2198-4f39-9e99-954c8f86b33b;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 087006cd-29ee-4e54-8047-e0bff40c7d05;
						- _myState = 2048;
						- _objectCreation = "39351292717201715-64741039";
						- _umlDependencyID = "1699";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Event";
							- _id = GUID bded13db-62b9-4ef6-a11e-475d53860dd9;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						- _myState = 2048;
						- _objectCreation = "39351312717201715-64761039";
						- _umlDependencyID = "1694";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Participant";
							- _id = GUID 9ab151d3-7bdf-4bf3-b50a-4072f4224c70;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						- _objectCreation = "39351332717201715-64781039";
						- _umlDependencyID = "1698";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "GUIInterface";
							- _id = GUID 4aa99777-6edc-45b5-8a77-41fc3f52b011;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 36;
					- value = 
					{ IMessage 
						- _id = GUID 0489328a-3618-44ce-a479-3e3d77ea3d83;
						- _name = "navToEditEvent";
						- _objectCreation = "39351352717201715-64801039";
						- _umlDependencyID = "3117";
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToEditEvent()";
							- _id = GUID 6259ec7e-eaf7-4fd4-a8ab-548465d2dd17;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 10d25c72-5a3d-4d48-8120-d5b880b15e96;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID e35c0ae4-6ca5-4a58-a82b-3897547e3fb4;
						}
					}
					{ IMessage 
						- _id = GUID 4130a873-0f00-4a91-8681-dab6a2048ca3;
						- _myState = 2048;
						- _name = "message_0";
						- _objectCreation = "39351372717201715-64821039";
						- _umlDependencyID = "2581";
						- m_szSequence = "28.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID c76bc550-3c2f-404a-9e71-f10c97755fe9;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 3032ca29-c5e4-45db-8ef5-f0470aac5bb6;
						}
					}
					{ IMessage 
						- _id = GUID 94ad433b-5e83-4843-8fa9-d6fba0abe864;
						- _name = "generateParticipantTimeList";
						- _objectCreation = "39351392717201715-64841039";
						- _umlDependencyID = "4506";
						- m_szSequence = "3.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateParticipantTimeList()";
							- _id = GUID c4270fa0-9401-4359-b655-b541a6442dcc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 580123f0-098c-4d18-83c9-e8779d8872c3;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 30d568fc-a34f-463d-9796-6d1bb703d71a;
						- _name = "getCalendar";
						- _objectCreation = "39351412717201715-64861039";
						- _umlDependencyID = "2810";
						- m_szSequence = "4.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "getCalendar()";
							- _id = GUID 912de468-53c6-4de7-85a4-59701e16be2f;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 697fbab2-5258-4e0e-826b-9564e8a9bc7c;
						- _name = "ListOfEvents";
						- _objectCreation = "39351432717201715-64881039";
						- _umlDependencyID = "2922";
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "ListOfEvents()";
							- _id = GUID 76e319ba-9466-4f06-ad69-7c67f55afbd8;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 8bb593d0-78a3-4c20-bcef-caa3847f8f0c;
						- _name = "updatePotentialEventTimes";
						- _objectCreation = "39351452717201715-64901039";
						- _umlDependencyID = "4310";
						- m_szSequence = "6.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "      ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 2ad6aaf7-4c15-49e3-b5a6-58ea1a0c1ed1;
						- _name = "addParticipant";
						- _objectCreation = "39351472717201715-64921039";
						- _umlDependencyID = "3147";
						- m_szSequence = "8.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "addParticipant()";
							- _id = GUID 3ebd7736-e904-4284-badd-2db9fd91e6b8;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID cc670276-a419-4ba1-82c9-66f1f3b3bd0e;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 7fd2ef48-0f4d-49f0-a0b9-d731987a8567;
						}
					}
					{ IMessage 
						- _id = GUID bd0bad71-bbbc-416e-8673-71699dc50190;
						- _name = "generateParticipantTimeList";
						- _objectCreation = "39351492717201715-64941039";
						- _umlDependencyID = "4508";
						- m_szSequence = "9.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateParticipantTimeList()";
							- _id = GUID c4270fa0-9401-4359-b655-b541a6442dcc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 558598bf-2f34-499e-a572-808c109b8352;
						- _name = "getCalendar";
						- _objectCreation = "39351512717201715-64961039";
						- _umlDependencyID = "2812";
						- m_szSequence = "10.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "getCalendar()";
							- _id = GUID 912de468-53c6-4de7-85a4-59701e16be2f;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID b9030c5c-5a80-40cf-9084-981facd0f3cd;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID b5d9891c-aa9d-4444-a76e-b56f037520e5;
						}
					}
					{ IMessage 
						- _id = GUID 984f3a37-8680-47b2-9fee-16ac1bf6bee3;
						- _name = "listOfEvents";
						- _objectCreation = "39351532717201715-64981039";
						- _umlDependencyID = "2956";
						- m_szSequence = "11.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "listOfEvents()";
							- _id = GUID 03f4fd26-5eb2-4a68-b547-910dcefa4f96;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID f6e03542-8aca-426f-937f-4d7c6a622181;
						- _name = "updatePotentialEventTimes";
						- _objectCreation = "39351552717201715-65001039";
						- _umlDependencyID = "4303";
						- m_szSequence = "12.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 35ac485b-a381-4c1f-8843-5da94f924743;
						- _name = "removeParticipant";
						- _objectCreation = "39351572717201715-65021039";
						- _umlDependencyID = "3497";
						- m_szSequence = "14.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "removeParticipant()";
							- _id = GUID 39b47fda-16b3-4484-82b2-70e44f2faadc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 99448b53-ead7-4f25-8f14-834d02b488d7;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7503644a-3b93-4efb-9c62-560f738c2308;
						- _name = "generateParticipantTimeList";
						- _objectCreation = "39351592717201715-65041039";
						- _umlDependencyID = "4501";
						- m_szSequence = "15.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateParticipantTimeList()";
							- _id = GUID c4270fa0-9401-4359-b655-b541a6442dcc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7caff3e8-d387-4d77-a746-991b1898f08a;
						- _name = "getCalendar";
						- _objectCreation = "39351612717201715-65061039";
						- _umlDependencyID = "2805";
						- m_szSequence = "16.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "getCalendar()";
							- _id = GUID 912de468-53c6-4de7-85a4-59701e16be2f;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 464fb5f4-e2db-4e28-a4bc-02a11870404f;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID be0ab49b-bd82-4790-89f1-0b5c6cd0abc8;
						}
					}
					{ IMessage 
						- _id = GUID be9f6691-0f4c-4668-801a-32ceb5515f25;
						- _name = "listOfEvents";
						- _objectCreation = "39351632717201715-65081039";
						- _umlDependencyID = "2949";
						- m_szSequence = "17.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "listOfEvents()";
							- _id = GUID 03f4fd26-5eb2-4a68-b547-910dcefa4f96;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 8359f09b-7b80-4a91-bff3-53ef741c6fee;
						- _name = "updatePotentialEventTimes";
						- _objectCreation = "39351652717201715-65101039";
						- _umlDependencyID = "4305";
						- m_szSequence = "18.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 895439ae-50de-41fa-80fb-036251a84723;
						- _name = "selectTime";
						- _objectCreation = "39351672717201715-65121039";
						- _umlDependencyID = "2733";
						- m_szSequence = "20.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "selectTime()";
							- _id = GUID 34382fdd-e372-46ef-950f-1f35f1247357;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 9fd00dd9-59b4-4267-b053-acbd519ad217;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0dfb176c-e4ba-4e0e-a2a5-130a93b486c3;
						- _name = "generateListOfAvailableRooms";
						- _objectCreation = "39351692717201715-65141039";
						- _umlDependencyID = "4559";
						- m_szSequence = "21.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateListOfAvailableRooms()";
							- _id = GUID 4100d92c-cd8d-41d4-943d-500b3d6359c6;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 48bb309b-1845-448f-9053-e880722372d8;
						- _name = "getCalendar";
						- _objectCreation = "39351712717201715-65161039";
						- _umlDependencyID = "2807";
						- m_szSequence = "22.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "getCalendar()";
							- _id = GUID 912de468-53c6-4de7-85a4-59701e16be2f;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID b7f7d466-5305-422f-bfa9-9dd58f79f379;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 7fa9938f-bde5-4f29-9925-2727091814ef;
						}
					}
					{ IMessage 
						- _id = GUID 4bb9e947-90e2-4253-9d57-c9b39aa602e8;
						- _name = "listOfEvents";
						- _objectCreation = "39351732717201715-65181039";
						- _umlDependencyID = "2951";
						- m_szSequence = "23.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "listOfEvents()";
							- _id = GUID 03f4fd26-5eb2-4a68-b547-910dcefa4f96;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a1a2e8fa-18f7-4d89-8257-dd2d8096fe40;
						- _name = "updateAvailableRooms";
						- _objectCreation = "39351752717201715-65201039";
						- _umlDependencyID = "3760";
						- m_szSequence = "24.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 5149bfc8-e8d3-4644-b86a-663a0fc0c7f7;
						- _name = "selectRoom";
						- _objectCreation = "39351772717201715-65221039";
						- _umlDependencyID = "2749";
						- m_szSequence = "26.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "selectRoom()";
							- _id = GUID 58674a86-4353-4194-8c0c-e54525bb3834;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID ad8d5ba9-2c16-4695-80f8-6f7db3c70610;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 34bd5774-6b34-4f10-a8d7-edb9db491ef4;
						- _name = "displayLocationField";
						- _objectCreation = "39351792717201715-65241039";
						- _umlDependencyID = "3767";
						- m_szSequence = "27.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c19453bf-42ce-4447-b1de-45c4f8ebbad2;
						- _name = "createEvent";
						- _objectCreation = "39351812717201715-65261039";
						- _umlDependencyID = "2837";
						- m_szSequence = "30.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "createEvent()";
							- _id = GUID 4abfbcc4-c9be-4e59-866e-83e7b785566e;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID ce42c34b-3dd8-43f7-930c-29db34e790cf;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 5f87801f-9af1-4fe9-8972-03adb80e25cc;
						- _name = "Event";
						- _objectCreation = "39351832717201715-65281039";
						- _umlDependencyID = "2213";
						- m_szSequence = "31.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 087006cd-29ee-4e54-8047-e0bff40c7d05;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Event";
							- _name = "Event()";
							- _id = GUID 3eacb536-1f00-4b8e-9498-54e24bd63115;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID eec725fe-e749-44a5-a341-bcb96e36456a;
						- _name = "setEventInCalendar";
						- _objectCreation = "39351852717201715-65301039";
						- _umlDependencyID = "3517";
						- m_szSequence = "32.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 66360f1a-eebf-4c06-9997-d11faa70cd1b;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "setEventInCalendar()";
							- _id = GUID ac6bf539-df63-4bc6-8d96-9a9069defe40;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a5bb02b7-26f4-4260-b1ef-c6088dff1e87;
						- _name = "navToHome";
						- _objectCreation = "39351872717201715-65321039";
						- _umlDependencyID = "2611";
						- m_szSequence = "33.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a3877942-f571-4a66-8f43-d935364c13ac;
						- _name = "cancelEvent";
						- _objectCreation = "39351892717201715-65341039";
						- _umlDependencyID = "2830";
						- m_szSequence = "35.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 789e68f0-53cc-41a6-9539-ea3c52152b82;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "cancelEvent()";
							- _id = GUID 2dff2d12-3cff-4bbe-97af-77fc9fb1ac2b;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID ab974099-8aaa-46fa-8793-e2facbced1d6;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 06b16727-236a-4b37-b927-d049a070bd7d;
						- _name = "navToHome";
						- _objectCreation = "39351912717201715-65361039";
						- _umlDependencyID = "2610";
						- m_szSequence = "36.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c3e26e96-db66-4559-b37e-da36af01ae28;
						- _myState = 2048;
						- _name = "message_1";
						- _objectCreation = "39351932717201715-65381039";
						- _umlDependencyID = "2586";
						- m_szSequence = "2.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID a43c9990-084c-43dc-a88c-76483c26b43d;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 01bfdc53-0dc8-4270-89b9-437d873aa975;
						}
					}
					{ IMessage 
						- _id = GUID e6d09e46-c830-4238-b780-a900026958c7;
						- _myState = 2048;
						- _name = "message_2";
						- _objectCreation = "39351952717201715-65401039";
						- _umlDependencyID = "2582";
						- m_szSequence = "7.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c17932ab-d1a6-4b5d-afa2-2fa275b3429b;
						- _myState = 2048;
						- _name = "message_3";
						- _objectCreation = "39351972717201715-65421039";
						- _umlDependencyID = "2587";
						- m_szSequence = "13.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 3c77c8f9-9374-452f-b16e-196cbe15c5c0;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID d872219d-8a2c-4865-8e5a-cb2824494585;
						}
					}
					{ IMessage 
						- _id = GUID f4d72865-e386-4201-ab52-d274f3ad34f9;
						- _myState = 2048;
						- _name = "message_4";
						- _objectCreation = "39351992717201715-65441039";
						- _umlDependencyID = "2592";
						- m_szSequence = "19.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 0146971d-3044-46e5-9365-e52e401183f0;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID c60a7d91-fcee-40cd-91dd-91a1768c8867;
						}
					}
					{ IMessage 
						- _id = GUID 7ad97dfd-5ccb-4d73-a61b-d442bf1e920f;
						- _myState = 2048;
						- _name = "message_5";
						- _objectCreation = "39352012717201715-65461039";
						- _umlDependencyID = "2579";
						- m_szSequence = "25.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 08fcd42a-32e2-408c-a050-9af8407a4825;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID b340deba-dc61-4887-94e6-b722abf70da1;
						}
					}
					{ IMessage 
						- _id = GUID def1be42-edab-476b-9659-15039b66ca90;
						- _myState = 2048;
						- _name = "message_6";
						- _objectCreation = "39352032717201715-65481039";
						- _umlDependencyID = "2584";
						- m_szSequence = "29.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID fb52f386-53a8-40f7-a990-dbbd9529aed2;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 718ed076-112d-486a-92b3-694cdff1d49b;
						}
					}
					{ IMessage 
						- _id = GUID 973fcd73-c97d-494a-aa81-232629493522;
						- _myState = 2048;
						- _name = "message_7";
						- _objectCreation = "39352052717201715-65501039";
						- _umlDependencyID = "2580";
						- m_szSequence = "34.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 74a6b53c-298f-43fe-8ae0-964a48d32c4f;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID cdca98ce-c973-4e8c-8ea3-384ee07cdee9;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID a3bda5fc-1eee-41fd-a9a7-eceb7e1cfef7;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 9fdebf90-9b2f-436b-879a-0dc9df3c940b;
						}
					}
				}
				- ExecutionOccurrences = { IRPYRawContainer 
					- size = 30;
					- value = 
					{ IExecutionOccurrence 
						- _id = GUID 10d25c72-5a3d-4d48-8120-d5b880b15e96;
						- _objectCreation = "39352072717201715-65521039";
						- _umlDependencyID = "1693";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 0489328a-3618-44ce-a479-3e3d77ea3d83;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 53;
					}
					{ IExecutionOccurrence 
						- _id = GUID e35c0ae4-6ca5-4a58-a82b-3897547e3fb4;
						- _objectCreation = "39352092717201715-65541039";
						- _umlDependencyID = "1697";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 0489328a-3618-44ce-a479-3e3d77ea3d83;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 53;
					}
					{ IExecutionOccurrence 
						- _id = GUID c76bc550-3c2f-404a-9e71-f10c97755fe9;
						- _objectCreation = "39352112717201715-65561039";
						- _umlDependencyID = "1692";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 4130a873-0f00-4a91-8681-dab6a2048ca3;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 3032ca29-c5e4-45db-8ef5-f0470aac5bb6;
						- _objectCreation = "39352132717201715-65581039";
						- _umlDependencyID = "1696";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 4130a873-0f00-4a91-8681-dab6a2048ca3;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 580123f0-098c-4d18-83c9-e8779d8872c3;
						- _objectCreation = "39352152717201715-65601039";
						- _umlDependencyID = "1691";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 94ad433b-5e83-4843-8fa9-d6fba0abe864;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 73;
					}
					{ IExecutionOccurrence 
						- _id = GUID cc670276-a419-4ba1-82c9-66f1f3b3bd0e;
						- _objectCreation = "39352172717201715-65621039";
						- _umlDependencyID = "1695";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 2ad6aaf7-4c15-49e3-b5a6-58ea1a0c1ed1;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 122;
					}
					{ IExecutionOccurrence 
						- _id = GUID b9030c5c-5a80-40cf-9084-981facd0f3cd;
						- _objectCreation = "39352192717201715-65641039";
						- _umlDependencyID = "1699";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 558598bf-2f34-499e-a572-808c109b8352;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID b5d9891c-aa9d-4444-a76e-b56f037520e5;
						- _objectCreation = "39352212717201715-65661039";
						- _umlDependencyID = "1694";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 558598bf-2f34-499e-a572-808c109b8352;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 51;
					}
					{ IExecutionOccurrence 
						- _id = GUID 99448b53-ead7-4f25-8f14-834d02b488d7;
						- _objectCreation = "39352232717201715-65681039";
						- _umlDependencyID = "1698";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 35ac485b-a381-4c1f-8843-5da94f924743;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 125;
					}
					{ IExecutionOccurrence 
						- _id = GUID 464fb5f4-e2db-4e28-a4bc-02a11870404f;
						- _objectCreation = "39352252717201715-65701039";
						- _umlDependencyID = "1693";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 7caff3e8-d387-4d77-a746-991b1898f08a;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID be0ab49b-bd82-4790-89f1-0b5c6cd0abc8;
						- _objectCreation = "39352272717201715-65721039";
						- _umlDependencyID = "1697";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 7caff3e8-d387-4d77-a746-991b1898f08a;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 42;
					}
					{ IExecutionOccurrence 
						- _id = GUID 9fd00dd9-59b4-4267-b053-acbd519ad217;
						- _objectCreation = "39352292717201715-65741039";
						- _umlDependencyID = "1701";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 895439ae-50de-41fa-80fb-036251a84723;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 107;
					}
					{ IExecutionOccurrence 
						- _id = GUID b7f7d466-5305-422f-bfa9-9dd58f79f379;
						- _objectCreation = "39352312717201715-65761039";
						- _umlDependencyID = "1696";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 48bb309b-1845-448f-9053-e880722372d8;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 7fa9938f-bde5-4f29-9925-2727091814ef;
						- _objectCreation = "39352332717201715-65781039";
						- _umlDependencyID = "1700";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 48bb309b-1845-448f-9053-e880722372d8;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 47;
					}
					{ IExecutionOccurrence 
						- _id = GUID ad8d5ba9-2c16-4695-80f8-6f7db3c70610;
						- _objectCreation = "39352352717201715-65801039";
						- _umlDependencyID = "1695";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 5149bfc8-e8d3-4644-b86a-663a0fc0c7f7;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 142;
					}
					{ IExecutionOccurrence 
						- _id = GUID ce42c34b-3dd8-43f7-930c-29db34e790cf;
						- _objectCreation = "39352372717201715-65821039";
						- _umlDependencyID = "1699";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c19453bf-42ce-4447-b1de-45c4f8ebbad2;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 83;
					}
					{ IExecutionOccurrence 
						- _id = GUID ab974099-8aaa-46fa-8793-e2facbced1d6;
						- _objectCreation = "39352392717201715-65841039";
						- _umlDependencyID = "1703";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID a3877942-f571-4a66-8f43-d935364c13ac;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 41;
					}
					{ IExecutionOccurrence 
						- _id = GUID a43c9990-084c-43dc-a88c-76483c26b43d;
						- _objectCreation = "39352412717201715-65861039";
						- _umlDependencyID = "1698";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c3e26e96-db66-4559-b37e-da36af01ae28;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 92;
					}
					{ IExecutionOccurrence 
						- _id = GUID 01bfdc53-0dc8-4270-89b9-437d873aa975;
						- _objectCreation = "39352432717201715-65881039";
						- _umlDependencyID = "1702";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c3e26e96-db66-4559-b37e-da36af01ae28;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 7fd2ef48-0f4d-49f0-a0b9-d731987a8567;
						- _objectCreation = "39352452717201715-65901039";
						- _umlDependencyID = "1697";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 2ad6aaf7-4c15-49e3-b5a6-58ea1a0c1ed1;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 131;
					}
					{ IExecutionOccurrence 
						- _id = GUID 3c77c8f9-9374-452f-b16e-196cbe15c5c0;
						- _objectCreation = "39352472717201715-65921039";
						- _umlDependencyID = "1701";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c17932ab-d1a6-4b5d-afa2-2fa275b3429b;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 155;
					}
					{ IExecutionOccurrence 
						- _id = GUID d872219d-8a2c-4865-8e5a-cb2824494585;
						- _objectCreation = "39352492717201715-65941039";
						- _umlDependencyID = "1705";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c17932ab-d1a6-4b5d-afa2-2fa275b3429b;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 0146971d-3044-46e5-9365-e52e401183f0;
						- _objectCreation = "39352512717201715-65961039";
						- _umlDependencyID = "1700";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID f4d72865-e386-4201-ab52-d274f3ad34f9;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 118;
					}
					{ IExecutionOccurrence 
						- _id = GUID c60a7d91-fcee-40cd-91dd-91a1768c8867;
						- _objectCreation = "39352532717201715-65981039";
						- _umlDependencyID = "1704";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID f4d72865-e386-4201-ab52-d274f3ad34f9;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 08fcd42a-32e2-408c-a050-9af8407a4825;
						- _objectCreation = "39352552717201715-66001039";
						- _umlDependencyID = "1690";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 7ad97dfd-5ccb-4d73-a61b-d442bf1e920f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 153;
					}
					{ IExecutionOccurrence 
						- _id = GUID b340deba-dc61-4887-94e6-b722abf70da1;
						- _objectCreation = "39352572717201715-66021039";
						- _umlDependencyID = "1694";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 7ad97dfd-5ccb-4d73-a61b-d442bf1e920f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID fb52f386-53a8-40f7-a990-dbbd9529aed2;
						- _objectCreation = "39352592717201715-66041039";
						- _umlDependencyID = "1698";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID def1be42-edab-476b-9659-15039b66ca90;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 153;
					}
					{ IExecutionOccurrence 
						- _id = GUID 718ed076-112d-486a-92b3-694cdff1d49b;
						- _objectCreation = "39352612717201715-66061039";
						- _umlDependencyID = "1693";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID def1be42-edab-476b-9659-15039b66ca90;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID a3bda5fc-1eee-41fd-a9a7-eceb7e1cfef7;
						- _objectCreation = "39352632717201715-66081039";
						- _umlDependencyID = "1697";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 973fcd73-c97d-494a-aa81-232629493522;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 73;
					}
					{ IExecutionOccurrence 
						- _id = GUID 9fdebf90-9b2f-436b-879a-0dc9df3c940b;
						- _objectCreation = "39352652717201715-66101039";
						- _umlDependencyID = "1692";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 973fcd73-c97d-494a-aa81-232629493522;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
				}
				- CombinedFragments = { IRPYRawContainer 
					- size = 7;
					- value = 
					{ ICombinedFragment 
						- _id = GUID 8cee516c-3c5d-466c-a14f-57f3acf287c7;
						- _myState = 2048;
						- _name = "interactionOperator_0";
						- _objectCreation = "39352672717201715-66121039";
						- _umlDependencyID = "3867";
						- _interactionOperator = "Loop";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID c7a2559d-d6bd-4dc7-ae90-25a259e296d9;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39352692717201715-66141039";
								- _umlDependencyID = "3740";
								- _interactionConstraint = "Adding Participants";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID 0931eb3b-a0e4-4b6f-9ae5-0f64d4e786d5;
						- _myState = 2048;
						- _name = "interactionOperator_1";
						- _objectCreation = "39352712717201715-66161039";
						- _umlDependencyID = "3867";
						- _interactionOperator = "Loop";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 47d05b3d-1fde-42b8-b363-ed8e0b8d794c;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39352732717201715-66181039";
								- _umlDependencyID = "3739";
								- _interactionConstraint = "change event date";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID 9620bff4-70e1-4d9b-bf7b-b581184a817d;
						- _myState = 2048;
						- _name = "interactionOperator_2";
						- _objectCreation = "39352752717201715-66201039";
						- _umlDependencyID = "3867";
						- _interactionOperator = "Loop";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 1fb69e26-e91d-4506-97ff-7d95b6d3cff4;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39352772717201715-66221039";
								- _umlDependencyID = "3738";
								- _interactionConstraint = "Removing Participants";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID 46e4c7ff-6e2a-406d-9516-2f5504738a1e;
						- _myState = 2048;
						- _name = "interactionOperator_3";
						- _objectCreation = "39352792717201715-66241039";
						- _umlDependencyID = "3876";
						- _interactionOperator = "Loop";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID c6ebf480-d107-4959-8dd6-85731364e97c;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39352812717201715-66261039";
								- _umlDependencyID = "3737";
								- _interactionConstraint = "Selecting Event Time";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID b0c2c14b-b1e8-4e4b-b351-f06067e64079;
						- _myState = 2048;
						- _name = "interactionOperator_4";
						- _objectCreation = "39352832717201715-66281039";
						- _umlDependencyID = "3876";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 971b455b-2cbf-43c8-b65b-e7c84a965825;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39352852717201715-66301039";
								- _umlDependencyID = "3736";
								- _interactionConstraint = "room == \"other\"";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID 72db2ed8-d3ad-47c6-8738-f36c1917f92b;
						- _myState = 2048;
						- _name = "interactionOperator_5";
						- _objectCreation = "39352872717201715-66321039";
						- _umlDependencyID = "3876";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 61754bd1-e4b3-45bf-9c6f-e6e0fef0adec;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39352892717201715-66341039";
								- _umlDependencyID = "3744";
								- _interactionConstraint = "user clicks save";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID f7f9d0d6-6aef-4349-8457-284a8abadf26;
						- _myState = 2048;
						- _name = "interactionOperator_6";
						- _objectCreation = "39352912717201715-66361039";
						- _umlDependencyID = "3876";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 57381dce-c33f-478b-adb4-1f768390adac;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39352932717201715-66381039";
								- _umlDependencyID = "3743";
								- _interactionConstraint = "user clicks cancel";
							}
						}
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID 5996c01c-dcea-4f7d-a7cd-53fb96a0a3c3;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 8;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "CombinedFragment";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,250,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "DataFlow";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "2";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "EnvironmentLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "121,122,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InteractionOperand";
								- Properties = { IRPYRawContainer 
									- size = 8;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,100,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Fill.Transparent_Fill";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "ReplyMessage";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "execution_occurrence";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
					{ IPropertySubject 
						- _Name = "SequenceDiagram";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "General";
								- Properties = { IRPYRawContainer 
									- size = 3;
									- value = 
									{ IProperty 
										- _Name = "ClassCentricMode";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "CleanupRealized";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "RealizeMessages";
										- _Value = "True";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "sequencediagram_EditEvent";
			- _objectCreation = "39352952717201715-66401039";
			- _umlDependencyID = "4279";
			- _lastModifiedTime = "10.27.2017::17:18:54";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 5;
				- m_usingActivationBar = 0;
				- _id = GUID f0bf66a1-1362-4e53-aa64-d53a525c5498;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 5996c01c-dcea-4f7d-a7cd-53fb96a0a3c3;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 81;
				{ CGIBox 
					- _id = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID 3c943bfc-2f18-46af-9612-85cb8080f966;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "Loop";
					- _id = GUID 4cc87133-0d96-49bf-b93b-a325dd729d3b;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID fe37cdaf-3026-4bcf-903c-a5c21025a661;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = "interactionOperator_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.06481 0 0 1 102 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 465  540 465  540 655  0 655  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=190>
<frame Id=GUID e2194726-041f-432e-8b74-4f0b2e8fd8bf>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "Loop";
					- _id = GUID 5760008a-5f6a-4943-8aec-54b83eef6d56;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 551b9661-2108-47d4-88b9-d797f37ff599;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = "interactionOperator_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.07238 0 0 1 101.131 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 12 276  537 276  537 439  12 439  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=163>
<frame Id=GUID 4dc65fa9-1225-4a11-8b8f-34c45bd681a0>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "Loop";
					- _id = GUID ff4025df-b26d-416e-81c9-af7b473f3708;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID dec63be2-85ed-45e9-b5c1-4d0c5753d999;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = "interactionOperator_2";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.06089 0 0 1 102 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 672  542 672  542 907  0 907  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=235>
<frame Id=GUID 47361058-924c-46e8-9c7d-82d87178f602>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "Loop";
					- _id = GUID 4132cea0-b1c5-4c33-ae50-e6a750e91ed6;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 4891f429-dd15-4d1c-bcba-ce84b696bc9e;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = "interactionOperator_3";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.01232 0 0 1 102 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 918  568 918  568 1114  0 1114  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=196>
<frame Id=GUID 17ef731a-c19e-4778-b38c-ac3d04e3c6bd>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID b014b95a-0af0-4083-8adf-8aa2bb678e5e;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID d7bd35cb-3dc3-42ca-b58f-0c6f0b0d26ac;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = "interactionOperator_4";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.28175 0 0 1 90.7301 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 40 1194  292 1194  292 1324  40 1324  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=130>
<frame Id=GUID 7d16100d-993e-4aaa-924b-1e1a54867f5b>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID 382ebb77-d18f-4c92-9dca-a3bb4788a6af;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 643363c7-a9df-49f3-9012-f4eaf09c8697;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = "interactionOperator_5";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1.07551 0 0 1 98.3755 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 48 1428  538 1428  538 1629  48 1629  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=201>
<frame Id=GUID 46e2a021-22cf-44ee-be74-ab3b028869c8>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID e00ca239-4449-4740-b574-607c17c3be8e;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID ffd815a1-b9f3-4e14-a35c-4c703e85a92d;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = "interactionOperator_6";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 0.689278 0 0 1 116.915 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 48 1644  505 1644  505 1803  48 1803  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=159>
<frame Id=GUID 89a50c27-0bd3-4102-9183-67c684591c2f>";
				}
				{ CGIMscColumnCR 
					- _id = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_type = 97;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = ":User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392102 155 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = ":Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392101 369 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 351f7157-b8ee-44f0-ae7c-55689170fb2a;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 171764d6-8606-4170-9f6e-4f7a7613608e;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = ":Event";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392102 475 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = ":Participant";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392102 581 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
					}
					- m_pParent = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
					- m_name = { CGIText 
						- m_str = ":GUIInterface";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0392102 261 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 111ac2b7-dfbe-43e3-b709-69c4b9f88a11;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID c4c418c5-af08-4287-ab4d-a04d1c416e1d;
					}
					- m_pParent = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 29.0458 42 42234 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 289f7944-111e-4c01-822f-f32352fd2821;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID b74ac474-ed97-4105-869c-34886eda8b0d;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 5dd4f894-0c7c-4b2d-94ac-3e1a438e7d2a;
					}
					- m_pParent = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 37.5469 42 2397.34 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID eaeb82f4-6753-47f2-9d8a-d0c82a557bf7;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID f6f37560-2ded-4a91-8192-6918436199ac;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID abf5f27e-702a-474b-b5ca-bdcadb70f3a1;
					}
					- m_pParent = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 37.5469 42 2397.34 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID eaeb82f4-6753-47f2-9d8a-d0c82a557bf7;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 65532891-931b-4f64-a2c7-3163e06ac083;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 3288e83a-92ec-4b27-a2ef-548d5a7d0ed7;
					}
					- m_pParent = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 41953.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 40f9808a-bcc0-45c9-b9af-fb074d026852;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 906d727f-0f9c-4369-857b-c0be219baf9d;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID a0eb8600-b64c-491b-a61a-5f74f64a8e5d;
					}
					- m_pParent = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 51.7156 42 41953.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 40f9808a-bcc0-45c9-b9af-fb074d026852;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID fd1a700f-5511-4af8-9abb-510238e93a2e;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID d563490f-d302-4298-8fa9-b93cd648dd5c;
					}
					- m_pParent = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 100.598 42 27901 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 8f0fdccd-d950-43ba-bcee-ebc747e6ceb4;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID d8b4b4a8-67bc-4c05-bbcb-65f45cff6715;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 2f6952a0-621c-4d18-bf68-15ef978ebede;
					}
					- m_pParent = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 75.8025 42 23361.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 64d834cd-5943-4ec0-a5c5-523aa8514736;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 7c0247d8-c371-408d-9ae0-13a74e9b109a;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 3f88fe48-f53a-4949-8378-e44665b2fb79;
					}
					- m_pParent = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 33690.2 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 47190bd8-e69e-4ac6-8aef-66af395622dc;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID fbc2f7e6-0544-4da3-994e-755a773e0646;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID edd6f336-f045-4963-a280-5ded7db3273b;
					}
					- m_pParent = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 33690.2 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 47190bd8-e69e-4ac6-8aef-66af395622dc;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 0614e77d-d91f-4b2c-af62-8733eb5b83e1;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 66128448-6dca-432c-8caf-cbda5c78bdd7;
					}
					- m_pParent = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 88.5543 42 17495.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 72143525-e38f-4a6f-a788-1c17aefa715a;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID eb5fa867-3556-4b68-beaf-ab7d9e41a2ae;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID f8ac78ad-4bd0-45f4-b7bb-fb746d02140c;
					}
					- m_pParent = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 23233.8 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID e024fbe5-accc-426a-af50-1227f6879fe2;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 46ffb590-0c39-4140-97fa-c5fcfaf9e88d;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 4c2eaa71-f307-4dfa-95af-4f9f2e2c85cd;
					}
					- m_pParent = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 83.595 42 23233.8 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID e024fbe5-accc-426a-af50-1227f6879fe2;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 36021094-8aa6-4097-afec-4dae952574d6;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID d6a39868-ef75-454e-8c00-cf943b5c3f34;
					}
					- m_pParent = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 92.8046 42 11629.6 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 427ceab3-5587-4b96-86c6-d1114975db75;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 3b07d0c6-c4d2-4f39-9c2a-fd5343d5dbc2;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 0a8ef074-19a5-481d-85c9-1d6793ce5b36;
					}
					- m_pParent = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 86.429 42 11629.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 427ceab3-5587-4b96-86c6-d1114975db75;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 413bb0d6-fd44-4639-8881-71d24b8740ab;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 22ad4968-931e-4798-ba9f-5ad9e633375f;
					}
					- m_pParent = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 35781.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 51f08062-0528-4bdc-b8fa-1ae5e3325d3c;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID fc582d49-1364-4226-ac9f-b177503cfed1;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 7ec247bb-b2de-470b-a4a2-a09f91742ee5;
					}
					- m_pParent = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 108.39 42 35781.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 51f08062-0528-4bdc-b8fa-1ae5e3325d3c;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID e7d4a01c-ad68-4e7d-9c12-92e00fa942bc;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 6b69fa55-603a-4531-a404-a0b5ca156f45;
					}
					- m_pParent = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 51.7157 42 7319.54 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 90e5fce5-fb06-4029-b744-be30e7f9d1ae;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 5beb873f-c659-45a4-8c44-1f38ac016ca5;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 0082d5f5-a2c6-4532-93a4-3a66a4a3671f;
					}
					- m_pParent = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 16959.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID bb2cb0ea-5199-4fbe-9c9e-e12cb3477510;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID a9ab6230-4e96-4eb1-9156-5fca67dce633;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 30280c2a-96a9-4e09-baf9-d5aeec135738;
					}
					- m_pParent = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 109.807 42 16959.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID bb2cb0ea-5199-4fbe-9c9e-e12cb3477510;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 919ffc5d-4785-4fcd-8c83-c6490d57f44d;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 935d102a-44f0-4159-bc9e-0461319d5d7f;
					}
					- m_pParent = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 7064.49 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 170a5e84-2c3f-4f7e-bb29-4c4d21cf6b20;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID c985d028-f769-46bc-9dbb-2165ee98fed7;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 2f2950dd-d3bf-4db9-93a3-3546089396dd;
					}
					- m_pParent = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 65.1758 42 7064.49 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 170a5e84-2c3f-4f7e-bb29-4c4d21cf6b20;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 6b22397a-3859-4db6-8073-2a7c1247d1b1;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 1e4eb600-7425-4c09-a27f-d87f60af02ef;
					}
					- m_pParent = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 27518.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 43a389f9-7470-4825-aec2-7434d937b741;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 02502b68-46bf-4160-b623-221e5aa107b1;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 9af6ff1c-bf8b-4e75-94b1-40104935fe43;
					}
					- m_pParent = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 108.39 42 27518.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 43a389f9-7470-4825-aec2-7434d937b741;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 1253f61d-e4a7-4bb9-becc-241fce665374;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 1da37a37-5b4a-4c03-b5cb-988d3a8846db;
					}
					- m_pParent = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 13414.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 2a6d732e-5826-4161-966b-3135d5434df1;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 49d4e021-0963-48cd-9c34-f70e9e35a3c9;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 7eca4162-2672-42cc-a7a6-79ae95a929e2;
					}
					- m_pParent = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 19612.2 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 46c19e8f-ece0-4f8a-895e-5dbd7b9604eb;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID df7a412f-e0d8-420a-9afe-3a96c820faa0;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 1c9b88e1-e84b-4eb3-92fc-93221ce881cc;
					}
					- m_pParent = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 25.5036 42 24891.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 8ed2ce20-17aa-4bc3-9a30-7d875ee4d541;
				}
				{ CGIMscMessage 
					- _id = GUID 4f365f55-33b2-4ba7-a3f6-945f0d280e1c;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 863a8c05-2b44-4b9c-b104-7ae7e0505513;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "updatePotentialEventTimes()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -84 -8  117 -8  117 10  -84 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 147 851 ;
						- m_nHorizontalSpacing = 26;
						- m_nVerticalSpacing = 43;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 20403 ;
					- m_TargetPort = 48 20403 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 72143525-e38f-4a6f-a788-1c17aefa715a;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0ba137ed-da88-4088-adb2-d9d4dce94aad;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "removeParticipant()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 17495 ;
					- m_TargetPort = 48 17495 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 0614e77d-d91f-4b2c-af62-8733eb5b83e1;
				}
				{ CGIMscMessage 
					- _id = GUID a74dcba7-1e70-4e83-b8fa-c88a46ba7e68;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 05a6eabf-1c24-478e-8892-1e18dd25b865;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "updatePotentialEventTimes()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -84 -8  117 -8  117 10  -84 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 171 639 ;
						- m_nHorizontalSpacing = 50;
						- m_nVerticalSpacing = 32;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 14461 ;
					- m_TargetPort = 48 14461 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID e024fbe5-accc-426a-af50-1227f6879fe2;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c096e416-9f2d-42bd-81dc-dfed1926f333;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_4";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 23234 ;
					- m_TargetPort = 48 23234 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID eb5fa867-3556-4b68-beaf-ab7d9e41a2ae;
					- m_pTargetExec = GUID 46ffb590-0c39-4140-97fa-c5fcfaf9e88d;
				}
				{ CGIMscMessage 
					- _id = GUID 427ceab3-5587-4b96-86c6-d1114975db75;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 1e838d8f-8adf-4fe3-8fe4-c6e37d3c64f3;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "addParticipant()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 11630 ;
					- m_TargetPort = 48 11630 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 36021094-8aa6-4097-afec-4dae952574d6;
					- m_pTargetExec = GUID 3b07d0c6-c4d2-4f39-9c2a-fd5343d5dbc2;
				}
				{ CGIMscMessage 
					- _id = GUID 5867f50d-0b0b-4d0b-8154-82391367a998;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 50ba5570-f885-4304-ab8a-fa6a236624b4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "updatePotentialEventTimes()      ";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -100 -8  126 -8  126 10  -100 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 169 416 ;
						- m_nHorizontalSpacing = 57;
						- m_nVerticalSpacing = 27;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 8901 ;
					- m_TargetPort = 48 8901 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 51f08062-0528-4bdc-b8fa-1ae5e3325d3c;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d6a7b3f0-59a2-422b-9ee6-8f312de59e9c;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_6";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 35781 ;
					- m_TargetPort = 48 35782 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 413bb0d6-fd44-4639-8881-71d24b8740ab;
					- m_pTargetExec = GUID fc582d49-1364-4226-ac9f-b177503cfed1;
				}
				{ CGIMscMessage 
					- _id = GUID 90e5fce5-fb06-4029-b744-be30e7f9d1ae;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f4acb4a1-0e0c-4be0-bdd2-9016569e27e2;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "generateParticipantTimeList()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 7320 ;
					- m_TargetPort = 48 7320 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID e7d4a01c-ad68-4e7d-9c12-92e00fa942bc;
				}
				{ CGIMscMessage 
					- _id = GUID bb2cb0ea-5199-4fbe-9c9e-e12cb3477510;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID e95f011e-4337-422b-9112-c5d3c46987de;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_3";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 16960 ;
					- m_TargetPort = 48 16960 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 5beb873f-c659-45a4-8c44-1f38ac016ca5;
					- m_pTargetExec = GUID a9ab6230-4e96-4eb1-9156-5fca67dce633;
				}
				{ CGIMscMessage 
					- _id = GUID 170a5e84-2c3f-4f7e-bb29-4c4d21cf6b20;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c21202ef-6db4-46c4-b7fe-4194cf65c62f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 7064 ;
					- m_TargetPort = 48 7064 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 919ffc5d-4785-4fcd-8c83-c6490d57f44d;
					- m_pTargetExec = GUID c985d028-f769-46bc-9dbb-2165ee98fed7;
				}
				{ CGIMscMessage 
					- _id = GUID 43a389f9-7470-4825-aec2-7434d937b741;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 607e4769-7c15-4136-880c-b3d6117a6097;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_5";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 27518 ;
					- m_TargetPort = 48 27518 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 6b22397a-3859-4db6-8073-2a7c1247d1b1;
					- m_pTargetExec = GUID 02502b68-46bf-4160-b623-221e5aa107b1;
				}
				{ CGIMscMessage 
					- _id = GUID 77b4ce50-0889-42f3-86f6-025a15d129ae;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 17b65781-1e56-4ce9-820b-fbd47591ea1c;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "ListOfEvents()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 8595 ;
					- m_TargetPort = 48 8595 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID c12d46bd-a999-47c4-a72b-a856f097d3e9;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c42cbbc6-9f5d-47e1-8b62-71bba4896460;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getCalendar()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 7881 ;
					- m_TargetPort = 48 7881 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID b5930520-4166-4a27-8ca7-172c060f836e;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 359d4b11-4be9-4a1f-86e0-e154e5f9f625;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "listOfEvents()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 20173 ;
					- m_TargetPort = 48 20173 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 4fb6126c-b527-4b3e-9fca-2a51261e0ede;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 11591690-237b-4e96-be2b-2d264989949d;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "listOfEvents()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 14205 ;
					- m_TargetPort = 48 14206 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID ab49bac9-33f3-4645-8222-ebee670f59dd;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 37351402-f123-44f0-bf85-dd59b82915df;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "listOfEvents()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 25580 ;
					- m_TargetPort = 48 25580 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 2a6d732e-5826-4161-966b-3135d5434df1;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a0ec9ace-8c52-49d8-9509-4c9cfc8a786f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getCalendar()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 13415 ;
					- m_TargetPort = 48 13415 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 1253f61d-e4a7-4bb9-becc-241fce665374;
				}
				{ CGIMscMessage 
					- _id = GUID 46c19e8f-ece0-4f8a-895e-5dbd7b9604eb;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 2242f92c-ea26-4953-a836-138e69bace0c;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getCalendar()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 19612 ;
					- m_TargetPort = 48 19612 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 49d4e021-0963-48cd-9c34-f70e9e35a3c9;
				}
				{ CGIMscMessage 
					- _id = GUID 8ed2ce20-17aa-4bc3-9a30-7d875ee4d541;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 343dd569-d711-4698-b75a-81519c15a859;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getCalendar()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID 07dad8a5-e2bc-4628-860e-f010f1f38de6;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 24892 ;
					- m_TargetPort = 48 24891 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID df7a412f-e0d8-420a-9afe-3a96c820faa0;
				}
				{ CGIMscMessage 
					- _id = GUID 831afecd-2d45-455c-8307-72a8441b296d;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 845c32a8-8d0c-4da4-9e55-7737a143bd52;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "generateParticipantTimeList()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 447 775  447 799  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 18490 ;
					- m_TargetPort = 48 19102 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 7baf6dff-553e-414f-b817-16086eb19365;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0e35d45b-0be6-4d51-9771-9ebd4107e682;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "generateParticipantTimeList()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 447 525  447 556  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 12114 ;
					- m_TargetPort = 48 12905 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 7174dc98-0a0d-44a2-9a48-d9bee7be37af;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c8d2373f-c467-4723-82f0-7d1ef68e5291;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "generateListOfAvailableRooms()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 447 986  447 1006  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 23871 ;
					- m_TargetPort = 48 24381 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID cc28d134-cdff-42f0-be8f-1c7f273cbd09;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 34ec029a-cf37-490d-83c4-7fd8a9672229;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getEventDetails()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 2958 ;
					- m_TargetPort = 48 2958 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID eaeb82f4-6753-47f2-9d8a-d0c82a557bf7;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f8e6fabf-7f86-496e-ba1e-0005f3fe3c87;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToEditEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 2397 ;
					- m_TargetPort = 48 2397 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID b74ac474-ed97-4105-869c-34886eda8b0d;
					- m_pTargetExec = GUID f6f37560-2ded-4a91-8192-6918436199ac;
				}
				{ CGIMscMessage 
					- _id = GUID 40f9808a-bcc0-45c9-b9af-fb074d026852;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 14b0f569-1cbf-47c2-927b-3aa18f69c063;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_7";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 41953 ;
					- m_TargetPort = 48 41953 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 65532891-931b-4f64-a2c7-3163e06ac083;
					- m_pTargetExec = GUID 906d727f-0f9c-4369-857b-c0be219baf9d;
				}
				{ CGIMscMessage 
					- _id = GUID e68dd5ea-ff1b-4388-9a7f-3843c1372fa0;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 052dbe7f-af59-45cd-9808-bb5ae40b4b75;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "printEventDetails()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -6 -9  124 -9  124 9  -6 9  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 313 180 ;
						- m_nHorizontalSpacing = 21;
						- m_nVerticalSpacing = -2;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 3596 ;
					- m_TargetPort = 48 3596 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 289f7944-111e-4c01-822f-f32352fd2821;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 53b0fe4e-5ad9-473b-8df0-dc7e3a8316ed;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "cancelEditEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 42234 ;
					- m_TargetPort = 48 42234 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 111ac2b7-dfbe-43e3-b709-69c4b9f88a11;
				}
				{ CGIMscMessage 
					- _id = GUID f1e22144-bf3d-45cd-9f7b-4e9990ef73c1;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 926937a5-311c-4c99-a688-561ad70b5c47;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToHome()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 339 1728  339 1752  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 42795 ;
					- m_TargetPort = 48 43407 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 56b93243-4640-4536-a9d1-12fca5237dfb;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 75b256d3-800e-4cd2-a297-a25102cc6117;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToHome()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 339 1547  339 1580  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 38179 ;
					- m_TargetPort = 48 39020 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID f716ef52-bd02-41a1-856d-22a4f78a9bef;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a04c280e-baad-4dcd-a4b4-1b601915431b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_2";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 11502 ;
					- m_TargetPort = 48 11502 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 8f0fdccd-d950-43ba-bcee-ebc747e6ceb4;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 157db84e-76ef-4459-8a5d-7593ad92b899;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "selectRoom()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 27901 ;
					- m_TargetPort = 48 27901 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID fd1a700f-5511-4af8-9abb-510238e93a2e;
				}
				{ CGIMscMessage 
					- _id = GUID f233da3a-43f1-427e-a718-7b3edad5c45e;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0c699b91-fc81-4a5d-938a-3f6059765e08;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "displayLocationField()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -6 -9  148 -9  148 9  -6 9  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 172 1291 ;
						- m_nHorizontalSpacing = 13;
						- m_nVerticalSpacing = 38;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 30910 ;
					- m_TargetPort = 48 30910 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 47190bd8-e69e-4ac6-8aef-66af395622dc;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 38208e74-31b3-48bb-9fca-734ffef8e543;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 8 -7  80 -7  80 9  8 9  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 93 161 ;
						- m_nVerticalSpacing = -1;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 19b4ce06-6ab3-4659-96e8-5861f409d8d6;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 33690 ;
					- m_TargetPort = 48 33690 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 7c0247d8-c371-408d-9ae0-13a74e9b109a;
					- m_pTargetExec = GUID fbc2f7e6-0544-4da3-994e-755a773e0646;
				}
				{ CGIMscMessage 
					- _id = GUID 64d834cd-5943-4ec0-a5c5-523aa8514736;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 94c545a1-09dc-4483-9ecd-8cd0cc4edaf8;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "selectTime()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_sourceType = 'F';
					- m_pTarget = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 23361 ;
					- m_TargetPort = 48 23361 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID d8b4b4a8-67bc-4c05-bbcb-65f45cff6715;
				}
				{ CGIMscMessage 
					- _id = GUID 65a22f24-50c1-47a3-b2db-f2f07375aa8f;
					- m_type = 193;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 3305e38d-69f9-42d6-96ff-e459451e7fb0;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "updateAvailableRooms()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -71 -8  100 -8  100 10  -71 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 158 1045 ;
						- m_nHorizontalSpacing = 20;
						- m_nVerticalSpacing = 32;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e4beea9a-346e-45d5-be11-687b2ab59fc1;
					- m_sourceType = 'F';
					- m_pTarget = GUID de76b77f-9f33-4a1f-b757-99b7f47ea40e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 25733 ;
					- m_TargetPort = 48 25733 ;
					- m_bLeft = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID e2194726-041f-432e-8b74-4f0b2e8fd8bf;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 48a56133-361d-4fbb-b535-358ec0e89f5b;
					}
					- m_pParent = GUID 4cc87133-0d96-49bf-b93b-a325dd729d3b;
					- m_name = { CGIText 
						- m_str = "Adding Participants";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 465  540 465  540 655  0 655  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 4dc65fa9-1225-4a11-8b8f-34c45bd681a0;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 6d5ed9b4-c1aa-4068-8155-59546d537e8a;
					}
					- m_pParent = GUID 5760008a-5f6a-4943-8aec-54b83eef6d56;
					- m_name = { CGIText 
						- m_str = "change event date";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 12 276  537 276  537 439  12 439  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 47361058-924c-46e8-9c7d-82d87178f602;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 0955b815-b17e-45d4-b4ae-07cc7b0139b8;
					}
					- m_pParent = GUID ff4025df-b26d-416e-81c9-af7b473f3708;
					- m_name = { CGIText 
						- m_str = "Removing Participants";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 672  542 672  542 907  0 907  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 17ef731a-c19e-4778-b38c-ac3d04e3c6bd;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 83ae2505-b19e-4cf2-a06c-fdfe02a4fa7c;
					}
					- m_pParent = GUID 4132cea0-b1c5-4c33-ae50-e6a750e91ed6;
					- m_name = { CGIText 
						- m_str = "Selecting Event Time";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 0 918  568 918  568 1114  0 1114  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 7d16100d-993e-4aaa-924b-1e1a54867f5b;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID b5b52162-3e46-42e0-8e72-6534b318e864;
					}
					- m_pParent = GUID b014b95a-0af0-4083-8adf-8aa2bb678e5e;
					- m_name = { CGIText 
						- m_str = "room == \"other\"";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -6 -9  116 -9  116 9  -6 9  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 93 1207 ;
						- m_nHorizontalSpacing = -52;
						- m_nVerticalSpacing = 4;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 40 1194  292 1194  292 1324  40 1324  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 46e2a021-22cf-44ee-be74-ab3b028869c8;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 467214e9-293b-4e68-a1cc-3480e12d06d0;
					}
					- m_pParent = GUID 382ebb77-d18f-4c92-9dca-a3bb4788a6af;
					- m_name = { CGIText 
						- m_str = "user clicks save";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 48 1428  538 1428  538 1629  48 1629  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 89a50c27-0bd3-4102-9183-67c684591c2f;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 51c242e1-04fc-41d0-8731-3e883ade2a3f;
					}
					- m_pParent = GUID e00ca239-4449-4740-b574-607c17c3be8e;
					- m_name = { CGIText 
						- m_str = "user clicks cancel";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 48 1644  505 1644  505 1803  48 1803  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 5c029a90-5f03-4686-9e79-81ed7b76ee01;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID 3c943bfc-2f18-46af-9612-85cb8080f966;
				- _objectCreation = "39352972717201715-66421039";
				- _umlDependencyID = "1702";
				- ClassifierRoles = { IRPYRawContainer 
					- size = 5;
					- value = 
					{ IClassifierRole 
						- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						- _myState = 2048;
						- _objectCreation = "39352992717201715-66441039";
						- _umlDependencyID = "1706";
						- m_eRoleType = ACTOR;
						- m_pBase = { IHandle 
							- _m2Class = "IActor";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "User";
							- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						- _myState = 2048;
						- _objectCreation = "39353012717201715-66461039";
						- _umlDependencyID = "1692";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Controller";
							- _id = GUID fe66ef5a-2198-4f39-9e99-954c8f86b33b;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 171764d6-8606-4170-9f6e-4f7a7613608e;
						- _myState = 2048;
						- _objectCreation = "39353032717201715-66481039";
						- _umlDependencyID = "1696";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Event";
							- _id = GUID bded13db-62b9-4ef6-a11e-475d53860dd9;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
						- _myState = 2048;
						- _objectCreation = "39353052717201715-66501039";
						- _umlDependencyID = "1691";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Participant";
							- _id = GUID 9ab151d3-7bdf-4bf3-b50a-4072f4224c70;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						- _objectCreation = "39353072717201715-66521039";
						- _umlDependencyID = "1695";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "GUIInterface";
							- _id = GUID 4aa99777-6edc-45b5-8a77-41fc3f52b011;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 35;
					- value = 
					{ IMessage 
						- _id = GUID c8d2373f-c467-4723-82f0-7d1ef68e5291;
						- _name = "generateListOfAvailableRooms";
						- _objectCreation = "39353092717201715-66541039";
						- _umlDependencyID = "4560";
						- m_szSequence = "23.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateListOfAvailableRooms()";
							- _id = GUID 4100d92c-cd8d-41d4-943d-500b3d6359c6;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0e35d45b-0be6-4d51-9771-9ebd4107e682;
						- _name = "generateParticipantTimeList";
						- _objectCreation = "39353112717201715-66561039";
						- _umlDependencyID = "4499";
						- m_szSequence = "11.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateParticipantTimeList()";
							- _id = GUID c4270fa0-9401-4359-b655-b541a6442dcc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 845c32a8-8d0c-4da4-9e55-7737a143bd52;
						- _name = "generateParticipantTimeList";
						- _objectCreation = "39353132717201715-66581039";
						- _umlDependencyID = "4503";
						- m_szSequence = "17.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateParticipantTimeList()";
							- _id = GUID c4270fa0-9401-4359-b655-b541a6442dcc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 343dd569-d711-4698-b75a-81519c15a859;
						- _name = "getCalendar";
						- _objectCreation = "39353152717201715-66601039";
						- _umlDependencyID = "2807";
						- m_szSequence = "24.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "getCalendar()";
							- _id = GUID 912de468-53c6-4de7-85a4-59701e16be2f;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 1c9b88e1-e84b-4eb3-92fc-93221ce881cc;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID cd098cdf-53ee-4d0c-b846-afea4bff8355;
						}
					}
					{ IMessage 
						- _id = GUID 2242f92c-ea26-4953-a836-138e69bace0c;
						- _name = "getCalendar";
						- _objectCreation = "39353172717201715-66621039";
						- _umlDependencyID = "2811";
						- m_szSequence = "18.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "getCalendar()";
							- _id = GUID 912de468-53c6-4de7-85a4-59701e16be2f;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 7eca4162-2672-42cc-a7a6-79ae95a929e2;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 98f5be72-2629-42f5-a050-608ca13df442;
						}
					}
					{ IMessage 
						- _id = GUID a0ec9ace-8c52-49d8-9509-4c9cfc8a786f;
						- _name = "getCalendar";
						- _objectCreation = "39353192717201715-66641039";
						- _umlDependencyID = "2815";
						- m_szSequence = "12.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "getCalendar()";
							- _id = GUID 912de468-53c6-4de7-85a4-59701e16be2f;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 1da37a37-5b4a-4c03-b5cb-988d3a8846db;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 0329d223-dd94-4818-90e7-2bfda59c081e;
						}
					}
					{ IMessage 
						- _id = GUID 37351402-f123-44f0-bf85-dd59b82915df;
						- _name = "listOfEvents";
						- _objectCreation = "39353212717201715-66661039";
						- _umlDependencyID = "2950";
						- m_szSequence = "25.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "listOfEvents()";
							- _id = GUID 03f4fd26-5eb2-4a68-b547-910dcefa4f96;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 11591690-237b-4e96-be2b-2d264989949d;
						- _name = "listOfEvents";
						- _objectCreation = "39353232717201715-66681039";
						- _umlDependencyID = "2954";
						- m_szSequence = "13.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "listOfEvents()";
							- _id = GUID 03f4fd26-5eb2-4a68-b547-910dcefa4f96;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 359d4b11-4be9-4a1f-86e0-e154e5f9f625;
						- _name = "listOfEvents";
						- _objectCreation = "39353252717201715-66701039";
						- _umlDependencyID = "2949";
						- m_szSequence = "19.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "listOfEvents()";
							- _id = GUID 03f4fd26-5eb2-4a68-b547-910dcefa4f96;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID c42cbbc6-9f5d-47e1-8b62-71bba4896460;
						- _name = "getCalendar";
						- _objectCreation = "39353272717201715-66721039";
						- _umlDependencyID = "2813";
						- m_szSequence = "6.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "getCalendar()";
							- _id = GUID 912de468-53c6-4de7-85a4-59701e16be2f;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 17b65781-1e56-4ce9-820b-fbd47591ea1c;
						- _name = "ListOfEvents";
						- _objectCreation = "39353292717201715-66741039";
						- _umlDependencyID = "2925";
						- m_szSequence = "7.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID aa43293f-45c1-4ffc-b8ef-6e9cceb6593c;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Participant";
							- _name = "ListOfEvents()";
							- _id = GUID 76e319ba-9466-4f06-ad69-7c67f55afbd8;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 607e4769-7c15-4136-880c-b3d6117a6097;
						- _myState = 2048;
						- _name = "message_5";
						- _objectCreation = "39353312717201715-66761039";
						- _umlDependencyID = "2587";
						- m_szSequence = "27.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 9af6ff1c-bf8b-4e75-94b1-40104935fe43;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 1e4eb600-7425-4c09-a27f-d87f60af02ef;
						}
					}
					{ IMessage 
						- _id = GUID c21202ef-6db4-46c4-b7fe-4194cf65c62f;
						- _myState = 2048;
						- _name = "message_1";
						- _objectCreation = "39353332717201715-66781039";
						- _umlDependencyID = "2587";
						- m_szSequence = "4.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 2f2950dd-d3bf-4db9-93a3-3546089396dd;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 935d102a-44f0-4159-bc9e-0461319d5d7f;
						}
					}
					{ IMessage 
						- _id = GUID e95f011e-4337-422b-9112-c5d3c46987de;
						- _myState = 2048;
						- _name = "message_3";
						- _objectCreation = "39353352717201715-66801039";
						- _umlDependencyID = "2584";
						- m_szSequence = "15.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 30280c2a-96a9-4e09-baf9-d5aeec135738;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 0082d5f5-a2c6-4532-93a4-3a66a4a3671f;
						}
					}
					{ IMessage 
						- _id = GUID f4acb4a1-0e0c-4be0-bdd2-9016569e27e2;
						- _name = "generateParticipantTimeList";
						- _objectCreation = "39353372717201715-66821039";
						- _umlDependencyID = "4506";
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "generateParticipantTimeList()";
							- _id = GUID c4270fa0-9401-4359-b655-b541a6442dcc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 6b69fa55-603a-4531-a404-a0b5ca156f45;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID d6a7b3f0-59a2-422b-9ee6-8f312de59e9c;
						- _myState = 2048;
						- _name = "message_6";
						- _objectCreation = "39353392717201715-66841039";
						- _umlDependencyID = "2595";
						- m_szSequence = "31.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 7ec247bb-b2de-470b-a4a2-a09f91742ee5;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 22ad4968-931e-4798-ba9f-5ad9e633375f;
						}
					}
					{ IMessage 
						- _id = GUID 50ba5570-f885-4304-ab8a-fa6a236624b4;
						- _name = "updatePotentialEventTimes";
						- _objectCreation = "39353412717201715-66861039";
						- _umlDependencyID = "4315";
						- m_szSequence = "8.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_freeText = "      ";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1e838d8f-8adf-4fe3-8fe4-c6e37d3c64f3;
						- _name = "addParticipant";
						- _objectCreation = "39353432717201715-66881039";
						- _umlDependencyID = "3152";
						- m_szSequence = "10.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "addParticipant()";
							- _id = GUID 3ebd7736-e904-4284-badd-2db9fd91e6b8;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 0a8ef074-19a5-481d-85c9-1d6793ce5b36;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID d6a39868-ef75-454e-8c00-cf943b5c3f34;
						}
					}
					{ IMessage 
						- _id = GUID c096e416-9f2d-42bd-81dc-dfed1926f333;
						- _myState = 2048;
						- _name = "message_4";
						- _objectCreation = "39353452717201715-66901039";
						- _umlDependencyID = "2587";
						- m_szSequence = "21.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 4c2eaa71-f307-4dfa-95af-4f9f2e2c85cd;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID f8ac78ad-4bd0-45f4-b7bb-fb746d02140c;
						}
					}
					{ IMessage 
						- _id = GUID 05a6eabf-1c24-478e-8892-1e18dd25b865;
						- _name = "updatePotentialEventTimes";
						- _objectCreation = "39353472717201715-66921039";
						- _umlDependencyID = "4318";
						- m_szSequence = "14.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0ba137ed-da88-4088-adb2-d9d4dce94aad;
						- _name = "removeParticipant";
						- _objectCreation = "39353492717201715-66941039";
						- _umlDependencyID = "3512";
						- m_szSequence = "16.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "removeParticipant()";
							- _id = GUID 39b47fda-16b3-4484-82b2-70e44f2faadc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 66128448-6dca-432c-8caf-cbda5c78bdd7;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 863a8c05-2b44-4b9c-b104-7ae7e0505513;
						- _name = "updatePotentialEventTimes";
						- _objectCreation = "39353512717201715-66961039";
						- _umlDependencyID = "4317";
						- m_szSequence = "20.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 38208e74-31b3-48bb-9fca-734ffef8e543;
						- _myState = 2048;
						- _name = "message_0";
						- _objectCreation = "39353532717201715-66981039";
						- _umlDependencyID = "2590";
						- m_szSequence = "30.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID edd6f336-f045-4963-a280-5ded7db3273b;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 3f88fe48-f53a-4949-8378-e44665b2fb79;
						}
					}
					{ IMessage 
						- _id = GUID 94c545a1-09dc-4483-9ecd-8cd0cc4edaf8;
						- _name = "selectTime";
						- _objectCreation = "39353552717201715-67001039";
						- _umlDependencyID = "2731";
						- m_szSequence = "22.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "selectTime()";
							- _id = GUID 34382fdd-e372-46ef-950f-1f35f1247357;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 2f6952a0-621c-4d18-bf68-15ef978ebede;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3305e38d-69f9-42d6-96ff-e459451e7fb0;
						- _name = "updateAvailableRooms";
						- _objectCreation = "39353572717201715-67021039";
						- _umlDependencyID = "3764";
						- m_szSequence = "26.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a04c280e-baad-4dcd-a4b4-1b601915431b;
						- _myState = 2048;
						- _name = "message_2";
						- _objectCreation = "39353592717201715-67041039";
						- _umlDependencyID = "2586";
						- m_szSequence = "9.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 157db84e-76ef-4459-8a5d-7593ad92b899;
						- _name = "selectRoom";
						- _objectCreation = "39353612717201715-67061039";
						- _umlDependencyID = "2748";
						- m_szSequence = "28.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "selectRoom()";
							- _id = GUID 58674a86-4353-4194-8c0c-e54525bb3834;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID d563490f-d302-4298-8fa9-b93cd648dd5c;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0c699b91-fc81-4a5d-938a-3f6059765e08;
						- _name = "displayLocationField";
						- _objectCreation = "39353632717201715-67081039";
						- _umlDependencyID = "3766";
						- m_szSequence = "29.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 14b0f569-1cbf-47c2-927b-3aa18f69c063;
						- _myState = 2048;
						- _name = "message_7";
						- _objectCreation = "39353652717201715-67101039";
						- _umlDependencyID = "2585";
						- m_szSequence = "33.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID a0eb8600-b64c-491b-a61a-5f74f64a8e5d;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 3288e83a-92ec-4b27-a2ef-548d5a7d0ed7;
						}
					}
					{ IMessage 
						- _id = GUID f8e6fabf-7f86-496e-ba1e-0005f3fe3c87;
						- _name = "navToEditEvent";
						- _objectCreation = "39353672717201715-67121039";
						- _umlDependencyID = "3122";
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a6d2549a-37aa-4d7a-b5fa-4e13b783daab;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToEditEvent()";
							- _id = GUID 6259ec7e-eaf7-4fd4-a8ab-548465d2dd17;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID abf5f27e-702a-474b-b5ca-bdcadb70f3a1;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 5dd4f894-0c7c-4b2d-94ac-3e1a438e7d2a;
						}
					}
					{ IMessage 
						- _id = GUID 75b256d3-800e-4cd2-a297-a25102cc6117;
						- _name = "navToHome";
						- _objectCreation = "39353692717201715-67141039";
						- _umlDependencyID = "2615";
						- m_szSequence = "32.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 926937a5-311c-4c99-a688-561ad70b5c47;
						- _name = "navToHome";
						- _objectCreation = "39353712717201715-67161039";
						- _umlDependencyID = "2610";
						- m_szSequence = "35.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 53b0fe4e-5ad9-473b-8df0-dc7e3a8316ed;
						- _name = "cancelEditEvent";
						- _objectCreation = "39353732717201715-67181039";
						- _umlDependencyID = "3219";
						- m_szSequence = "34.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "cancelEditEvent()";
							- _id = GUID 91b612bc-4981-4536-8660-9ce7de09a77a;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID c4c418c5-af08-4287-ab4d-a04d1c416e1d;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 34ec029a-cf37-490d-83c4-7fd8a9672229;
						- _name = "getEventDetails";
						- _objectCreation = "39353752717201715-67201039";
						- _umlDependencyID = "3240";
						- m_szSequence = "2.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "getEventDetails()";
							- _id = GUID 6cbd1459-9997-4fc3-a927-4fdb1748356f;
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 052dbe7f-af59-45cd-9808-bb5ae40b4b75;
						- _name = "printEventDetails";
						- _objectCreation = "39353772717201715-67221039";
						- _umlDependencyID = "3481";
						- m_szSequence = "3.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a02671ab-451d-49c1-86a4-e76cc018b4db;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID c35bd989-67cd-4b6e-a8e9-280cf2fd1977;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = REPLY;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- ExecutionOccurrences = { IRPYRawContainer 
					- size = 29;
					- value = 
					{ IExecutionOccurrence 
						- _id = GUID 1c9b88e1-e84b-4eb3-92fc-93221ce881cc;
						- _objectCreation = "39353792717201715-67241039";
						- _umlDependencyID = "1704";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 343dd569-d711-4698-b75a-81519c15a859;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID cd098cdf-53ee-4d0c-b846-afea4bff8355;
						- _objectCreation = "39353812717201715-67261039";
						- _umlDependencyID = "1699";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 343dd569-d711-4698-b75a-81519c15a859;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 20;
					}
					{ IExecutionOccurrence 
						- _id = GUID 7eca4162-2672-42cc-a7a6-79ae95a929e2;
						- _objectCreation = "39353832717201715-67281039";
						- _umlDependencyID = "1703";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 2242f92c-ea26-4953-a836-138e69bace0c;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 98f5be72-2629-42f5-a050-608ca13df442;
						- _objectCreation = "39353852717201715-67301039";
						- _umlDependencyID = "1698";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 2242f92c-ea26-4953-a836-138e69bace0c;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 20;
					}
					{ IExecutionOccurrence 
						- _id = GUID 1da37a37-5b4a-4c03-b5cb-988d3a8846db;
						- _objectCreation = "39353872717201715-67321039";
						- _umlDependencyID = "1702";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID a0ec9ace-8c52-49d8-9509-4c9cfc8a786f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 0329d223-dd94-4818-90e7-2bfda59c081e;
						- _objectCreation = "39353892717201715-67341039";
						- _umlDependencyID = "1706";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID a0ec9ace-8c52-49d8-9509-4c9cfc8a786f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 20;
					}
					{ IExecutionOccurrence 
						- _id = GUID 9af6ff1c-bf8b-4e75-94b1-40104935fe43;
						- _objectCreation = "39353912717201715-67361039";
						- _umlDependencyID = "1701";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 607e4769-7c15-4136-880c-b3d6117a6097;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 153;
					}
					{ IExecutionOccurrence 
						- _id = GUID 1e4eb600-7425-4c09-a27f-d87f60af02ef;
						- _objectCreation = "39353932717201715-67381039";
						- _umlDependencyID = "1705";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 607e4769-7c15-4136-880c-b3d6117a6097;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 2f2950dd-d3bf-4db9-93a3-3546089396dd;
						- _objectCreation = "39353952717201715-67401039";
						- _umlDependencyID = "1700";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c21202ef-6db4-46c4-b7fe-4194cf65c62f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 92;
					}
					{ IExecutionOccurrence 
						- _id = GUID 935d102a-44f0-4159-bc9e-0461319d5d7f;
						- _objectCreation = "39353972717201715-67421039";
						- _umlDependencyID = "1704";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c21202ef-6db4-46c4-b7fe-4194cf65c62f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 30280c2a-96a9-4e09-baf9-d5aeec135738;
						- _objectCreation = "39353992717201715-67441039";
						- _umlDependencyID = "1708";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID e95f011e-4337-422b-9112-c5d3c46987de;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 155;
					}
					{ IExecutionOccurrence 
						- _id = GUID 0082d5f5-a2c6-4532-93a4-3a66a4a3671f;
						- _objectCreation = "39354012717201715-67461039";
						- _umlDependencyID = "1694";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID e95f011e-4337-422b-9112-c5d3c46987de;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 6b69fa55-603a-4531-a404-a0b5ca156f45;
						- _objectCreation = "39354032717201715-67481039";
						- _umlDependencyID = "1698";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID f4acb4a1-0e0c-4be0-bdd2-9016569e27e2;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 73;
					}
					{ IExecutionOccurrence 
						- _id = GUID 7ec247bb-b2de-470b-a4a2-a09f91742ee5;
						- _objectCreation = "39354052717201715-67501039";
						- _umlDependencyID = "1693";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID d6a7b3f0-59a2-422b-9ee6-8f312de59e9c;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 153;
					}
					{ IExecutionOccurrence 
						- _id = GUID 22ad4968-931e-4798-ba9f-5ad9e633375f;
						- _objectCreation = "39354072717201715-67521039";
						- _umlDependencyID = "1697";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID d6a7b3f0-59a2-422b-9ee6-8f312de59e9c;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 0a8ef074-19a5-481d-85c9-1d6793ce5b36;
						- _objectCreation = "39354092717201715-67541039";
						- _umlDependencyID = "1701";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 1e838d8f-8adf-4fe3-8fe4-c6e37d3c64f3;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 122;
					}
					{ IExecutionOccurrence 
						- _id = GUID d6a39868-ef75-454e-8c00-cf943b5c3f34;
						- _objectCreation = "39354112717201715-67561039";
						- _umlDependencyID = "1696";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 1e838d8f-8adf-4fe3-8fe4-c6e37d3c64f3;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 131;
					}
					{ IExecutionOccurrence 
						- _id = GUID 4c2eaa71-f307-4dfa-95af-4f9f2e2c85cd;
						- _objectCreation = "39354132717201715-67581039";
						- _umlDependencyID = "1700";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c096e416-9f2d-42bd-81dc-dfed1926f333;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 118;
					}
					{ IExecutionOccurrence 
						- _id = GUID f8ac78ad-4bd0-45f4-b7bb-fb746d02140c;
						- _objectCreation = "39354152717201715-67601039";
						- _umlDependencyID = "1695";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c096e416-9f2d-42bd-81dc-dfed1926f333;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 66128448-6dca-432c-8caf-cbda5c78bdd7;
						- _objectCreation = "39354172717201715-67621039";
						- _umlDependencyID = "1699";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 0ba137ed-da88-4088-adb2-d9d4dce94aad;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 125;
					}
					{ IExecutionOccurrence 
						- _id = GUID edd6f336-f045-4963-a280-5ded7db3273b;
						- _objectCreation = "39354192717201715-67641039";
						- _umlDependencyID = "1703";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 38208e74-31b3-48bb-9fca-734ffef8e543;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 3f88fe48-f53a-4949-8378-e44665b2fb79;
						- _objectCreation = "39354212717201715-67661039";
						- _umlDependencyID = "1698";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 38208e74-31b3-48bb-9fca-734ffef8e543;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 2f6952a0-621c-4d18-bf68-15ef978ebede;
						- _objectCreation = "39354232717201715-67681039";
						- _umlDependencyID = "1702";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 94c545a1-09dc-4483-9ecd-8cd0cc4edaf8;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 107;
					}
					{ IExecutionOccurrence 
						- _id = GUID d563490f-d302-4298-8fa9-b93cd648dd5c;
						- _objectCreation = "39354252717201715-67701039";
						- _umlDependencyID = "1697";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 157db84e-76ef-4459-8a5d-7593ad92b899;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 142;
					}
					{ IExecutionOccurrence 
						- _id = GUID a0eb8600-b64c-491b-a61a-5f74f64a8e5d;
						- _objectCreation = "39354272717201715-67721039";
						- _umlDependencyID = "1701";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 14b0f569-1cbf-47c2-927b-3aa18f69c063;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 73;
					}
					{ IExecutionOccurrence 
						- _id = GUID 3288e83a-92ec-4b27-a2ef-548d5a7d0ed7;
						- _objectCreation = "39354292717201715-67741039";
						- _umlDependencyID = "1705";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 14b0f569-1cbf-47c2-927b-3aa18f69c063;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID abf5f27e-702a-474b-b5ca-bdcadb70f3a1;
						- _objectCreation = "39354312717201715-67761039";
						- _umlDependencyID = "1700";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID f8e6fabf-7f86-496e-ba1e-0005f3fe3c87;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 53;
					}
					{ IExecutionOccurrence 
						- _id = GUID 5dd4f894-0c7c-4b2d-94ac-3e1a438e7d2a;
						- _objectCreation = "39354332717201715-67781039";
						- _umlDependencyID = "1704";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID f8e6fabf-7f86-496e-ba1e-0005f3fe3c87;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 53;
					}
					{ IExecutionOccurrence 
						- _id = GUID c4c418c5-af08-4287-ab4d-a04d1c416e1d;
						- _objectCreation = "39354352717201715-67801039";
						- _umlDependencyID = "1699";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 53b0fe4e-5ad9-473b-8df0-dc7e3a8316ed;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 41;
					}
				}
				- CombinedFragments = { IRPYRawContainer 
					- size = 7;
					- value = 
					{ ICombinedFragment 
						- _id = GUID fe37cdaf-3026-4bcf-903c-a5c21025a661;
						- _myState = 2048;
						- _name = "interactionOperator_0";
						- _objectCreation = "39354372717201715-67821039";
						- _umlDependencyID = "3874";
						- _interactionOperator = "Loop";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 48a56133-361d-4fbb-b535-358ec0e89f5b;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39354392717201715-67841039";
								- _umlDependencyID = "3747";
								- _interactionConstraint = "Adding Participants";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID 551b9661-2108-47d4-88b9-d797f37ff599;
						- _myState = 2048;
						- _name = "interactionOperator_1";
						- _objectCreation = "39354412717201715-67861039";
						- _umlDependencyID = "3874";
						- _interactionOperator = "Loop";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 6d5ed9b4-c1aa-4068-8155-59546d537e8a;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39354432717201715-67881039";
								- _umlDependencyID = "3746";
								- _interactionConstraint = "change event date";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID dec63be2-85ed-45e9-b5c1-4d0c5753d999;
						- _myState = 2048;
						- _name = "interactionOperator_2";
						- _objectCreation = "39354452717201715-67901039";
						- _umlDependencyID = "3874";
						- _interactionOperator = "Loop";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 0955b815-b17e-45d4-b4ae-07cc7b0139b8;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39354472717201715-67921039";
								- _umlDependencyID = "3745";
								- _interactionConstraint = "Removing Participants";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID 4891f429-dd15-4d1c-bcba-ce84b696bc9e;
						- _myState = 2048;
						- _name = "interactionOperator_3";
						- _objectCreation = "39354492717201715-67941039";
						- _umlDependencyID = "3883";
						- _interactionOperator = "Loop";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 83ae2505-b19e-4cf2-a06c-fdfe02a4fa7c;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39354512717201715-67961039";
								- _umlDependencyID = "3744";
								- _interactionConstraint = "Selecting Event Time";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID d7bd35cb-3dc3-42ca-b58f-0c6f0b0d26ac;
						- _myState = 2048;
						- _name = "interactionOperator_4";
						- _objectCreation = "39354532717201715-67981039";
						- _umlDependencyID = "3883";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID b5b52162-3e46-42e0-8e72-6534b318e864;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39354552717201715-68001039";
								- _umlDependencyID = "3734";
								- _interactionConstraint = "room == \"other\"";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID 643363c7-a9df-49f3-9012-f4eaf09c8697;
						- _myState = 2048;
						- _name = "interactionOperator_5";
						- _objectCreation = "39354572717201715-68021039";
						- _umlDependencyID = "3874";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 467214e9-293b-4e68-a1cc-3480e12d06d0;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39354592717201715-68041039";
								- _umlDependencyID = "3742";
								- _interactionConstraint = "user clicks save";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID ffd815a1-b9f3-4e14-a35c-4c703e85a92d;
						- _myState = 2048;
						- _name = "interactionOperator_6";
						- _objectCreation = "39354612717201715-68061039";
						- _umlDependencyID = "3874";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 51c242e1-04fc-41d0-8731-3e883ade2a3f;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39354632717201715-68081039";
								- _umlDependencyID = "3741";
								- _interactionConstraint = "user clicks cancel";
							}
						}
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID 9e83b1b9-3a5e-427a-ac91-2b7ad5a92631;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 8;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "CombinedFragment";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,250,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Condition_Mark";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,67,39";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "DataFlow";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "2";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "121,122,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InteractionOccurrence";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,216,134";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,16,230";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InteractionOperand";
								- Properties = { IRPYRawContainer 
									- size = 8;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,100,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Fill.Transparent_Fill";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "execution_occurrence";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
					{ IPropertySubject 
						- _Name = "SequenceDiagram";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "General";
								- Properties = { IRPYRawContainer 
									- size = 3;
									- value = 
									{ IProperty 
										- _Name = "ClassCentricMode";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "CleanupRealized";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "RealizeMessages";
										- _Value = "True";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "sequencediagram_DeleteEvent";
			- _objectCreation = "39354652717201715-68101039";
			- _umlDependencyID = "4482";
			- _lastModifiedTime = "10.27.2017::16:57:9";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 20;
				- m_usingActivationBar = 0;
				- _id = GUID a30211d2-c031-42e6-b85c-249f89d2472b;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 9e83b1b9-3a5e-427a-ac91-2b7ad5a92631;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 25;
				{ CGIBox 
					- _id = GUID 63a6de19-e025-45b3-ab61-633d3174c886;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID fda08405-8261-4464-bafb-d6c745854bb0;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "alt";
					- _id = GUID 0f25e073-9fbb-423a-8376-3b9b1443e157;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID d91c76c7-d7ed-4059-a34c-b764a74c81ba;
					}
					- m_pParent = GUID 63a6de19-e025-45b3-ab61-633d3174c886;
					- m_name = { CGIText 
						- m_str = "interactionOperator_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 36 372  535 372  535 597  36 597  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=115,110>
<frame Id=GUID c0a08d8a-6121-4d1b-bd17-7fbf6ad0ec10>
<frame Id=GUID 40b76669-a114-4024-8127-4671d1ca341d>";
				}
				{ CGIMscColumnCR 
					- _id = GUID 1bd8a55c-9a05-4b15-b633-ed1a13c734b7;
					- m_type = 97;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 99b3c0c0-58fc-49ee-bd12-d7d5ba8b5818;
					}
					- m_pParent = GUID 63a6de19-e025-45b3-ab61-633d3174c886;
					- m_name = { CGIText 
						- m_str = ":User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0147603 38 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
					}
					- m_pParent = GUID 63a6de19-e025-45b3-ab61-633d3174c886;
					- m_name = { CGIText 
						- m_str = ":GUIInterface";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0147603 206 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID d094bbd1-ad90-4a58-b738-f4897e9bb73e;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID d8102fd9-1a67-4873-b481-07644b08a0e6;
					}
					- m_pParent = GUID 63a6de19-e025-45b3-ab61-633d3174c886;
					- m_name = { CGIText 
						- m_str = ":Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0147603 397 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscMessage 
					- _id = GUID d05f574b-1d30-4604-b5db-7acd5bc5718b;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 790a5ea4-09f0-44ea-87a4-8e3a75bc6150;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "printEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d094bbd1-ad90-4a58-b738-f4897e9bb73e;
					- m_sourceType = 'F';
					- m_pTarget = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 8875 ;
					- m_TargetPort = 48 8875 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 878efd46-8979-4391-9258-ca1ee5a911d6;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 77e3ea39-0b06-4bea-bfeb-59d26decd37a;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 1bd8a55c-9a05-4b15-b633-ed1a13c734b7;
					- m_sourceType = 'F';
					- m_pTarget = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 10908 ;
					- m_TargetPort = 48 10908 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID afde9e03-a19a-47d0-85eb-971ca71d3bef;
					- m_pTargetExec = GUID a405b90c-6110-4623-91df-7880170fb330;
				}
				{ CGIMscMessage 
					- _id = GUID 4f07a5ec-1576-4656-b6f2-149ee2d6a4db;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID eba8a974-a83b-471c-b8c5-725ae829e5f7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "displayDeletePopUp()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_sourceType = 'F';
					- m_pTarget = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 284 226  284 256  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 11924 ;
					- m_TargetPort = 48 13956 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID cced1ed5-d55a-45b2-b77e-8ec2a36de1d1;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 061b60ef-29e0-4a67-8e23-dff40a7c549a;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 1bd8a55c-9a05-4b15-b633-ed1a13c734b7;
					- m_sourceType = 'F';
					- m_pTarget = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 6233 ;
					- m_TargetPort = 48 6233 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 2404c280-4a25-4911-bb08-3c221fdafe80;
					- m_pTargetExec = GUID 3b869e43-63b0-442b-9027-4acc93edd51b;
				}
				{ CGIMscMessage 
					- _id = GUID 30dfe47d-d457-492f-be2d-210f83af4a24;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 515f8986-b8e2-4b09-89e8-b8d8b0e5bf99;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getEventDetails()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_sourceType = 'F';
					- m_pTarget = GUID d094bbd1-ad90-4a58-b738-f4897e9bb73e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 7046 ;
					- m_TargetPort = 48 7046 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3e91b20e-c6a3-49c6-90a0-334356e76a14;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 2fc4b789-0616-4db8-a5ba-f86ae8410683;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_2";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 1bd8a55c-9a05-4b15-b633-ed1a13c734b7;
					- m_sourceType = 'F';
					- m_pTarget = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 25406 ;
					- m_TargetPort = 48 25406 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID d03e8bab-687d-4246-92ad-1a1cd414afc5;
					- m_pTargetExec = GUID 972c3b78-ff75-48e5-aa43-2f7adeed87d9;
				}
				{ CGIMscMessage 
					- _id = GUID 8b7ca413-b7b3-4a97-8d7b-b77ec3847718;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 97e52cdd-b179-492c-95ec-c87a288875a9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "deleteEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_sourceType = 'F';
					- m_pTarget = GUID d094bbd1-ad90-4a58-b738-f4897e9bb73e;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 26964 ;
					- m_TargetPort = 48 26964 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 127c27ef-faa4-4b9e-9b17-ee24886048f7;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7065f9e9-89c2-4d19-a216-3b2ec38ceaf7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToHome()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d094bbd1-ad90-4a58-b738-f4897e9bb73e;
					- m_sourceType = 'F';
					- m_pTarget = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 28455 ;
					- m_TargetPort = 48 28455 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID b72216aa-d41f-4cdf-adda-56873edf1eb8;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a0fbb249-0b5d-47e6-b7da-01072ae202f7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_3";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 1bd8a55c-9a05-4b15-b633-ed1a13c734b7;
					- m_sourceType = 'F';
					- m_pTarget = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 32723 ;
					- m_TargetPort = 48 32723 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID a79d6505-ec65-4bd9-b2d6-8072d3812aba;
					- m_pTargetExec = GUID fb25cc9d-1f9c-452a-9187-80ded6974092;
				}
				{ CGIMscMessage 
					- _id = GUID c225fadc-2cdf-4b09-833b-f846abbafe23;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7d2266e0-9ed8-4866-ae6f-9475d97c17e7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "closeDeletePopUp()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_sourceType = 'F';
					- m_pTarget = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 284 548  284 582  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 33739 ;
					- m_TargetPort = 48 36043 ;
					- m_bLeft = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 40b76669-a114-4024-8127-4671d1ca341d;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID e1480aa2-1d1f-4969-a0b1-0a6ad6b40a12;
					}
					- m_pParent = GUID 0f25e073-9fbb-423a-8376-3b9b1443e157;
					- m_name = { CGIText 
						- m_str = "else";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 21 -7  67 -7  67 11  21 11  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 244 498 ;
						- m_nHorizontalSpacing = 3;
						- m_nVerticalSpacing = 4;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 36 487  535 487  535 597  36 597  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID c0a08d8a-6121-4d1b-bd17-7fbf6ad0ec10;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 0fba2568-4b86-4e22-b09a-39871c19119a;
					}
					- m_pParent = GUID 0f25e073-9fbb-423a-8376-3b9b1443e157;
					- m_name = { CGIText 
						- m_str = "user selects yes";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 36 372  535 372  535 487  36 487  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 3b869e43-63b0-442b-9027-4acc93edd51b;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 4da2e34e-1b6f-416c-ac70-c47c232e4769;
					}
					- m_pParent = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 111.034 42 6232.96 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID cced1ed5-d55a-45b2-b77e-8ec2a36de1d1;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 2404c280-4a25-4911-bb08-3c221fdafe80;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 863745e2-b2c0-4015-93f9-1c536e27c91a;
					}
					- m_pParent = GUID 1bd8a55c-9a05-4b15-b633-ed1a13c734b7;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 67.7495 42 6232.96 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID cced1ed5-d55a-45b2-b77e-8ec2a36de1d1;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID a405b90c-6110-4623-91df-7880170fb330;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 9046ccc7-b292-41a6-ba4f-51d3858a0ccd;
					}
					- m_pParent = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 143.027 42 10907.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 878efd46-8979-4391-9258-ca1ee5a911d6;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID afde9e03-a19a-47d0-85eb-971ca71d3bef;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID bc24c9f6-959a-4bb2-9b09-7c84da7d541d;
					}
					- m_pParent = GUID 1bd8a55c-9a05-4b15-b633-ed1a13c734b7;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 67.7495 42 10907.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 878efd46-8979-4391-9258-ca1ee5a911d6;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 972c3b78-ff75-48e5-aa43-2f7adeed87d9;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 0ed99d92-6353-407a-af05-29a094eb59b6;
					}
					- m_pParent = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 122.326 42 25406.1 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 3e91b20e-c6a3-49c6-90a0-334356e76a14;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID d03e8bab-687d-4246-92ad-1a1cd414afc5;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID a59808c7-b02a-43b2-b506-8f851fb5cd25;
					}
					- m_pParent = GUID 1bd8a55c-9a05-4b15-b633-ed1a13c734b7;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 67.7495 42 25406.1 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 3e91b20e-c6a3-49c6-90a0-334356e76a14;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID fb25cc9d-1f9c-452a-9187-80ded6974092;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID f94bd442-9124-41e4-943b-f664bb187906;
					}
					- m_pParent = GUID a38bf231-b1e8-4302-ba83-32a78e2dcd24;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 114.798 42 32723 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID b72216aa-d41f-4cdf-adda-56873edf1eb8;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID a79d6505-ec65-4bd9-b2d6-8072d3812aba;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 997e03fc-173f-4bac-8652-185221b37325;
					}
					- m_pParent = GUID 1bd8a55c-9a05-4b15-b633-ed1a13c734b7;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 67.7495 42 32723 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID b72216aa-d41f-4cdf-adda-56873edf1eb8;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 63a6de19-e025-45b3-ab61-633d3174c886;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID fda08405-8261-4464-bafb-d6c745854bb0;
				- _objectCreation = "39354672717201715-68121039";
				- _umlDependencyID = "1700";
				- ClassifierRoles = { IRPYRawContainer 
					- size = 3;
					- value = 
					{ IClassifierRole 
						- _id = GUID 99b3c0c0-58fc-49ee-bd12-d7d5ba8b5818;
						- _myState = 2048;
						- _objectCreation = "39354692717201715-68141039";
						- _umlDependencyID = "1704";
						- m_eRoleType = ACTOR;
						- m_pBase = { IHandle 
							- _m2Class = "IActor";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "User";
							- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						- _myState = 2048;
						- _objectCreation = "39354712717201715-68161039";
						- _umlDependencyID = "1699";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "GUIInterface";
							- _id = GUID 4aa99777-6edc-45b5-8a77-41fc3f52b011;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID d8102fd9-1a67-4873-b481-07644b08a0e6;
						- _myState = 2048;
						- _objectCreation = "39354732717201715-68181039";
						- _umlDependencyID = "1703";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Controller";
							- _id = GUID fe66ef5a-2198-4f39-9e99-954c8f86b33b;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 10;
					- value = 
					{ IMessage 
						- _id = GUID 061b60ef-29e0-4a67-8e23-dff40a7c549a;
						- _myState = 2048;
						- _name = "message_0";
						- _objectCreation = "39354752717201715-68201039";
						- _umlDependencyID = "2582";
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 99b3c0c0-58fc-49ee-bd12-d7d5ba8b5818;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 4da2e34e-1b6f-416c-ac70-c47c232e4769;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 863745e2-b2c0-4015-93f9-1c536e27c91a;
						}
					}
					{ IMessage 
						- _id = GUID 515f8986-b8e2-4b09-89e8-b8d8b0e5bf99;
						- _name = "getEventDetails";
						- _objectCreation = "39354772717201715-68221039";
						- _umlDependencyID = "3246";
						- m_szSequence = "2.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d8102fd9-1a67-4873-b481-07644b08a0e6;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "getEventDetails()";
							- _id = GUID 88a18313-229f-4a20-9eaf-1599d7582da7;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 790a5ea4-09f0-44ea-87a4-8e3a75bc6150;
						- _name = "printEvent";
						- _objectCreation = "39354792717201715-68241039";
						- _umlDependencyID = "2777";
						- m_szSequence = "3.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d8102fd9-1a67-4873-b481-07644b08a0e6;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "printEvent()";
							- _id = GUID 1f096719-fb8b-4d15-b480-7de4e4bfaf4d;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 77e3ea39-0b06-4bea-bfeb-59d26decd37a;
						- _myState = 2048;
						- _name = "message_1";
						- _objectCreation = "39354812717201715-68261039";
						- _umlDependencyID = "2586";
						- m_szSequence = "4.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 99b3c0c0-58fc-49ee-bd12-d7d5ba8b5818;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 9046ccc7-b292-41a6-ba4f-51d3858a0ccd;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID bc24c9f6-959a-4bb2-9b09-7c84da7d541d;
						}
					}
					{ IMessage 
						- _id = GUID eba8a974-a83b-471c-b8c5-725ae829e5f7;
						- _name = "displayDeletePopUp";
						- _objectCreation = "39354832717201715-68281039";
						- _umlDependencyID = "3558";
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "displayDeletePopUp()";
							- _id = GUID 40650828-684f-4c4c-a5ee-f892a041d746;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 2fc4b789-0616-4db8-a5ba-f86ae8410683;
						- _myState = 2048;
						- _name = "message_2";
						- _objectCreation = "39354852717201715-68301039";
						- _umlDependencyID = "2586";
						- m_szSequence = "6.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 99b3c0c0-58fc-49ee-bd12-d7d5ba8b5818;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 0ed99d92-6353-407a-af05-29a094eb59b6;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID a59808c7-b02a-43b2-b506-8f851fb5cd25;
						}
					}
					{ IMessage 
						- _id = GUID 97e52cdd-b179-492c-95ec-c87a288875a9;
						- _name = "deleteEvent";
						- _objectCreation = "39354872717201715-68321039";
						- _umlDependencyID = "2845";
						- m_szSequence = "7.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d8102fd9-1a67-4873-b481-07644b08a0e6;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "deleteEvent()";
							- _id = GUID 4c9a3d68-cf07-49d4-bd98-f7efaf8318e7;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7065f9e9-89c2-4d19-a216-3b2ec38ceaf7;
						- _name = "navToHome";
						- _objectCreation = "39354892717201715-68341039";
						- _umlDependencyID = "2621";
						- m_szSequence = "8.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d8102fd9-1a67-4873-b481-07644b08a0e6;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToHome()";
							- _id = GUID 79508c7e-333a-4a2e-8a20-525746a301dd;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID a0fbb249-0b5d-47e6-b7da-01072ae202f7;
						- _myState = 2048;
						- _name = "message_3";
						- _objectCreation = "39354912717201715-68361039";
						- _umlDependencyID = "2590";
						- m_szSequence = "9.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 99b3c0c0-58fc-49ee-bd12-d7d5ba8b5818;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID f94bd442-9124-41e4-943b-f664bb187906;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 997e03fc-173f-4bac-8652-185221b37325;
						}
					}
					{ IMessage 
						- _id = GUID 7d2266e0-9ed8-4866-ae6f-9475d97c17e7;
						- _name = "closeDeletePopUp";
						- _objectCreation = "39354932717201715-68381039";
						- _umlDependencyID = "3336";
						- m_szSequence = "10.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID d5a08dd4-a59a-42d6-a481-073a2e5eff56;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "closeDeletePopUp()";
							- _id = GUID 6e2d3088-84f8-4792-9f4b-65bcb32b24b5;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- ExecutionOccurrences = { IRPYRawContainer 
					- size = 8;
					- value = 
					{ IExecutionOccurrence 
						- _id = GUID 4da2e34e-1b6f-416c-ac70-c47c232e4769;
						- _objectCreation = "39354952717201715-68401039";
						- _umlDependencyID = "1702";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 061b60ef-29e0-4a67-8e23-dff40a7c549a;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 59;
					}
					{ IExecutionOccurrence 
						- _id = GUID 863745e2-b2c0-4015-93f9-1c536e27c91a;
						- _objectCreation = "39354972717201715-68421039";
						- _umlDependencyID = "1706";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 061b60ef-29e0-4a67-8e23-dff40a7c549a;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 9046ccc7-b292-41a6-ba4f-51d3858a0ccd;
						- _objectCreation = "39354992717201715-68441039";
						- _umlDependencyID = "1710";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 77e3ea39-0b06-4bea-bfeb-59d26decd37a;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 76;
					}
					{ IExecutionOccurrence 
						- _id = GUID bc24c9f6-959a-4bb2-9b09-7c84da7d541d;
						- _objectCreation = "39355012717201715-68461039";
						- _umlDependencyID = "1696";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 77e3ea39-0b06-4bea-bfeb-59d26decd37a;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 0ed99d92-6353-407a-af05-29a094eb59b6;
						- _objectCreation = "39355032717201715-68481039";
						- _umlDependencyID = "1700";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 2fc4b789-0616-4db8-a5ba-f86ae8410683;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 65;
					}
					{ IExecutionOccurrence 
						- _id = GUID a59808c7-b02a-43b2-b506-8f851fb5cd25;
						- _objectCreation = "39355052717201715-68501039";
						- _umlDependencyID = "1695";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 2fc4b789-0616-4db8-a5ba-f86ae8410683;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID f94bd442-9124-41e4-943b-f664bb187906;
						- _objectCreation = "39355072717201715-68521039";
						- _umlDependencyID = "1699";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID a0fbb249-0b5d-47e6-b7da-01072ae202f7;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 61;
					}
					{ IExecutionOccurrence 
						- _id = GUID 997e03fc-173f-4bac-8652-185221b37325;
						- _objectCreation = "39355092717201715-68541039";
						- _umlDependencyID = "1703";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID a0fbb249-0b5d-47e6-b7da-01072ae202f7;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
				}
				- CombinedFragments = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ ICombinedFragment 
						- _id = GUID d91c76c7-d7ed-4059-a34c-b764a74c81ba;
						- _myState = 2048;
						- _name = "interactionOperator_0";
						- _objectCreation = "39355112717201715-68561039";
						- _umlDependencyID = "3869";
						- _interactionOperator = "alt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 2;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 0fba2568-4b86-4e22-b09a-39871c19119a;
								- _myState = 2048;
								- _name = "interactionOperand_1";
								- _objectCreation = "39355132717201715-68581039";
								- _umlDependencyID = "3743";
								- _interactionConstraint = "user selects yes";
							}
							{ IInteractionOperand 
								- _id = GUID e1480aa2-1d1f-4969-a0b1-0a6ad6b40a12;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39355152717201715-68601039";
								- _umlDependencyID = "3737";
								- _interactionConstraint = "else";
							}
						}
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID bf8dd359-261c-4758-bcbe-c1634f4f3294;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 4;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "DataFlow";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "2";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "121,122,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "execution_occurrence";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
					{ IPropertySubject 
						- _Name = "SequenceDiagram";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "General";
								- Properties = { IRPYRawContainer 
									- size = 3;
									- value = 
									{ IProperty 
										- _Name = "ClassCentricMode";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "CleanupRealized";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "RealizeMessages";
										- _Value = "True";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "sequencediagram_ViewDaily";
			- _objectCreation = "39355172717201715-68621039";
			- _umlDependencyID = "4288";
			- _lastModifiedTime = "10.27.2017::17:3:13";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 20;
				- m_usingActivationBar = 0;
				- _id = GUID 9d5396f3-aa1b-4fa2-b1ee-d473fb19696c;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID bf8dd359-261c-4758-bcbe-c1634f4f3294;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 15;
				{ CGIBox 
					- _id = GUID e221e725-ee96-424c-96fe-96bc6ca00423;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID 1b1b83c3-e56a-4953-b70c-0bed3fa0817d;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID d4aaa016-e2e6-43ad-9cb2-0a9091f21090;
					- m_type = 97;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 98339af5-971e-48fa-8ac8-b3841887e095;
					}
					- m_pParent = GUID e221e725-ee96-424c-96fe-96bc6ca00423;
					- m_name = { CGIText 
						- m_str = ":User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00876015 54 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
					}
					- m_pParent = GUID e221e725-ee96-424c-96fe-96bc6ca00423;
					- m_name = { CGIText 
						- m_str = ":GUIInterface";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00876015 221 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 5004a079-3072-4079-8a50-ffc344350b3b;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID a56e945c-e9ec-41d0-b604-ecb49b0f3af2;
					}
					- m_pParent = GUID e221e725-ee96-424c-96fe-96bc6ca00423;
					- m_name = { CGIText 
						- m_str = ":Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00876015 425 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 48ab67f5-e23a-4a46-8f8b-b04d24784cc4;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 54fe3461-b227-4716-9d17-4c2793aa7f8b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d4aaa016-e2e6-43ad-9cb2-0a9091f21090;
					- m_sourceType = 'F';
					- m_pTarget = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 11415 ;
					- m_TargetPort = 48 11415 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID f099d112-7cf2-4fb7-96fb-d726a2aecd4d;
					- m_pTargetExec = GUID 2576c2ca-10e6-4a16-b610-42ba994c1ca1;
				}
				{ CGIMscMessage 
					- _id = GUID 62b7618e-7b66-4ad2-a72f-2b42df279748;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 24093d38-5d75-4127-98b2-aeeb1e859f95;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "selectDay()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_sourceType = 'F';
					- m_pTarget = GUID 5004a079-3072-4079-8a50-ffc344350b3b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 14155 ;
					- m_TargetPort = 48 14155 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3ac14fad-dedd-4f03-90c2-4f4479965f3d;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 527abe55-7990-4707-8b98-72f656b976cd;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "printEventsForDay()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 5004a079-3072-4079-8a50-ffc344350b3b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 18036 ;
					- m_TargetPort = 48 18036 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 3920e919-43ac-40d4-86e7-aab967cdcc2b;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID fa050d58-4087-4375-8c3c-7502d63278ec;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d4aaa016-e2e6-43ad-9cb2-0a9091f21090;
					- m_sourceType = 'F';
					- m_pTarget = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 22146 ;
					- m_TargetPort = 48 22146 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 29cab28c-9b60-4b87-8bb7-27f49ab9f725;
					- m_pTargetExec = GUID 0e50ace6-57fd-4f69-8192-d008fe8037f8;
				}
				{ CGIMscMessage 
					- _id = GUID 0d9d389d-6a89-4541-94f0-53a681a1120a;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 5f0e38f3-8fd8-4355-95b6-9d2fbe3ef09f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_sourceType = 'F';
					- m_pTarget = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 299 263  299 302  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 24315 ;
					- m_TargetPort = 48 28767 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 4cb645f9-94ca-4cbb-84ad-cbdf379da6d3;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0e16ebaf-37bd-484e-9aca-b256ac72cbd0;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getEventDetails()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_sourceType = 'F';
					- m_pTarget = GUID 5004a079-3072-4079-8a50-ffc344350b3b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 31050 ;
					- m_TargetPort = 48 31050 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 015b3de6-eddc-4a6e-a0ed-a2320b17300f;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 1a5c0c7b-89b0-4194-b489-2bd4247b430f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "printEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 5004a079-3072-4079-8a50-ffc344350b3b;
					- m_sourceType = 'F';
					- m_pTarget = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 34018 ;
					- m_TargetPort = 48 34018 ;
					- m_bLeft = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 2576c2ca-10e6-4a16-b610-42ba994c1ca1;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 41406b4f-0027-4bb9-9ab6-b1130c3cf236;
					}
					- m_pParent = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 247.332 42 11415.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 48ab67f5-e23a-4a46-8f8b-b04d24784cc4;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID f099d112-7cf2-4fb7-96fb-d726a2aecd4d;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID ce196de0-c687-4592-a62d-c0986fc6ae16;
					}
					- m_pParent = GUID d4aaa016-e2e6-43ad-9cb2-0a9091f21090;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 114.153 42 11415.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 48ab67f5-e23a-4a46-8f8b-b04d24784cc4;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 0e50ace6-57fd-4f69-8192-d008fe8037f8;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID ea0c07b2-e8c6-4f67-9326-dee8a0f9ee0a;
					}
					- m_pParent = GUID d550857e-882b-4cf5-a5ab-b7d8910444d7;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 393.195 42 22145.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 3920e919-43ac-40d4-86e7-aab967cdcc2b;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 29cab28c-9b60-4b87-8bb7-27f49ab9f725;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID a48004f8-b70b-4cba-87fb-a722bf2c3e22;
					}
					- m_pParent = GUID d4aaa016-e2e6-43ad-9cb2-0a9091f21090;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 114.153 42 22145.7 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 3920e919-43ac-40d4-86e7-aab967cdcc2b;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID e221e725-ee96-424c-96fe-96bc6ca00423;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID 1b1b83c3-e56a-4953-b70c-0bed3fa0817d;
				- _objectCreation = "39355192717201715-68641039";
				- _umlDependencyID = "1705";
				- ClassifierRoles = { IRPYRawContainer 
					- size = 3;
					- value = 
					{ IClassifierRole 
						- _id = GUID 98339af5-971e-48fa-8ac8-b3841887e095;
						- _myState = 2048;
						- _objectCreation = "39355212717201715-68661039";
						- _umlDependencyID = "1700";
						- m_eRoleType = ACTOR;
						- m_pBase = { IHandle 
							- _m2Class = "IActor";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "User";
							- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
						- _myState = 2048;
						- _objectCreation = "39355232717201715-68681039";
						- _umlDependencyID = "1704";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "GUIInterface";
							- _id = GUID 4aa99777-6edc-45b5-8a77-41fc3f52b011;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID a56e945c-e9ec-41d0-b604-ecb49b0f3af2;
						- _myState = 2048;
						- _objectCreation = "39355252717201715-68701039";
						- _umlDependencyID = "1699";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Controller";
							- _id = GUID fe66ef5a-2198-4f39-9e99-954c8f86b33b;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 7;
					- value = 
					{ IMessage 
						- _id = GUID 54fe3461-b227-4716-9d17-4c2793aa7f8b;
						- _myState = 2048;
						- _name = "message_0";
						- _objectCreation = "39355272717201715-68721039";
						- _umlDependencyID = "2587";
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 98339af5-971e-48fa-8ac8-b3841887e095;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 41406b4f-0027-4bb9-9ab6-b1130c3cf236;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID ce196de0-c687-4592-a62d-c0986fc6ae16;
						}
					}
					{ IMessage 
						- _id = GUID 24093d38-5d75-4127-98b2-aeeb1e859f95;
						- _name = "selectDay";
						- _objectCreation = "39355292717201715-68741039";
						- _umlDependencyID = "2633";
						- m_szSequence = "2.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a56e945c-e9ec-41d0-b604-ecb49b0f3af2;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "selectDay()";
							- _id = GUID 3b25f7c5-ee36-4e4b-ab70-e09ca588438a;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 527abe55-7990-4707-8b98-72f656b976cd;
						- _name = "printEventsForDay";
						- _objectCreation = "39355312717201715-68761039";
						- _umlDependencyID = "3469";
						- m_szSequence = "3.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a56e945c-e9ec-41d0-b604-ecb49b0f3af2;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "printEventsForDay()";
							- _id = GUID 715836ee-6444-4b63-91d5-fd8e18f5f1cc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID fa050d58-4087-4375-8c3c-7502d63278ec;
						- _myState = 2048;
						- _name = "message_1";
						- _objectCreation = "39355332717201715-68781039";
						- _umlDependencyID = "2591";
						- m_szSequence = "4.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 98339af5-971e-48fa-8ac8-b3841887e095;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID ea0c07b2-e8c6-4f67-9326-dee8a0f9ee0a;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID a48004f8-b70b-4cba-87fb-a722bf2c3e22;
						}
					}
					{ IMessage 
						- _id = GUID 5f0e38f3-8fd8-4355-95b6-9d2fbe3ef09f;
						- _name = "navToEvent";
						- _objectCreation = "39355352717201715-68801039";
						- _umlDependencyID = "2735";
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToEvent()";
							- _id = GUID a2a23b50-2a4c-4c52-b564-2068d2ab646c;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 0e16ebaf-37bd-484e-9aca-b256ac72cbd0;
						- _name = "getEventDetails";
						- _objectCreation = "39355372717201715-68821039";
						- _umlDependencyID = "3249";
						- m_szSequence = "6.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a56e945c-e9ec-41d0-b604-ecb49b0f3af2;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "getEventDetails()";
							- _id = GUID 88a18313-229f-4a20-9eaf-1599d7582da7;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 1a5c0c7b-89b0-4194-b489-2bd4247b430f;
						- _name = "printEvent";
						- _objectCreation = "39355392717201715-68841039";
						- _umlDependencyID = "2780";
						- m_szSequence = "7.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 150c8f10-ad90-4e4b-bd93-985ebde63a08;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a56e945c-e9ec-41d0-b604-ecb49b0f3af2;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "printEvent()";
							- _id = GUID 1f096719-fb8b-4d15-b480-7de4e4bfaf4d;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- ExecutionOccurrences = { IRPYRawContainer 
					- size = 4;
					- value = 
					{ IExecutionOccurrence 
						- _id = GUID 41406b4f-0027-4bb9-9ab6-b1130c3cf236;
						- _objectCreation = "39355412717201715-68861039";
						- _umlDependencyID = "1704";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 54fe3461-b227-4716-9d17-4c2793aa7f8b;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 78;
					}
					{ IExecutionOccurrence 
						- _id = GUID ce196de0-c687-4592-a62d-c0986fc6ae16;
						- _objectCreation = "39355432717201715-68881039";
						- _umlDependencyID = "1708";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 54fe3461-b227-4716-9d17-4c2793aa7f8b;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID ea0c07b2-e8c6-4f67-9326-dee8a0f9ee0a;
						- _objectCreation = "39355452717201715-68901039";
						- _umlDependencyID = "1703";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID fa050d58-4087-4375-8c3c-7502d63278ec;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 124;
					}
					{ IExecutionOccurrence 
						- _id = GUID a48004f8-b70b-4cba-87fb-a722bf2c3e22;
						- _objectCreation = "39355472717201715-68921039";
						- _umlDependencyID = "1707";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID fa050d58-4087-4375-8c3c-7502d63278ec;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID 9577c8b9-2bb0-4a5f-8de5-fd2d613f5084;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 7;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "CombinedFragment";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,250,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "DataFlow";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "2";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "EnvironmentLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "121,122,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InteractionOperand";
								- Properties = { IRPYRawContainer 
									- size = 8;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,100,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Fill.Transparent_Fill";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "execution_occurrence";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
					{ IPropertySubject 
						- _Name = "SequenceDiagram";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "General";
								- Properties = { IRPYRawContainer 
									- size = 3;
									- value = 
									{ IProperty 
										- _Name = "ClassCentricMode";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "CleanupRealized";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "RealizeMessages";
										- _Value = "True";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "sequencediagram_ViewMonthly";
			- _objectCreation = "39355492717201715-68941039";
			- _umlDependencyID = "4546";
			- _lastModifiedTime = "10.27.2017::19:8:59";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 19;
				- m_usingActivationBar = 0;
				- _id = GUID e371433e-4bc5-4756-8128-b7b490073974;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 9577c8b9-2bb0-4a5f-8de5-fd2d613f5084;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 22;
				{ CGIBox 
					- _id = GUID d0d24f27-512c-4631-8294-4caba85b0732;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID cf1a2f9c-f090-4c1a-a042-c7bce51a5c72;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID caf8033c-075f-4fa8-9b2d-45f81c1c2bb2;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 9a64e4c7-b4cb-42de-b30c-035612b8acab;
					}
					- m_pParent = GUID d0d24f27-512c-4631-8294-4caba85b0732;
					- m_name = { CGIText 
						- m_str = "interactionOperator_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 117 400  117 579  633 579  633 400  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=179>
<frame Id=GUID d6c7df16-2b7b-4e33-8e9f-b3edabfbcc4c>";
				}
				{ CGIMscColumnCR 
					- _id = GUID e84a098d-5937-4900-b91b-7c3bfb956fed;
					- m_type = 97;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID ea53aaa4-6fb3-4381-a5bb-ca05d1df5965;
					}
					- m_pParent = GUID d0d24f27-512c-4631-8294-4caba85b0732;
					- m_name = { CGIText 
						- m_str = ":User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0147603 117 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
					}
					- m_pParent = GUID d0d24f27-512c-4631-8294-4caba85b0732;
					- m_name = { CGIText 
						- m_str = ":GUIInterface";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0147603 284 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 5f92ae8e-39fc-4518-ac6d-7aa1f9c4271c;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 84b03b7f-103e-402b-ac04-ec8c729a2f68;
					}
					- m_pParent = GUID d0d24f27-512c-4631-8294-4caba85b0732;
					- m_name = { CGIText 
						- m_str = ":Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0147603 488 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 9d27ec3b-11d2-421a-b455-3e260fd4888f;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID e608a5cf-0b26-4771-9e5d-b4cb545e197d;
					}
					- m_pParent = GUID e84a098d-5937-4900-b91b-7c3bfb956fed;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 67.7493 42 26151.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 5b8eb081-45b8-4202-9633-ec65b1ae02a0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 519ff392-60a2-43bb-8197-20027ecb39f8;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID a0bd3247-c4af-451b-9df1-8ca68d285c07;
					}
					- m_pParent = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 233.36 42 26151.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 5b8eb081-45b8-4202-9633-ec65b1ae02a0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 4cec760f-f722-4148-9a0c-bd9546414a49;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 656f8390-fdae-45cf-bb9b-8792ac5b0574;
					}
					- m_pParent = GUID e84a098d-5937-4900-b91b-7c3bfb956fed;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 67.7493 42 6774.93 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID dcde35ad-0a32-4fe8-ad17-69cf74e99abe;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID f6807335-f5e8-4884-9b72-b30ecacb9d5e;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 4d9ecdd4-2e83-45b7-9887-89838af59146;
					}
					- m_pParent = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 146.791 42 6774.93 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID dcde35ad-0a32-4fe8-ad17-69cf74e99abe;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 9637c5af-6b8d-42ed-9958-65dfa5cdffa1;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 89284d5b-201a-4e60-b98e-9cb3c1a9dae6;
					}
					- m_pParent = GUID e84a098d-5937-4900-b91b-7c3bfb956fed;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 67.7495 42 14091.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 6a5ec755-4eb2-4905-8f23-e18924a1b626;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 795e77cb-72f7-4ea8-be0b-062f99df1831;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 003a5c72-d61e-47a9-b4f7-0d3793893eae;
					}
					- m_pParent = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 227.714 42 14091.9 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 6a5ec755-4eb2-4905-8f23-e18924a1b626;
				}
				{ CGIMscMessage 
					- _id = GUID 89eeca1d-7853-49ce-b9df-3cb86e33c5eb;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID fc721136-7760-450e-b7cf-6e8268c304f4;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "printEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 5f92ae8e-39fc-4518-ac6d-7aa1f9c4271c;
					- m_sourceType = 'F';
					- m_pTarget = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 33197 ;
					- m_TargetPort = 48 33197 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 18af9429-3ca1-4ef0-8a6a-5c3c68b26c11;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f3b29ae7-bb5e-4bef-aba7-54863362c060;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToEvent()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_sourceType = 'F';
					- m_pTarget = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 362 455  362 494  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 27439 ;
					- m_TargetPort = 48 30081 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 5b8eb081-45b8-4202-9633-ec65b1ae02a0;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID c0a4ab41-27ae-4ef8-9c39-1c49464f7f72;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e84a098d-5937-4900-b91b-7c3bfb956fed;
					- m_sourceType = 'F';
					- m_pTarget = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 26151 ;
					- m_TargetPort = 48 26151 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 9d27ec3b-11d2-421a-b455-3e260fd4888f;
					- m_pTargetExec = GUID 519ff392-60a2-43bb-8197-20027ecb39f8;
				}
				{ CGIMscMessage 
					- _id = GUID dcde35ad-0a32-4fe8-ad17-69cf74e99abe;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 78a1efe5-df87-4fc2-a2f7-0622928ab8ae;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e84a098d-5937-4900-b91b-7c3bfb956fed;
					- m_sourceType = 'F';
					- m_pTarget = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 6775 ;
					- m_TargetPort = 48 6775 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 4cec760f-f722-4148-9a0c-bd9546414a49;
					- m_pTargetExec = GUID f6807335-f5e8-4884-9b72-b30ecacb9d5e;
				}
				{ CGIMscMessage 
					- _id = GUID 6a5ec755-4eb2-4905-8f23-e18924a1b626;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 8b23dbb9-68a7-47c5-8805-94b4e45b6b70;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_2";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID e84a098d-5937-4900-b91b-7c3bfb956fed;
					- m_sourceType = 'F';
					- m_pTarget = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 14092 ;
					- m_TargetPort = 48 14092 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 9637c5af-6b8d-42ed-9958-65dfa5cdffa1;
					- m_pTargetExec = GUID 795e77cb-72f7-4ea8-be0b-062f99df1831;
				}
				{ CGIMscMessage 
					- _id = GUID 7250e936-81b9-49d1-9a83-65a104be0e1a;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID e462fecf-1f8c-4e63-ba6f-f6809624ee84;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToMonthView()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_sourceType = 'F';
					- m_pTarget = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 362 270  362 300  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 14905 ;
					- m_TargetPort = 48 16937 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 267161e6-496a-4a7a-907a-dbc22da9a4f8;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9117cacc-2297-429c-8a4b-ce325205c71f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getEventsForMonth()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_sourceType = 'F';
					- m_pTarget = GUID 5f92ae8e-39fc-4518-ac6d-7aa1f9c4271c;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 18225 ;
					- m_TargetPort = 48 18225 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 853a97f3-e2ba-4e20-897f-96a51c6de982;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 4b1716f1-2144-47e2-90b5-36f87bc694a8;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "printEventsForMonth()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 5f92ae8e-39fc-4518-ac6d-7aa1f9c4271c;
					- m_sourceType = 'F';
					- m_pTarget = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 20393 ;
					- m_TargetPort = 48 20393 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 23550771-4483-4e25-8ec8-eaf941721508;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ce7deedd-4ca9-4cec-9c0d-d22a80b558a7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "getEventDetails()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_sourceType = 'F';
					- m_pTarget = GUID 5f92ae8e-39fc-4518-ac6d-7aa1f9c4271c;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 31436 ;
					- m_TargetPort = 48 31436 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 9c896710-4fcb-402b-bbfa-beeab58d91c9;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7b43443f-0728-4e23-892b-6ef2d1487ca9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "printEventsForDay()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_sourceType = 'F';
					- m_pTarget = GUID 69d71bbd-1ed0-4955-89a2-32ca3341a2d8;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 362 174  362 208  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 8401 ;
					- m_TargetPort = 48 10704 ;
					- m_bLeft = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID d6c7df16-2b7b-4e33-8e9f-b3edabfbcc4c;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID c9f5d5b3-b2f9-43f3-a554-25dceea4d00f;
					}
					- m_pParent = GUID caf8033c-075f-4fa8-9b2d-45f81c1c2bb2;
					- m_name = { CGIText 
						- m_str = "user clicks on an event";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 117 400  633 400  633 579  117 579  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID d0d24f27-512c-4631-8294-4caba85b0732;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID cf1a2f9c-f090-4c1a-a042-c7bce51a5c72;
				- _objectCreation = "39355512717201715-68961039";
				- _umlDependencyID = "1706";
				- ClassifierRoles = { IRPYRawContainer 
					- size = 3;
					- value = 
					{ IClassifierRole 
						- _id = GUID ea53aaa4-6fb3-4381-a5bb-ca05d1df5965;
						- _myState = 2048;
						- _objectCreation = "39355532717201715-68981039";
						- _umlDependencyID = "1710";
						- m_eRoleType = ACTOR;
						- m_pBase = { IHandle 
							- _m2Class = "IActor";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "User";
							- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						- _myState = 2048;
						- _objectCreation = "39355552717201715-69001039";
						- _umlDependencyID = "1696";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "GUIInterface";
							- _id = GUID 4aa99777-6edc-45b5-8a77-41fc3f52b011;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 84b03b7f-103e-402b-ac04-ec8c729a2f68;
						- _myState = 2048;
						- _objectCreation = "39355572717201715-69021039";
						- _umlDependencyID = "1700";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Controller";
							- _id = GUID fe66ef5a-2198-4f39-9e99-954c8f86b33b;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 10;
					- value = 
					{ IMessage 
						- _id = GUID 78a1efe5-df87-4fc2-a2f7-0622928ab8ae;
						- _myState = 2048;
						- _name = "message_0";
						- _objectCreation = "39355592717201715-69041039";
						- _umlDependencyID = "2588";
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID ea53aaa4-6fb3-4381-a5bb-ca05d1df5965;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 4d9ecdd4-2e83-45b7-9887-89838af59146;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 656f8390-fdae-45cf-bb9b-8792ac5b0574;
						}
					}
					{ IMessage 
						- _id = GUID c0a4ab41-27ae-4ef8-9c39-1c49464f7f72;
						- _myState = 2048;
						- _name = "message_1";
						- _objectCreation = "39355612717201715-69061039";
						- _umlDependencyID = "2584";
						- m_szSequence = "7.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID ea53aaa4-6fb3-4381-a5bb-ca05d1df5965;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID a0bd3247-c4af-451b-9df1-8ca68d285c07;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID e608a5cf-0b26-4771-9e5d-b4cb545e197d;
						}
					}
					{ IMessage 
						- _id = GUID f3b29ae7-bb5e-4bef-aba7-54863362c060;
						- _name = "navToEvent";
						- _objectCreation = "39355632717201715-69081039";
						- _umlDependencyID = "2737";
						- m_szSequence = "8.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToEvent()";
							- _id = GUID a2a23b50-2a4c-4c52-b564-2068d2ab646c;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 7b43443f-0728-4e23-892b-6ef2d1487ca9;
						- _name = "printEventsForDay";
						- _objectCreation = "39355652717201715-69101039";
						- _umlDependencyID = "3465";
						- m_szSequence = "2.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "printEventsForDay()";
							- _id = GUID 715836ee-6444-4b63-91d5-fd8e18f5f1cc;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ce7deedd-4ca9-4cec-9c0d-d22a80b558a7;
						- _name = "getEventDetails";
						- _objectCreation = "39355672717201715-69121039";
						- _umlDependencyID = "3246";
						- m_szSequence = "9.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 84b03b7f-103e-402b-ac04-ec8c729a2f68;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "getEventDetails()";
							- _id = GUID 88a18313-229f-4a20-9eaf-1599d7582da7;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID fc721136-7760-450e-b7cf-6e8268c304f4;
						- _name = "printEvent";
						- _objectCreation = "39355692717201715-69141039";
						- _umlDependencyID = "2777";
						- m_szSequence = "10.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 84b03b7f-103e-402b-ac04-ec8c729a2f68;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "printEvent()";
							- _id = GUID 1f096719-fb8b-4d15-b480-7de4e4bfaf4d;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 8b23dbb9-68a7-47c5-8805-94b4e45b6b70;
						- _myState = 2048;
						- _name = "message_2";
						- _objectCreation = "39355712717201715-69161039";
						- _umlDependencyID = "2587";
						- m_szSequence = "3.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID ea53aaa4-6fb3-4381-a5bb-ca05d1df5965;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 003a5c72-d61e-47a9-b4f7-0d3793893eae;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 89284d5b-201a-4e60-b98e-9cb3c1a9dae6;
						}
					}
					{ IMessage 
						- _id = GUID e462fecf-1f8c-4e63-ba6f-f6809624ee84;
						- _name = "navToMonthView";
						- _objectCreation = "39355732717201715-69181039";
						- _umlDependencyID = "3154";
						- m_szSequence = "4.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToMonthView()";
							- _id = GUID f5969ad3-3c58-423f-882f-0ebd4239664c;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 9117cacc-2297-429c-8a4b-ce325205c71f;
						- _name = "getEventsForMonth";
						- _objectCreation = "39355752717201715-69201039";
						- _umlDependencyID = "3462";
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 84b03b7f-103e-402b-ac04-ec8c729a2f68;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "getEventsForMonth()";
							- _id = GUID 6feda786-3214-469a-9746-af669aabd717;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 4b1716f1-2144-47e2-90b5-36f87bc694a8;
						- _name = "printEventsForMonth";
						- _objectCreation = "39355772717201715-69221039";
						- _umlDependencyID = "3703";
						- m_szSequence = "6.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID db86905e-8f64-4212-a09a-1777c5ea6003;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 84b03b7f-103e-402b-ac04-ec8c729a2f68;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "printEventsForMonth()";
							- _id = GUID 4e8e2b03-5e45-47e6-a291-988c40bbb987;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- ExecutionOccurrences = { IRPYRawContainer 
					- size = 6;
					- value = 
					{ IExecutionOccurrence 
						- _id = GUID 4d9ecdd4-2e83-45b7-9887-89838af59146;
						- _objectCreation = "39355792717201715-69241039";
						- _umlDependencyID = "1708";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 78a1efe5-df87-4fc2-a2f7-0622928ab8ae;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 78;
					}
					{ IExecutionOccurrence 
						- _id = GUID 656f8390-fdae-45cf-bb9b-8792ac5b0574;
						- _objectCreation = "39355812717201715-69261039";
						- _umlDependencyID = "1703";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 78a1efe5-df87-4fc2-a2f7-0622928ab8ae;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID a0bd3247-c4af-451b-9df1-8ca68d285c07;
						- _objectCreation = "39355832717201715-69281039";
						- _umlDependencyID = "1707";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c0a4ab41-27ae-4ef8-9c39-1c49464f7f72;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 124;
					}
					{ IExecutionOccurrence 
						- _id = GUID e608a5cf-0b26-4771-9e5d-b4cb545e197d;
						- _objectCreation = "39355852717201715-69301039";
						- _umlDependencyID = "1702";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID c0a4ab41-27ae-4ef8-9c39-1c49464f7f72;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 003a5c72-d61e-47a9-b4f7-0d3793893eae;
						- _objectCreation = "39355872717201715-69321039";
						- _umlDependencyID = "1706";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 8b23dbb9-68a7-47c5-8805-94b4e45b6b70;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 121;
					}
					{ IExecutionOccurrence 
						- _id = GUID 89284d5b-201a-4e60-b98e-9cb3c1a9dae6;
						- _objectCreation = "39355892717201715-69341039";
						- _umlDependencyID = "1710";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 8b23dbb9-68a7-47c5-8805-94b4e45b6b70;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
				}
				- CombinedFragments = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ ICombinedFragment 
						- _id = GUID 9a64e4c7-b4cb-42de-b30c-035612b8acab;
						- _myState = 2048;
						- _name = "interactionOperator_0";
						- _objectCreation = "39355912717201715-69361039";
						- _umlDependencyID = "3876";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID c9f5d5b3-b2f9-43f3-a554-25dceea4d00f;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39355932717201715-69381039";
								- _umlDependencyID = "3749";
								- _interactionConstraint = "user clicks on an event";
							}
						}
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID f13cb82f-01d3-4138-af2f-a046da2c9af8;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 6;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "CombinedFragment";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,250,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "DataFlow";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "2";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "121,122,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InteractionOperand";
								- Properties = { IRPYRawContainer 
									- size = 8;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,100,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Fill.Transparent_Fill";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "execution_occurrence";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
					{ IPropertySubject 
						- _Name = "SequenceDiagram";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "General";
								- Properties = { IRPYRawContainer 
									- size = 3;
									- value = 
									{ IProperty 
										- _Name = "ClassCentricMode";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "CleanupRealized";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "RealizeMessages";
										- _Value = "True";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "sequencediagram_Login";
			- _objectCreation = "39355952717201715-69401039";
			- _umlDependencyID = "3886";
			- _lastModifiedTime = "10.27.2017::18:46:11";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 20;
				- m_usingActivationBar = 0;
				- _id = GUID 75c2e810-2369-41c8-b8c1-7db9436228c4;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID f13cb82f-01d3-4138-af2f-a046da2c9af8;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 17;
				{ CGIBox 
					- _id = GUID 2ee77064-7de7-4296-ad79-311fa3334704;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID 5c9a5889-585c-4196-847c-c5cd279ce4ef;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID 1ad998d8-ad05-4558-a87c-5623451e3014;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 2fcfedf8-c84b-4112-8e75-25629bc66fe9;
					}
					- m_pParent = GUID 2ee77064-7de7-4296-ad79-311fa3334704;
					- m_name = { CGIText 
						- m_str = "interactionOperator_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 24 252  24 424  514 424  514 252  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=71,101>
<frame Id=GUID 493f52ed-26f3-411c-9201-8bcf0a8e6bb8>
<frame Id=GUID aff0cad8-cd1f-4837-a279-dc96d512c49e>";
				}
				{ CGIMscColumnCR 
					- _id = GUID 6e1add10-0c89-4334-aaed-740a17f71ac3;
					- m_type = 97;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 2bebbe4d-62f9-477c-b733-c474714acaf8;
					}
					- m_pParent = GUID 2ee77064-7de7-4296-ad79-311fa3334704;
					- m_name = { CGIText 
						- m_str = ":User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0117602 26 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 58743b39-da94-48ab-b3af-ccf7da5b2c05;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 0f9b9c54-9f33-42b2-bb2b-b4eaf1b424cb;
					}
					- m_pParent = GUID 2ee77064-7de7-4296-ad79-311fa3334704;
					- m_name = { CGIText 
						- m_str = ":GUIInterface";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0117602 173 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 87fdc2fa-02dd-4c99-8171-ab1ccec88a43;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 4e6b1f68-3b06-497a-8f01-260b07657ccf;
					}
					- m_pParent = GUID 2ee77064-7de7-4296-ad79-311fa3334704;
					- m_name = { CGIText 
						- m_str = ":Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0117602 333 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID a8b4abc5-9a50-452c-b62e-7edd477090a4;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID d0230d7a-eb42-46ca-8462-e0f38f05529f;
					}
					- m_pParent = GUID 6e1add10-0c89-4334-aaed-740a17f71ac3;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 85.0326 42 8418.22 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 8bf8ad04-469d-4f47-b76f-24255fb2503f;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID fa66bbad-577f-45f3-93a1-bf736b6ae011;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID a7fbf7fc-db55-4a80-ac48-a8d1b8c6c2b1;
					}
					- m_pParent = GUID 58743b39-da94-48ab-b3af-ccf7da5b2c05;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 85.0326 42 8418.22 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 8bf8ad04-469d-4f47-b76f-24255fb2503f;
				}
				{ CGIMscMessage 
					- _id = GUID 78ed4368-db6c-4fb8-9e29-696fe2bf4492;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 7abdab93-77a4-4b0e-b5f2-0136fe1220a5;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "login()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 58743b39-da94-48ab-b3af-ccf7da5b2c05;
					- m_sourceType = 'F';
					- m_pTarget = GUID 87fdc2fa-02dd-4c99-8171-ab1ccec88a43;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 14711 ;
					- m_TargetPort = 48 14711 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 47cba578-933a-4c9f-b7b8-a7377f2e817f;
				}
				{ CGIMscMessage 
					- _id = GUID 2e8e636b-cfa9-40e8-987a-8ae1a182e499;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f4091de1-4cc1-4e6c-b76f-f712f3eb5825;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToHome()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 87fdc2fa-02dd-4c99-8171-ab1ccec88a43;
					- m_sourceType = 'F';
					- m_pTarget = GUID 58743b39-da94-48ab-b3af-ccf7da5b2c05;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 21003 ;
					- m_TargetPort = 48 21003 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 119daeee-0f1a-425c-8d9e-3185cbc906be;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID ca73eb39-256f-4a05-9039-8177558e9477;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "displayLoginFailure()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 4;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 87fdc2fa-02dd-4c99-8171-ab1ccec88a43;
					- m_sourceType = 'F';
					- m_pTarget = GUID 58743b39-da94-48ab-b3af-ccf7da5b2c05;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 27721 ;
					- m_TargetPort = 48 27721 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 7968d7ce-043c-41e9-9c4b-8b75b9be40bc;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID d3f2c1d9-3781-4763-b0c5-412720c4a7e9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 6e1add10-0c89-4334-aaed-740a17f71ac3;
					- m_sourceType = 'F';
					- m_pTarget = GUID 58743b39-da94-48ab-b3af-ccf7da5b2c05;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 13180 ;
					- m_TargetPort = 48 13180 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID f19842ca-5ef6-4cd2-80f8-cad7d7cf2b91;
					- m_pTargetExec = GUID b2fec202-fef0-4423-aedd-84c39ad5869d;
				}
				{ CGIMscMessage 
					- _id = GUID 8bf8ad04-469d-4f47-b76f-24255fb2503f;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID f861cbe9-1ce5-43ae-b4a6-1b187da37665;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 6e1add10-0c89-4334-aaed-740a17f71ac3;
					- m_sourceType = 'F';
					- m_pTarget = GUID 58743b39-da94-48ab-b3af-ccf7da5b2c05;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 8418 ;
					- m_TargetPort = 48 8418 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID a8b4abc5-9a50-452c-b62e-7edd477090a4;
					- m_pTargetExec = GUID fa66bbad-577f-45f3-93a1-bf736b6ae011;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID aff0cad8-cd1f-4837-a279-dc96d512c49e;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 8526d575-3a2b-4a62-b9be-170e0851ae15;
					}
					- m_pParent = GUID 1ad998d8-ad05-4558-a87c-5623451e3014;
					- m_name = { CGIText 
						- m_str = "Login Failure";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 24 323  514 323  514 424  24 424  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 493f52ed-26f3-411c-9201-8bcf0a8e6bb8;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID fb15cf8b-046f-420b-adda-2fab7e114a31;
					}
					- m_pParent = GUID 1ad998d8-ad05-4558-a87c-5623451e3014;
					- m_name = { CGIText 
						- m_str = "Login Success";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nHorizontalSpacing = 5;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 24 252  514 252  514 323  24 323  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID b2fec202-fef0-4423-aedd-84c39ad5869d;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 1dfe104e-c713-4f11-aee9-d93f7e366076;
					}
					- m_pParent = GUID 58743b39-da94-48ab-b3af-ccf7da5b2c05;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 462.955 42 13180 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 7968d7ce-043c-41e9-9c4b-8b75b9be40bc;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID f19842ca-5ef6-4cd2-80f8-cad7d7cf2b91;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 10f67e4c-6b39-4ec2-9e67-763859eda86f;
					}
					- m_pParent = GUID 6e1add10-0c89-4334-aaed-740a17f71ac3;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 85.0326 42 13180 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 7968d7ce-043c-41e9-9c4b-8b75b9be40bc;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 47cba578-933a-4c9f-b7b8-a7377f2e817f;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 1e80273c-690a-4433-99a0-98544ed6a162;
					}
					- m_pParent = GUID 87fdc2fa-02dd-4c99-8171-ab1ccec88a43;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 410.991 42 14710.6 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 78ed4368-db6c-4fb8-9e29-696fe2bf4492;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 2ee77064-7de7-4296-ad79-311fa3334704;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID 5c9a5889-585c-4196-847c-c5cd279ce4ef;
				- _objectCreation = "39355972717201715-69421039";
				- _umlDependencyID = "1708";
				- ClassifierRoles = { IRPYRawContainer 
					- size = 3;
					- value = 
					{ IClassifierRole 
						- _id = GUID 2bebbe4d-62f9-477c-b733-c474714acaf8;
						- _myState = 2048;
						- _objectCreation = "39355992717201715-69441039";
						- _umlDependencyID = "1712";
						- m_eRoleType = ACTOR;
						- m_pBase = { IHandle 
							- _m2Class = "IActor";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "User";
							- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 0f9b9c54-9f33-42b2-bb2b-b4eaf1b424cb;
						- _myState = 2048;
						- _objectCreation = "39356012717201715-69461039";
						- _umlDependencyID = "1698";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "GUIInterface";
							- _id = GUID 4aa99777-6edc-45b5-8a77-41fc3f52b011;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 4e6b1f68-3b06-497a-8f01-260b07657ccf;
						- _myState = 2048;
						- _objectCreation = "39356032717201715-69481039";
						- _umlDependencyID = "1702";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Controller";
							- _id = GUID fe66ef5a-2198-4f39-9e99-954c8f86b33b;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 5;
					- value = 
					{ IMessage 
						- _id = GUID f861cbe9-1ce5-43ae-b4a6-1b187da37665;
						- _myState = 2048;
						- _name = "message_0";
						- _objectCreation = "39356052717201715-69501039";
						- _umlDependencyID = "2581";
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 0f9b9c54-9f33-42b2-bb2b-b4eaf1b424cb;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 2bebbe4d-62f9-477c-b733-c474714acaf8;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID a7fbf7fc-db55-4a80-ac48-a8d1b8c6c2b1;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID d0230d7a-eb42-46ca-8462-e0f38f05529f;
						}
					}
					{ IMessage 
						- _id = GUID d3f2c1d9-3781-4763-b0c5-412720c4a7e9;
						- _myState = 2048;
						- _name = "message_1";
						- _objectCreation = "39356072717201715-69521039";
						- _umlDependencyID = "2586";
						- m_szSequence = "2.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 0f9b9c54-9f33-42b2-bb2b-b4eaf1b424cb;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 2bebbe4d-62f9-477c-b733-c474714acaf8;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 1dfe104e-c713-4f11-aee9-d93f7e366076;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 10f67e4c-6b39-4ec2-9e67-763859eda86f;
						}
					}
					{ IMessage 
						- _id = GUID 7abdab93-77a4-4b0e-b5f2-0136fe1220a5;
						- _name = "login";
						- _objectCreation = "39356092717201715-69541039";
						- _umlDependencyID = "2242";
						- m_szSequence = "3.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 4e6b1f68-3b06-497a-8f01-260b07657ccf;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 0f9b9c54-9f33-42b2-bb2b-b4eaf1b424cb;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "login()";
							- _id = GUID b8885941-35cd-459d-b161-d8592944a666;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 1e80273c-690a-4433-99a0-98544ed6a162;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID fec3d69f-9f4b-4c4b-b851-2d0c267000e4;
						}
					}
					{ IMessage 
						- _id = GUID f4091de1-4cc1-4e6c-b76f-f712f3eb5825;
						- _name = "navToHome";
						- _objectCreation = "39356112717201715-69561039";
						- _umlDependencyID = "2613";
						- m_szSequence = "4.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 0f9b9c54-9f33-42b2-bb2b-b4eaf1b424cb;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 4e6b1f68-3b06-497a-8f01-260b07657ccf;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToHome()";
							- _id = GUID 79508c7e-333a-4a2e-8a20-525746a301dd;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID ca73eb39-256f-4a05-9039-8177558e9477;
						- _name = "displayLoginFailure";
						- _objectCreation = "39356132717201715-69581039";
						- _umlDependencyID = "3679";
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 0f9b9c54-9f33-42b2-bb2b-b4eaf1b424cb;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 4e6b1f68-3b06-497a-8f01-260b07657ccf;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "displayLoginFailure()";
							- _id = GUID dfca098c-fb32-4764-8d56-6c333077b7ca;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- ExecutionOccurrences = { IRPYRawContainer 
					- size = 6;
					- value = 
					{ IExecutionOccurrence 
						- _id = GUID a7fbf7fc-db55-4a80-ac48-a8d1b8c6c2b1;
						- _objectCreation = "39356152717201715-69601039";
						- _umlDependencyID = "1699";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID f861cbe9-1ce5-43ae-b4a6-1b187da37665;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID d0230d7a-eb42-46ca-8462-e0f38f05529f;
						- _objectCreation = "39356172717201715-69621039";
						- _umlDependencyID = "1703";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID f861cbe9-1ce5-43ae-b4a6-1b187da37665;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 1dfe104e-c713-4f11-aee9-d93f7e366076;
						- _objectCreation = "39356192717201715-69641039";
						- _umlDependencyID = "1707";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID d3f2c1d9-3781-4763-b0c5-412720c4a7e9;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 196;
					}
					{ IExecutionOccurrence 
						- _id = GUID 10f67e4c-6b39-4ec2-9e67-763859eda86f;
						- _objectCreation = "39356212717201715-69661039";
						- _umlDependencyID = "1702";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID d3f2c1d9-3781-4763-b0c5-412720c4a7e9;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 1e80273c-690a-4433-99a0-98544ed6a162;
						- _objectCreation = "39356232717201715-69681039";
						- _umlDependencyID = "1706";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 7abdab93-77a4-4b0e-b5f2-0136fe1220a5;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 174;
					}
					{ IExecutionOccurrence 
						- _id = GUID fec3d69f-9f4b-4c4b-b851-2d0c267000e4;
						- _objectCreation = "39356252717201715-69701039";
						- _umlDependencyID = "1701";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 7abdab93-77a4-4b0e-b5f2-0136fe1220a5;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
				}
				- CombinedFragments = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ ICombinedFragment 
						- _id = GUID 2fcfedf8-c84b-4112-8e75-25629bc66fe9;
						- _myState = 2048;
						- _name = "interactionOperator_0";
						- _objectCreation = "39356272717201715-69721039";
						- _umlDependencyID = "3876";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 2;
							- value = 
							{ IInteractionOperand 
								- _id = GUID fb15cf8b-046f-420b-adda-2fab7e114a31;
								- _myState = 2048;
								- _name = "interactionOperand_1";
								- _objectCreation = "39356292717201715-69741039";
								- _umlDependencyID = "3750";
								- _interactionConstraint = "Login Success";
							}
							{ IInteractionOperand 
								- _id = GUID 8526d575-3a2b-4a62-b9be-170e0851ae15;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39356312717201715-69761039";
								- _umlDependencyID = "3744";
								- _interactionConstraint = "Login Failure";
							}
						}
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID 3b471251-eddb-4b48-b2bd-d02cbf994e07;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 7;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "CombinedFragment";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,250,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "DataFlow";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "2";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "EnvironmentLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "121,122,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InteractionOperand";
								- Properties = { IRPYRawContainer 
									- size = 8;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,100,150";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Fill.Transparent_Fill";
										- _Value = "1";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "execution_occurrence";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
					{ IPropertySubject 
						- _Name = "SequenceDiagram";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "General";
								- Properties = { IRPYRawContainer 
									- size = 3;
									- value = 
									{ IProperty 
										- _Name = "ClassCentricMode";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "CleanupRealized";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "RealizeMessages";
										- _Value = "True";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "sequencediagram_Register";
			- _objectCreation = "39356332717201715-69781039";
			- _umlDependencyID = "4222";
			- _lastModifiedTime = "10.27.2017::18:6:24";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 3;
				- m_usingActivationBar = 0;
				- _id = GUID ef9a8555-594c-418b-90f4-0fe14b3ba651;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 3b471251-eddb-4b48-b2bd-d02cbf994e07;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 31;
				{ CGIBox 
					- _id = GUID bb6e8505-837e-4df0-bd5c-21536c7061b0;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID e8b0feed-c782-434a-a061-4428871eefa0;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID 031054af-a7b8-461b-b05d-642fb298a882;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 813b70ce-9150-4a27-95bb-29d672a4b6a5;
					}
					- m_pParent = GUID bb6e8505-837e-4df0-bd5c-21536c7061b0;
					- m_name = { CGIText 
						- m_str = "interactionOperator_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 1 0 0 1 190 0 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 56 240  56 546  805 546  805 240  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=306>
<frame Id=GUID 3eab58cd-c38b-4d44-be49-0d792b23fd97>";
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "opt";
					- _id = GUID 483facdb-75ad-4999-ba29-9260117e7074;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 13127f18-61bc-41a4-8b58-a5be4295a056;
					}
					- m_pParent = GUID bb6e8505-837e-4df0-bd5c-21536c7061b0;
					- m_name = { CGIText 
						- m_str = "interactionOperator_2";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 0.982955 0 0 1 196.818 -15 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 48 564  48 679  400 679  400 564  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=115>
<frame Id=GUID 93d0157f-4cc2-485d-a19a-b665824d34ee>";
				}
				{ CGIMscColumnCR 
					- _id = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
					}
					- m_pParent = GUID bb6e8505-837e-4df0-bd5c-21536c7061b0;
					- m_name = { CGIText 
						- m_str = ":GUIInterface";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0164779 415 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 68578be7-2b4e-442e-9635-99e10fa3d525;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 3f7ad3ae-2a48-4c5f-827b-2444ed5fc304;
					}
					- m_pParent = GUID bb6e8505-837e-4df0-bd5c-21536c7061b0;
					- m_name = { CGIText 
						- m_str = ":Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0164779 568 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 71ee3f3d-28bd-4c5c-b157-0d1305d14203;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID fd0b563c-f9bf-4797-9e11-d5e95c72eed7;
					}
					- m_pParent = GUID bb6e8505-837e-4df0-bd5c-21536c7061b0;
					- m_name = { CGIText 
						- m_str = ":Account";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0164779 726 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 8cfe1e0f-3e37-4881-a469-96887df15221;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID ab49559b-de2c-4015-ba2b-4b89305c881d;
					}
					- m_pParent = GUID bb6e8505-837e-4df0-bd5c-21536c7061b0;
					- m_name = { CGIText 
						- m_str = ":Contact";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0164779 890 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID d29d1908-a0e8-4c31-8cd2-39c240e76a56;
					- m_type = 97;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 274a80ae-8a00-4e42-b5bc-30e637496950;
					}
					- m_pParent = GUID bb6e8505-837e-4df0-bd5c-21536c7061b0;
					- m_name = { CGIText 
						- m_str = ":User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.0164779 247 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 04e40778-7070-43ae-a3d4-8b490cf23b56;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID f54f4917-c32d-43e4-8fd7-9ec295a6fb07;
					}
					- m_pParent = GUID 68578be7-2b4e-442e-9635-99e10fa3d525;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 84.288 42 15960.8 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID bfdbe34f-91a0-42b1-a793-36ee43f8afd9;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 6398fcb3-efd3-4cb1-9800-b9781283a83a;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 058412c7-886a-4eb6-ba17-48df704c1c90;
					}
					- m_pParent = GUID d29d1908-a0e8-4c31-8cd2-39c240e76a56;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 60.6873 42 13533.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 4d24e153-1c84-45c5-92f4-9f1c2a6bc6ae;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID b6c736bb-28a5-47de-918b-196095bce98a;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 774eec42-66e6-47e1-86bf-1dce84621ca5;
					}
					- m_pParent = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 396.154 42 13533.3 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 4d24e153-1c84-45c5-92f4-9f1c2a6bc6ae;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID e3f48b35-a046-45e0-b72b-cc3f78f55c9a;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID f1f2252b-5b68-493d-950f-aa115f1c55c3;
					}
					- m_pParent = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 60.6873 42 9406.54 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 170b6b18-6846-4997-b7a0-26baa90b957d;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID c1274816-d5e1-43a7-be47-53baee09d308;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID c07f20a3-f522-4cc7-8aca-9e4729ccd89c;
					}
					- m_pParent = GUID 71ee3f3d-28bd-4c5c-b157-0d1305d14203;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 60.6873 42 16810.4 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID b13a6bf0-9f20-4f02-8813-c8d87b1f746b;
				}
				{ CGIMscMessage 
					- _id = GUID ff7487b2-7b8d-46df-8791-d6cf0277a93d;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID e600ff8a-1031-479f-8e5e-33f7947378c5;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d29d1908-a0e8-4c31-8cd2-39c240e76a56;
					- m_sourceType = 'F';
					- m_pTarget = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 5462 ;
					- m_TargetPort = 48 5462 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 170b6b18-6846-4997-b7a0-26baa90b957d;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 298f3898-0b64-496c-965f-14a23d116e92;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToRegister()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_sourceType = 'F';
					- m_pTarget = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 493 158  493 205  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 6554 ;
					- m_TargetPort = 48 9407 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID e3f48b35-a046-45e0-b72b-cc3f78f55c9a;
				}
				{ CGIMscMessage 
					- _id = GUID 62e39c4d-b42d-4a87-aa68-7b4a6dd35336;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 09db02ac-4f6e-4d6c-9ecb-7dea97add11b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "displayRegistrationFail()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_sourceType = 'F';
					- m_pTarget = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 493 455  493 487  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 24578 ;
					- m_TargetPort = 48 26520 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 8ce02a6d-adde-4006-99b8-93dde76f0d05;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID efcf9b45-ec4e-4a84-9884-135c02c94329;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToLogin()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_sourceType = 'F';
					- m_pTarget = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 493 383  493 405  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 20209 ;
					- m_TargetPort = 48 21544 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 2a407f62-f6fc-43a5-9d4a-70001957f0cc;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 5133d87f-ed0d-479a-8c79-9e5f961750c7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToLogin()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_sourceType = 'F';
					- m_pTarget = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 493 602  493 628  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 33499 ;
					- m_TargetPort = 48 35077 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID b13a6bf0-9f20-4f02-8813-c8d87b1f746b;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 64fb80bf-3835-4847-ac51-80bab79f608f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "account()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 68578be7-2b4e-442e-9635-99e10fa3d525;
					- m_sourceType = 'F';
					- m_pTarget = GUID 71ee3f3d-28bd-4c5c-b157-0d1305d14203;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 16810 ;
					- m_TargetPort = 48 16810 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID c1274816-d5e1-43a7-be47-53baee09d308;
				}
				{ CGIMscMessage 
					- _id = GUID bfdbe34f-91a0-42b1-a793-36ee43f8afd9;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 33e916ef-2981-47c2-8f62-2cad95f374de;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "registerAccount()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 15 -8  86 -8  86 10  15 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 266 357 ;
						- m_nHorizontalSpacing = 8;
						- m_nVerticalSpacing = -7;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_sourceType = 'F';
					- m_pTarget = GUID 68578be7-2b4e-442e-9635-99e10fa3d525;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 15961 ;
					- m_TargetPort = 48 15961 ;
					- m_bLeft = 0;
					- m_pTargetExec = GUID 04e40778-7070-43ae-a3d4-8b490cf23b56;
				}
				{ CGIMscMessage 
					- _id = GUID 01bab574-a1f3-41f9-aa60-ddf6c9559805;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 26b3c767-64ac-4656-a101-d01f8d309b5f;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_3";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d29d1908-a0e8-4c31-8cd2-39c240e76a56;
					- m_sourceType = 'F';
					- m_pTarget = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 32893 ;
					- m_TargetPort = 48 32893 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 972f7c25-a9ae-4c0c-be53-3b7389b91af2;
					- m_pTargetExec = GUID 8d93c85c-440e-4d9e-9da3-306eda12b7a4;
				}
				{ CGIMscMessage 
					- _id = GUID 7297aac7-69ac-455f-b959-9452c486e057;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 9c2bf9a7-0f64-48a4-b431-a41c48ab14c9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "contact()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 71ee3f3d-28bd-4c5c-b157-0d1305d14203;
					- m_sourceType = 'F';
					- m_pTarget = GUID 8cfe1e0f-3e37-4881-a469-96887df15221;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 17478 ;
					- m_TargetPort = 48 17478 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 79f8e7b5-f31f-4839-9801-e2bcbdd51387;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 54ec4da6-880f-4671-88f8-91daa95004ee;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d29d1908-a0e8-4c31-8cd2-39c240e76a56;
					- m_sourceType = 'F';
					- m_pTarget = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 10135 ;
					- m_TargetPort = 48 10135 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID 4d24e153-1c84-45c5-92f4-9f1c2a6bc6ae;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 0fd89fc7-c7a5-4775-8ddc-224e7d3fae94;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_2";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID d29d1908-a0e8-4c31-8cd2-39c240e76a56;
					- m_sourceType = 'F';
					- m_pTarget = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 13533 ;
					- m_TargetPort = 48 13533 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID 6398fcb3-efd3-4cb1-9800-b9781283a83a;
					- m_pTargetExec = GUID b6c736bb-28a5-47de-918b-196095bce98a;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 3eab58cd-c38b-4d44-be49-0d792b23fd97;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 00ff4951-7808-4c64-ba48-c8844e063c12;
					}
					- m_pParent = GUID 031054af-a7b8-461b-b05d-642fb298a882;
					- m_name = { CGIText 
						- m_str = "User clicks submit";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -6 -9  74 -9  74 9  -6 9  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 1;
						- m_transform = 2.1625 0 0 1 283.975 255 ;
						- m_nHorizontalSpacing = -73;
						- m_nVerticalSpacing = 6;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 56 240  805 240  805 546  56 546  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperator 
					- m_operatorType = "alt";
					- _id = GUID 3e1d8644-a8cd-43f1-a8c9-725a7a79566f;
					- m_type = 196;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICombinedFragment";
						- _id = GUID 80fa0d27-7e5f-4d0d-b368-fd8984f44c87;
					}
					- m_pParent = GUID 031054af-a7b8-461b-b05d-642fb298a882;
					- m_name = { CGIText 
						- m_str = "interactionOperator_1";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_bFramesetModified = 1;
					- m_polygon = 4 71 286  782 286  782 526  71 526  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- frameset = "<frameset rows=128,112>
<frame Id=GUID 12ae061f-b656-4074-adf7-17f78ff94932>
<frame Id=GUID f16fb199-cb9a-45e8-a55f-e15f197ff136>";
				}
				{ CGIMscInteractionOperand 
					- _id = GUID f16fb199-cb9a-45e8-a55f-e15f197ff136;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 0e6d1c2b-fa36-42ab-b5c6-f9bc6084226d;
					}
					- m_pParent = GUID 3e1d8644-a8cd-43f1-a8c9-725a7a79566f;
					- m_name = { CGIText 
						- m_str = "Else";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 24 -15  72 -15  72 3  24 3  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 101 442 ;
						- m_nHorizontalSpacing = -277;
						- m_nVerticalSpacing = 13;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 71 414  782 414  782 526  71 526  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 12ae061f-b656-4074-adf7-17f78ff94932;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 5f1b46d7-05a6-4186-a067-6c28130abc88;
					}
					- m_pParent = GUID 3e1d8644-a8cd-43f1-a8c9-725a7a79566f;
					- m_name = { CGIText 
						- m_str = "if submission successful";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -43 -16  122 -16  122 32  -43 32  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 1;
						- m_transform = 1.19394 0 0 0.583333 144.339 299.333 ;
						- m_nHorizontalSpacing = -235;
						- m_nVerticalSpacing = 4;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 71 286  782 286  782 414  71 414  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscInteractionOperand 
					- _id = GUID 93d0157f-4cc2-485d-a19a-b665824d34ee;
					- m_type = 197;
					- m_pModelObject = { IHandle 
						- _m2Class = "IInteractionOperand";
						- _id = GUID 7286bb62-18f1-4b62-b8a4-880728e97d4f;
					}
					- m_pParent = GUID 483facdb-75ad-4999-ba29-9260117e7074;
					- m_name = { CGIText 
						- m_str = "user clicks cancel";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 4 -24 -8  106 -8  106 10  -24 10  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_transform = 1 0 0 1 151 575 ;
						- m_nHorizontalSpacing = -32;
						- m_nVerticalSpacing = 3;
						- m_nOrientationCtrlPt = 1;
					}
					- m_drawBehavior = 4104;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 48 564  400 564  400 679  48 679  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 8d93c85c-440e-4d9e-9da3-306eda12b7a4;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 485e1a2b-a1f9-4b96-84f0-7c09a8a3dff6;
					}
					- m_pParent = GUID d479dd8c-956d-4d47-b435-bf69e744e12f;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 89.3453 42 32892.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 01bab574-a1f3-41f9-aa60-ddf6c9559805;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 972f7c25-a9ae-4c0c-be53-3b7389b91af2;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 40012697-e354-4b16-99b1-30a6cef2faa3;
					}
					- m_pParent = GUID d29d1908-a0e8-4c31-8cd2-39c240e76a56;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 60.6873 42 32892.5 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID 01bab574-a1f3-41f9-aa60-ddf6c9559805;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID bb6e8505-837e-4df0-bd5c-21536c7061b0;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID e8b0feed-c782-434a-a061-4428871eefa0;
				- _objectCreation = "39356352717201715-69801039";
				- _umlDependencyID = "1703";
				- ClassifierRoles = { IRPYRawContainer 
					- size = 5;
					- value = 
					{ IClassifierRole 
						- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						- _myState = 2048;
						- _objectCreation = "39356372717201715-69821039";
						- _umlDependencyID = "1707";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "GUIInterface";
							- _id = GUID 4aa99777-6edc-45b5-8a77-41fc3f52b011;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 3f7ad3ae-2a48-4c5f-827b-2444ed5fc304;
						- _myState = 2048;
						- _objectCreation = "39356392717201715-69841039";
						- _umlDependencyID = "1711";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Controller";
							- _id = GUID fe66ef5a-2198-4f39-9e99-954c8f86b33b;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID fd0b563c-f9bf-4797-9e11-d5e95c72eed7;
						- _myState = 2048;
						- _objectCreation = "39356412717201715-69861039";
						- _umlDependencyID = "1706";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Account";
							- _id = GUID 30b8898a-abd4-4b48-b642-2ce3e7830d0a;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID ab49559b-de2c-4015-ba2b-4b89305c881d;
						- _myState = 2048;
						- _objectCreation = "39356432717201715-69881039";
						- _umlDependencyID = "1710";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Contact";
							- _id = GUID ae86cfee-b398-4127-84da-82ad5aa80966;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 274a80ae-8a00-4e42-b5bc-30e637496950;
						- _myState = 2048;
						- _objectCreation = "39356452717201715-69901039";
						- _umlDependencyID = "1705";
						- m_eRoleType = ACTOR;
						- m_pBase = { IHandle 
							- _m2Class = "IActor";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "User";
							- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 11;
					- value = 
					{ IMessage 
						- _id = GUID 5133d87f-ed0d-479a-8c79-9e5f961750c7;
						- _name = "navToLogin";
						- _objectCreation = "39356472717201715-69921039";
						- _umlDependencyID = "2734";
						- m_szSequence = "11.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToLogin()";
							- _id = GUID f7b437a2-4170-440d-b1f2-5a5de00a2d3c;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID efcf9b45-ec4e-4a84-9884-135c02c94329;
						- _name = "navToLogin";
						- _objectCreation = "39356492717201715-69941039";
						- _umlDependencyID = "2738";
						- m_szSequence = "8.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToLogin()";
							- _id = GUID f7b437a2-4170-440d-b1f2-5a5de00a2d3c;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 09db02ac-4f6e-4d6c-9ecb-7dea97add11b;
						- _name = "displayRegistrationFail";
						- _objectCreation = "39356512717201715-69961039";
						- _umlDependencyID = "4121";
						- m_szSequence = "9.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "displayRegistrationFail()";
							- _id = GUID e7986dad-17ca-4866-9270-d6fd673b18ad;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 298f3898-0b64-496c-965f-14a23d116e92;
						- _name = "navToRegister";
						- _objectCreation = "39356532717201715-69981039";
						- _umlDependencyID = "3069";
						- m_szSequence = "2.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToRegister()";
							- _id = GUID 4a719ca7-70b8-4711-907b-86865508a0c3;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID f1f2252b-5b68-493d-950f-aa115f1c55c3;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 33e916ef-2981-47c2-8f62-2cad95f374de;
						- _name = "registerAccount";
						- _objectCreation = "39356552717201715-70001039";
						- _umlDependencyID = "3275";
						- m_szSequence = "5.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 3f7ad3ae-2a48-4c5f-827b-2444ed5fc304;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "registerAccount()";
							- _id = GUID 1ce7fabf-7fdb-428f-a70e-767e0963de34;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID f54f4917-c32d-43e4-8fd7-9ec295a6fb07;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID c779b7c2-9233-4847-ad91-04d1c7545b41;
						}
					}
					{ IMessage 
						- _id = GUID 26b3c767-64ac-4656-a101-d01f8d309b5f;
						- _myState = 2048;
						- _name = "message_3";
						- _objectCreation = "39356572717201715-70021039";
						- _umlDependencyID = "2580";
						- m_szSequence = "10.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 274a80ae-8a00-4e42-b5bc-30e637496950;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 485e1a2b-a1f9-4b96-84f0-7c09a8a3dff6;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 40012697-e354-4b16-99b1-30a6cef2faa3;
						}
					}
					{ IMessage 
						- _id = GUID 0fd89fc7-c7a5-4775-8ddc-224e7d3fae94;
						- _myState = 2048;
						- _name = "message_2";
						- _objectCreation = "39356592717201715-70041039";
						- _umlDependencyID = "2583";
						- m_szSequence = "4.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 274a80ae-8a00-4e42-b5bc-30e637496950;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 774eec42-66e6-47e1-86bf-1dce84621ca5;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 058412c7-886a-4eb6-ba17-48df704c1c90;
						}
					}
					{ IMessage 
						- _id = GUID 54ec4da6-880f-4671-88f8-91daa95004ee;
						- _myState = 2048;
						- _name = "message_1";
						- _objectCreation = "39356612717201715-70061039";
						- _umlDependencyID = "2577";
						- m_szSequence = "3.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 274a80ae-8a00-4e42-b5bc-30e637496950;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID e600ff8a-1031-479f-8e5e-33f7947378c5;
						- _myState = 2048;
						- _name = "message_0";
						- _objectCreation = "39356632717201715-70081039";
						- _umlDependencyID = "2580";
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 5f3ab396-5fdb-41f2-99be-53f5a5518ca7;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 274a80ae-8a00-4e42-b5bc-30e637496950;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 64fb80bf-3835-4847-ac51-80bab79f608f;
						- _name = "account";
						- _objectCreation = "39356652717201715-70101039";
						- _umlDependencyID = "2440";
						- m_szSequence = "6.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID fd0b563c-f9bf-4797-9e11-d5e95c72eed7;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 3f7ad3ae-2a48-4c5f-827b-2444ed5fc304;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Account";
							- _name = "account()";
							- _id = GUID e0c456f0-ff11-43b2-a962-004829522fd2;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID c07f20a3-f522-4cc7-8aca-9e4729ccd89c;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID d33f4744-4435-41ce-9604-e5002745a143;
						}
					}
					{ IMessage 
						- _id = GUID 9c2bf9a7-0f64-48a4-b431-a41c48ab14c9;
						- _name = "contact";
						- _objectCreation = "39356672717201715-70121039";
						- _umlDependencyID = "2443";
						- m_szSequence = "7.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID ab49559b-de2c-4015-ba2b-4b89305c881d;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID fd0b563c-f9bf-4797-9e11-d5e95c72eed7;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Contact";
							- _name = "contact()";
							- _id = GUID 964d3d61-2883-4662-b631-2c146b8b55ee;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- ExecutionOccurrences = { IRPYRawContainer 
					- size = 9;
					- value = 
					{ IExecutionOccurrence 
						- _id = GUID f1f2252b-5b68-493d-950f-aa115f1c55c3;
						- _objectCreation = "39356692717201715-70141039";
						- _umlDependencyID = "1699";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 298f3898-0b64-496c-965f-14a23d116e92;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 774eec42-66e6-47e1-86bf-1dce84621ca5;
						- _objectCreation = "39356712717201715-70161039";
						- _umlDependencyID = "1694";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 0fd89fc7-c7a5-4775-8ddc-224e7d3fae94;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 235;
					}
					{ IExecutionOccurrence 
						- _id = GUID 058412c7-886a-4eb6-ba17-48df704c1c90;
						- _objectCreation = "39356732717201715-70181039";
						- _umlDependencyID = "1698";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 0fd89fc7-c7a5-4775-8ddc-224e7d3fae94;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID f54f4917-c32d-43e4-8fd7-9ec295a6fb07;
						- _objectCreation = "39356752717201715-70201039";
						- _umlDependencyID = "1693";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 33e916ef-2981-47c2-8f62-2cad95f374de;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 50;
					}
					{ IExecutionOccurrence 
						- _id = GUID c779b7c2-9233-4847-ad91-04d1c7545b41;
						- _objectCreation = "39356772717201715-70221039";
						- _umlDependencyID = "1697";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 33e916ef-2981-47c2-8f62-2cad95f374de;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID c07f20a3-f522-4cc7-8aca-9e4729ccd89c;
						- _objectCreation = "39356792717201715-70241039";
						- _umlDependencyID = "1701";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 64fb80bf-3835-4847-ac51-80bab79f608f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID d33f4744-4435-41ce-9604-e5002745a143;
						- _objectCreation = "39356812717201715-70261039";
						- _umlDependencyID = "1696";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 64fb80bf-3835-4847-ac51-80bab79f608f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
					{ IExecutionOccurrence 
						- _id = GUID 485e1a2b-a1f9-4b96-84f0-7c09a8a3dff6;
						- _objectCreation = "39356832717201715-70281039";
						- _umlDependencyID = "1700";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 26b3c767-64ac-4656-a101-d01f8d309b5f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 53;
					}
					{ IExecutionOccurrence 
						- _id = GUID 40012697-e354-4b16-99b1-30a6cef2faa3;
						- _objectCreation = "39356852717201715-70301039";
						- _umlDependencyID = "1695";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 26b3c767-64ac-4656-a101-d01f8d309b5f;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
				}
				- CombinedFragments = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ ICombinedFragment 
						- _id = GUID 813b70ce-9150-4a27-95bb-29d672a4b6a5;
						- _myState = 2048;
						- _name = "interactionOperator_0";
						- _objectCreation = "39356872717201715-70321039";
						- _umlDependencyID = "3870";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 00ff4951-7808-4c64-ba48-c8844e063c12;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39356892717201715-70341039";
								- _umlDependencyID = "3743";
								- CombinedFragments = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ ICombinedFragment 
										- _id = GUID 80fa0d27-7e5f-4d0d-b368-fd8984f44c87;
										- _myState = 2048;
										- _name = "interactionOperator_1";
										- _objectCreation = "39356912717201715-70361039";
										- _umlDependencyID = "3870";
										- _interactionOperator = "alt";
										- InteractionOperands = { IRPYRawContainer 
											- size = 2;
											- value = 
											{ IInteractionOperand 
												- _id = GUID 5f1b46d7-05a6-4186-a067-6c28130abc88;
												- _myState = 2048;
												- _name = "interactionOperand_1";
												- _objectCreation = "39356932717201715-70381039";
												- _umlDependencyID = "3743";
												- _interactionConstraint = "if submission successful";
											}
											{ IInteractionOperand 
												- _id = GUID 0e6d1c2b-fa36-42ab-b5c6-f9bc6084226d;
												- _myState = 2048;
												- _name = "interactionOperand_0";
												- _objectCreation = "39356952717201715-70401039";
												- _umlDependencyID = "3737";
												- _interactionConstraint = "Else";
											}
										}
									}
								}
								- _interactionConstraint = "User clicks submit";
							}
						}
					}
					{ ICombinedFragment 
						- _id = GUID 13127f18-61bc-41a4-8b58-a5be4295a056;
						- _myState = 2048;
						- _name = "interactionOperator_2";
						- _objectCreation = "39356972717201715-70421039";
						- _umlDependencyID = "3874";
						- _interactionOperator = "opt";
						- InteractionOperands = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IInteractionOperand 
								- _id = GUID 7286bb62-18f1-4b62-b8a4-880728e97d4f;
								- _myState = 2048;
								- _name = "interactionOperand_0";
								- _objectCreation = "39356992717201715-70441039";
								- _umlDependencyID = "3745";
								- _interactionConstraint = "user clicks cancel";
							}
						}
					}
				}
			}
		}
		{ IMSC 
			- _id = GUID 8a410791-42e9-4251-b949-8bbeab637289;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 5;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "DataFlow";
								- Properties = { IRPYRawContainer 
									- size = 6;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineStyle";
										- _Value = "2";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "EnvironmentLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "InstanceLine";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,96,437";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "121,122,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "0";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Message";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,112,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "execution_occurrence";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
					{ IPropertySubject 
						- _Name = "SequenceDiagram";
						- Metaclasses = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "General";
								- Properties = { IRPYRawContainer 
									- size = 3;
									- value = 
									{ IProperty 
										- _Name = "ClassCentricMode";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "CleanupRealized";
										- _Value = "True";
										- _Type = Bool;
									}
									{ IProperty 
										- _Name = "RealizeMessages";
										- _Value = "True";
										- _Type = Bool;
									}
								}
							}
						}
					}
				}
			}
			- _name = "sequencediagram_Logout";
			- _objectCreation = "39357012717201715-70461039";
			- _umlDependencyID = "4002";
			- _lastModifiedTime = "10.27.2017::17:56:4";
			- _graphicChart = { CGIMscChart 
				- vLadderMargin = 14;
				- m_usingActivationBar = 0;
				- _id = GUID a5e2eeac-c33c-4770-bd7a-8571d4cf2a58;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IMSC";
					- _id = GUID 8a410791-42e9-4251-b949-8bbeab637289;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 9;
				{ CGIBox 
					- _id = GUID e5fa0926-14cd-4e73-a527-3d2e44033c24;
					- m_type = 108;
					- m_pModelObject = { IHandle 
						- _m2Class = "ICollaboration";
						- _id = GUID fd51029b-35cc-44e8-bf52-97eee913fe55;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID 3ffa0cf3-a033-4b1c-a254-0bfcebeb0644;
					- m_type = 97;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID ad9cb2e5-deee-45ec-a1cc-68bc0951f11c;
					}
					- m_pParent = GUID e5fa0926-14cd-4e73-a527-3d2e44033c24;
					- m_name = { CGIText 
						- m_str = ":User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00522009 174 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID ae8f1fd7-ec6e-4cb9-b8bb-179192849138;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID a8185b84-47b3-414f-b898-c123590daf69;
					}
					- m_pParent = GUID e5fa0926-14cd-4e73-a527-3d2e44033c24;
					- m_name = { CGIText 
						- m_str = ":GUIInterface";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00522009 374 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscColumnCR 
					- _id = GUID e56089ab-3add-40b8-92fb-82f738f96bc8;
					- m_type = 109;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClassifierRole";
						- _id = GUID 99f6c923-f87a-4898-ac5c-913ed3860fc4;
					}
					- m_pParent = GUID e5fa0926-14cd-4e73-a527-3d2e44033c24;
					- m_name = { CGIText 
						- m_str = ":Controller";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 0.00522009 549 50 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_position = 4 0 0  0 49885  96 49885  96 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
				}
				{ CGIMscMessage 
					- _id = GUID cceb2a49-5e27-474b-9ee0-eb5f61b86c95;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 230a0416-ce58-4a20-b3b4-248dd6c0f90e;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "navToLogin()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID ae8f1fd7-ec6e-4cb9-b8bb-179192849138;
					- m_sourceType = 'F';
					- m_pTarget = GUID ae8f1fd7-ec6e-4cb9-b8bb-179192849138;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_arrow = 2 452 185  452 220  ;
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 2;
					- m_SourcePort = 48 25862 ;
					- m_TargetPort = 48 32566 ;
					- m_bLeft = 0;
				}
				{ CGIMscMessage 
					- _id = GUID fa587c6f-475d-4306-ba51-858025622d5e;
					- m_type = 214;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID 3e54144c-a7d8-445f-81d4-94e499e006a9;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "message_0";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 3ffa0cf3-a033-4b1c-a254-0bfcebeb0644;
					- m_sourceType = 'F';
					- m_pTarget = GUID ae8f1fd7-ec6e-4cb9-b8bb-179192849138;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 17241 ;
					- m_TargetPort = 48 17241 ;
					- m_bLeft = 0;
					- m_pSourceExec = GUID cc7fb002-c769-4ede-a185-6e38a2df9054;
					- m_pTargetExec = GUID 274a5c11-8097-4ae5-b8db-058c8fe2e2da;
				}
				{ CGIMscMessage 
					- _id = GUID dbf1c982-282f-42f6-86ed-e627abd8f786;
					- m_type = 110;
					- m_pModelObject = { IHandle 
						- _m2Class = "IMessage";
						- _id = GUID a7dc3e3c-83b3-48a6-bc8e-30cfb2612495;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "cleanUp()";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 6;
					}
					- m_drawBehavior = 4096;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID ae8f1fd7-ec6e-4cb9-b8bb-179192849138;
					- m_sourceType = 'F';
					- m_pTarget = GUID e56089ab-3add-40b8-92fb-82f738f96bc8;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 48 19923 ;
					- m_TargetPort = 48 19923 ;
					- m_bLeft = 0;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID 274a5c11-8097-4ae5-b8db-058c8fe2e2da;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID fca60313-53ef-4709-855a-4a3e54fa5d17;
					}
					- m_pParent = GUID ae8f1fd7-ec6e-4cb9-b8bb-179192849138;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 585.345 42 17241.1 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID fa587c6f-475d-4306-ba51-858025622d5e;
				}
				{ CGIMscExecutionOccurrence 
					- _id = GUID cc7fb002-c769-4ede-a185-6e38a2df9054;
					- m_type = 162;
					- m_pModelObject = { IHandle 
						- _m2Class = "IExecutionOccurrence";
						- _id = GUID 1b1321eb-868a-4f8f-9eaa-25849923bfd3;
					}
					- m_pParent = GUID 3ffa0cf3-a033-4b1c-a254-0bfcebeb0644;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_transform = 1 0 0 191.568 42 17241.1 ;
					- m_bIsPreferencesInitialized = 1;
					- m_position = 4 0 0  0 36  12 36  12 0  ;
					- m_pInheritsFrom = { IHandle 
						- _m2Class = "";
					}
					- m_nInheritanceMask = 0;
					- m_SubType = 0;
					- m_pStartMessage = GUID fa587c6f-475d-4306-ba51-858025622d5e;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID e5fa0926-14cd-4e73-a527-3d2e44033c24;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
			- m_pICollaboration = { ICollaboration 
				- _id = GUID fd51029b-35cc-44e8-bf52-97eee913fe55;
				- _objectCreation = "39357032717201715-70481039";
				- _umlDependencyID = "1695";
				- ClassifierRoles = { IRPYRawContainer 
					- size = 3;
					- value = 
					{ IClassifierRole 
						- _id = GUID ad9cb2e5-deee-45ec-a1cc-68bc0951f11c;
						- _myState = 2048;
						- _objectCreation = "39357052717201715-70501039";
						- _umlDependencyID = "1690";
						- m_eRoleType = ACTOR;
						- m_pBase = { IHandle 
							- _m2Class = "IActor";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "User";
							- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID a8185b84-47b3-414f-b898-c123590daf69;
						- _myState = 2048;
						- _objectCreation = "39357072717201715-70521039";
						- _umlDependencyID = "1694";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "GUIInterface";
							- _id = GUID 4aa99777-6edc-45b5-8a77-41fc3f52b011;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
					{ IClassifierRole 
						- _id = GUID 99f6c923-f87a-4898-ac5c-913ed3860fc4;
						- _myState = 2048;
						- _objectCreation = "39357092717201715-70541039";
						- _umlDependencyID = "1698";
						- m_eRoleType = CLASS;
						- m_pBase = { IHandle 
							- _m2Class = "IClass";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "";
							- _name = "Controller";
							- _id = GUID fe66ef5a-2198-4f39-9e99-954c8f86b33b;
						}
						- m_instance = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- Messages = { IRPYRawContainer 
					- size = 3;
					- value = 
					{ IMessage 
						- _id = GUID 230a0416-ce58-4a20-b3b4-248dd6c0f90e;
						- _name = "navToLogin";
						- _objectCreation = "39357112717201715-70561039";
						- _umlDependencyID = "2718";
						- m_szSequence = "3.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a8185b84-47b3-414f-b898-c123590daf69;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a8185b84-47b3-414f-b898-c123590daf69;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "GUIInterface";
							- _name = "navToLogin()";
							- _id = GUID f7b437a2-4170-440d-b1f2-5a5de00a2d3c;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
					{ IMessage 
						- _id = GUID 3e54144c-a7d8-445f-81d4-94e499e006a9;
						- _myState = 2048;
						- _name = "message_0";
						- _objectCreation = "39357132717201715-70581039";
						- _umlDependencyID = "2581";
						- m_szSequence = "1.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a8185b84-47b3-414f-b898-c123590daf69;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID ad9cb2e5-deee-45ec-a1cc-68bc0951f11c;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "";
						}
						- m_eType = DATAFLOW;
						- m_targetExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID fca60313-53ef-4709-855a-4a3e54fa5d17;
						}
						- m_srcExec = { IHandle 
							- _m2Class = "IExecutionOccurrence";
							- _id = GUID 1b1321eb-868a-4f8f-9eaa-25849923bfd3;
						}
					}
					{ IMessage 
						- _id = GUID a7dc3e3c-83b3-48a6-bc8e-30cfb2612495;
						- _name = "cleanUp";
						- _objectCreation = "39357152717201715-70601039";
						- _umlDependencyID = "2404";
						- m_szSequence = "2.";
						- m_szActualArgs = "";
						- m_szReturnVal = "";
						- m_pCommunicationConnection = { IHandle 
							- _m2Class = "";
						}
						- m_pReceiver = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID 99f6c923-f87a-4898-ac5c-913ed3860fc4;
						}
						- m_pSender = { IHandle 
							- _m2Class = "IClassifierRole";
							- _id = GUID a8185b84-47b3-414f-b898-c123590daf69;
						}
						- m_pFormalMessage = { IHandle 
							- _m2Class = "IPrimitiveOperation";
							- _filename = "Default.sbs";
							- _subsystem = "Default";
							- _class = "Controller";
							- _name = "cleanUp()";
							- _id = GUID 5777259d-456a-4843-a218-ec4a09cc0269;
						}
						- m_eType = PRIMITIVE;
						- m_targetExec = { IHandle 
							- _m2Class = "";
						}
						- m_srcExec = { IHandle 
							- _m2Class = "";
						}
					}
				}
				- ExecutionOccurrences = { IRPYRawContainer 
					- size = 2;
					- value = 
					{ IExecutionOccurrence 
						- _id = GUID fca60313-53ef-4709-855a-4a3e54fa5d17;
						- _objectCreation = "39357172717201715-70621039";
						- _umlDependencyID = "1696";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 3e54144c-a7d8-445f-81d4-94e499e006a9;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 110;
					}
					{ IExecutionOccurrence 
						- _id = GUID 1b1321eb-868a-4f8f-9eaa-25849923bfd3;
						- _objectCreation = "39357192717201715-70641039";
						- _umlDependencyID = "1700";
						- m_startMessage = { IHandle 
							- _m2Class = "IMessage";
							- _id = GUID 3e54144c-a7d8-445f-81d4-94e499e006a9;
						}
						- m_endMessage = { IHandle 
							- _m2Class = "";
						}
						- m_length = 36;
					}
				}
			}
		}
	}
	- Components = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IComponent 
			- fileName = "DefaultComponent";
			- _id = GUID e5399f4e-a77a-4e39-8be0-82402ad42379;
		}
	}
	- UCDiagrams = { IRPYRawContainer 
		- size = 1;
		- value = 
		{ IUCDiagram 
			- _id = GUID 1db57d68-4c39-4af6-8f70-9d0913a4556a;
			- _myState = 8192;
			- _properties = { IPropertyContainer 
				- Subjects = { IRPYRawContainer 
					- size = 1;
					- value = 
					{ IPropertySubject 
						- _Name = "Format";
						- Metaclasses = { IRPYRawContainer 
							- size = 4;
							- value = 
							{ IPropertyMetaclass 
								- _Name = "Actor";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,26,84,168";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "111,0,107";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "Association";
								- Properties = { IRPYRawContainer 
									- size = 5;
									- value = 
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,128";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "221,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "System_Border";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,0,228,240";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "111,0,107";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
							{ IPropertyMetaclass 
								- _Name = "UseCase";
								- Properties = { IRPYRawContainer 
									- size = 7;
									- value = 
									{ IProperty 
										- _Name = "DefaultSize";
										- _Value = "0,21,129,92";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Fill.FillColor";
										- _Value = "255,255,255";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Font";
										- _Value = "Arial";
										- _Type = String;
									}
									{ IProperty 
										- _Name = "Font.FontColor";
										- _Value = "0,0,0";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Font.Size";
										- _Value = "10";
										- _Type = Int;
									}
									{ IProperty 
										- _Name = "Line.LineColor";
										- _Value = "111,0,107";
										- _Type = Color;
									}
									{ IProperty 
										- _Name = "Line.LineWidth";
										- _Value = "1";
										- _Type = Int;
									}
								}
							}
						}
					}
				}
			}
			- _name = "useCaseDiagram";
			- _objectCreation = "39357212717201715-70661039";
			- _umlDependencyID = "3101";
			- _lastModifiedTime = "9.28.2017::21:37:40";
			- _graphicChart = { CGIClassChart 
				- _id = GUID 01a34a47-861e-464f-8f7a-7859ba4ec4a3;
				- m_type = 0;
				- m_pModelObject = { IHandle 
					- _m2Class = "IUCDiagram";
					- _id = GUID 1db57d68-4c39-4af6-8f70-9d0913a4556a;
				}
				- m_pParent = ;
				- m_name = { CGIText 
					- m_str = "";
					- m_style = "Arial" 10 0 0 0 1 ;
					- m_color = { IColor 
						- m_fgColor = 0;
						- m_bgColor = 0;
						- m_bgFlag = 0;
					}
					- m_position = 1 0 0  ;
					- m_nIdent = 0;
					- m_bImplicitSetRectPoints = 0;
					- m_nOrientationCtrlPt = 8;
				}
				- m_drawBehavior = 0;
				- m_bIsPreferencesInitialized = 0;
				- elementList = 25;
				{ CGIClass 
					- _id = GUID 9cd26027-ee67-45c2-a75b-c8de7dede15d;
					- m_type = 78;
					- m_pModelObject = { IHandle 
						- _m2Class = "IClass";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "TopLevel";
						- _id = GUID 1d214e90-2b2b-44ac-832f-bc1e6e816445;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "TopLevel";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 0;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 0 ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
					- Attrs = { IRPYRawContainer 
						- size = 0;
					}
					- Operations = { IRPYRawContainer 
						- size = 0;
					}
				}
				{ CGIBasicClass 
					- _id = GUID 54b2899d-8946-40a0-95ac-8abe465fcf92;
					- m_type = 125;
					- m_pModelObject = { IHandle 
						- _m2Class = "IUseCase";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Add Event";
						- _id = GUID 1860be17-27de-4e55-847f-3b0033c57b28;
					}
					- m_pParent = GUID 11ae1ae1-fb04-4263-a3c7-22159a57b173;
					- m_name = { CGIText 
						- m_str = "Add Event";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.6154 0 0 0.147055 254.374 142.115 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 -2 -1  -2 1070  1127 1070  1127 -1  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID 3b5ab271-ef4c-42d6-8fb4-2697f709bdf9;
					- m_type = 125;
					- m_pModelObject = { IHandle 
						- _m2Class = "IUseCase";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Delete Event";
						- _id = GUID 01da98d7-5a08-48f9-bd93-af4ba051ecbb;
					}
					- m_pParent = GUID 11ae1ae1-fb04-4263-a3c7-22159a57b173;
					- m_name = { CGIText 
						- m_str = "Delete Event";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.6154 0 0 0.147055 254.374 931.81 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 -2 -1  -2 1070  1127 1070  1127 -1  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID 68383a0e-3e05-4270-bc53-b3073bcb09af;
					- m_type = 125;
					- m_pModelObject = { IHandle 
						- _m2Class = "IUseCase";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Edit Event";
						- _id = GUID 4a41f66d-986b-4c3c-9348-873959ea5732;
					}
					- m_pParent = GUID 11ae1ae1-fb04-4263-a3c7-22159a57b173;
					- m_name = { CGIText 
						- m_str = "Edit Event";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.6154 0 0 0.147055 254.374 339.538 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 -2 -1  -2 1070  1127 1070  1127 -1  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID 46eb36ea-520b-44be-8931-4ef4569a090d;
					- m_type = 125;
					- m_pModelObject = { IHandle 
						- _m2Class = "IUseCase";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "View Daily Events";
						- _id = GUID 3645889c-b28b-4329-ae48-e9e858cb9c07;
					}
					- m_pParent = GUID 11ae1ae1-fb04-4263-a3c7-22159a57b173;
					- m_name = { CGIText 
						- m_str = "View Daily Events";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.6154 0 0 0.147055 254.374 536.962 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 -2 -1  -2 1070  1127 1070  1127 -1  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID 1fbe2af1-87d0-48e8-a67a-d7869d0f571b;
					- m_type = 125;
					- m_pModelObject = { IHandle 
						- _m2Class = "IUseCase";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "View Monthly Events";
						- _id = GUID 9fee6573-bfc8-497e-a3ab-4b30a205f44a;
					}
					- m_pParent = GUID 11ae1ae1-fb04-4263-a3c7-22159a57b173;
					- m_name = { CGIText 
						- m_str = "View Monthly Events";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.6154 0 0 0.147055 254.374 734.386 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 -2 -1  -2 1070  1127 1070  1127 -1  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBox 
					- _id = GUID 11ae1ae1-fb04-4263-a3c7-22159a57b173;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "FitBoxToItsTextuals";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 123;
					- m_pModelObject = { IHandle 
						- _m2Class = "";
					}
					- m_pParent = GUID 9cd26027-ee67-45c2-a75b-c8de7dede15d;
					- m_name = { CGIText 
						- m_str = "Event: Meeting Coordinator";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 0.185668 0 0 0.450806 340 31 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 1240  1228 1240  1228 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID 2788cce8-2c18-4371-9e1f-5bfa849efa4f;
					- m_type = 125;
					- m_pModelObject = { IHandle 
						- _m2Class = "IUseCase";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Register Account";
						- _id = GUID 5715597a-e49e-470a-82e2-3fa5956c8228;
					}
					- m_pParent = GUID a6e748e7-6dd1-4f5e-9cfe-ad1bcbfdaa95;
					- m_name = { CGIText 
						- m_str = "Register Account";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.671346 0 0 0.259317 77.7283 207.577 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 -2 -1  -2 1070  1127 1070  1127 -1  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID 818e54de-1bf0-4928-88fd-84bec8d8fcc3;
					- m_type = 125;
					- m_pModelObject = { IHandle 
						- _m2Class = "IUseCase";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Log In";
						- _id = GUID c3b82927-ad3c-4243-aff4-d4f8c1a593fd;
					}
					- m_pParent = GUID a6e748e7-6dd1-4f5e-9cfe-ad1bcbfdaa95;
					- m_name = { CGIText 
						- m_str = "Log In";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.671346 0 0 0.259317 77.7283 536.157 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 -2 -1  -2 1070  1127 1070  1127 -1  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID d4cbd070-4d98-450b-86c6-2f201cbfc9ae;
					- m_type = 125;
					- m_pModelObject = { IHandle 
						- _m2Class = "IUseCase";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Log Out";
						- _id = GUID 659d6359-5507-4ac2-8747-e68612f7d27a;
					}
					- m_pParent = GUID a6e748e7-6dd1-4f5e-9cfe-ad1bcbfdaa95;
					- m_name = { CGIText 
						- m_str = "Log Out";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.671346 0 0 0.259317 77.7283 864.738 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 -2 -1  -2 1070  1127 1070  1127 -1  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID 31cef3db-34e6-411a-94ce-1be7c0f9bf92;
					- m_type = 125;
					- m_pModelObject = { IHandle 
						- _m2Class = "IUseCase";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "Create Room";
						- _id = GUID 63aff574-69d1-4f0c-89be-400db35710c7;
					}
					- m_pParent = GUID e0c503be-1413-49d9-8850-bf6e07989730;
					- m_name = { CGIText 
						- m_str = "Create Room";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.684445 0 0 0.430385 217.021 422.418 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 -2 -1  -2 1070  1127 1070  1127 -1  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID 8d614306-2a00-41ab-9ac9-b97a24edb692;
					- m_type = 124;
					- m_pModelObject = { IHandle 
						- _m2Class = "IActor";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "User";
						- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
					}
					- m_pParent = GUID 9cd26027-ee67-45c2-a75b-c8de7dede15d;
					- m_name = { CGIText 
						- m_str = "User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.077634 0 0 0.123909 145.895 168.023 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 40 250  40 1396  1122 1396  1122 250  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBasicClass 
					- _id = GUID 90394056-f787-465c-bc6a-00b26cc48d59;
					- m_type = 124;
					- m_pModelObject = { IHandle 
						- _m2Class = "IActor";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "User";
						- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
					}
					- m_pParent = GUID 9cd26027-ee67-45c2-a75b-c8de7dede15d;
					- m_name = { CGIText 
						- m_str = "User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.077634 0 0 0.123909 633.895 185.023 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 40 250  40 1396  1122 1396  1122 250  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIAssociationEnd 
					- _id = GUID a2b41860-7fa2-4241-84cc-3b1cf680c273;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "ShowLabels";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 94;
					- m_pModelObject = { IHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "User";
						- _name = "itsAdd Event";
						- _id = GUID 930bf59b-103f-4798-89ad-bc3947ed45dd;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 8d614306-2a00-41ab-9ac9-b97a24edb692;
					- m_sourceType = 'F';
					- m_pTarget = GUID 54b2899d-8946-40a0-95ac-8abe465fcf92;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 7;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 980 532 ;
					- m_TargetPort = 191 633 ;
					- m_pInverseModelObject = { IAssociationEndHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "Add Event";
						- _name = "itsUser";
						- _id = GUID f59380b1-d1a2-40df-ada8-f0b26ba56310;
					}
					- m_pInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_pInverseInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_bShowSourceMultiplicity = 0;
					- m_bShowSourceRole = 0;
					- m_bShowTargetMultiplicity = 0;
					- m_bShowTargetRole = 0;
					- m_bShowLinkName = 1;
					- m_bShowSpecificType = 0;
					- m_bInstance = 0;
					- m_bShowQualifier1 = 0;
					- m_bShowQualifier2 = 0;
					- m_sourceRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 2;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 3;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 4;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 5;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 6;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_targetQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 7;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_specificType = symmetric_type;
				}
				{ CGIAssociationEnd 
					- _id = GUID a743ae1e-481c-4e0f-8ade-825d98d5d56e;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "ShowLabels";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 94;
					- m_pModelObject = { IHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "User";
						- _name = "itsEdit Event";
						- _id = GUID 07b0118d-b9c6-434d-9a56-2cc3dbdcdd84;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 8d614306-2a00-41ab-9ac9-b97a24edb692;
					- m_sourceType = 'F';
					- m_pTarget = GUID 68383a0e-3e05-4270-bc53-b3073bcb09af;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 7;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 1096 524 ;
					- m_TargetPort = 409 527 ;
					- m_pInverseModelObject = { IAssociationEndHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "Edit Event";
						- _name = "itsUser";
						- _id = GUID 923c4abe-b9a2-40a3-8ac8-01c6d6b5d6c7;
					}
					- m_pInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_pInverseInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_bShowSourceMultiplicity = 0;
					- m_bShowSourceRole = 0;
					- m_bShowTargetMultiplicity = 0;
					- m_bShowTargetRole = 0;
					- m_bShowLinkName = 1;
					- m_bShowSpecificType = 0;
					- m_bInstance = 0;
					- m_bShowQualifier1 = 0;
					- m_bShowQualifier2 = 0;
					- m_sourceRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 2;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 3;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 4;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 5;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 6;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_targetQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 7;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_specificType = symmetric_type;
				}
				{ CGIAssociationEnd 
					- _id = GUID 12f24ed1-aa6c-4e1a-bd40-b3923e8baebe;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "ShowLabels";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 94;
					- m_pModelObject = { IHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "User";
						- _name = "itsView Daily Events";
						- _id = GUID 13d7ef62-2a31-40de-8956-c8dba3ec20b2;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 8d614306-2a00-41ab-9ac9-b97a24edb692;
					- m_sourceType = 'F';
					- m_pTarget = GUID 46eb36ea-520b-44be-8931-4ef4569a090d;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 7;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 1083 541 ;
					- m_TargetPort = 287 557 ;
					- m_pInverseModelObject = { IAssociationEndHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "View Daily Events";
						- _name = "itsUser";
						- _id = GUID 0441b133-2798-432b-a8af-5711e6e4debc;
					}
					- m_pInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_pInverseInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_bShowSourceMultiplicity = 0;
					- m_bShowSourceRole = 0;
					- m_bShowTargetMultiplicity = 0;
					- m_bShowTargetRole = 0;
					- m_bShowLinkName = 1;
					- m_bShowSpecificType = 0;
					- m_bInstance = 0;
					- m_bShowQualifier1 = 0;
					- m_bShowQualifier2 = 0;
					- m_sourceRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 2;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 3;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 4;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 5;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 6;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_targetQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 7;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_specificType = symmetric_type;
				}
				{ CGIAssociationEnd 
					- _id = GUID 5a4bce27-2baa-4ae4-990c-6ce6c70983e4;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "ShowLabels";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 94;
					- m_pModelObject = { IHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "User";
						- _name = "itsView Monthly Events";
						- _id = GUID 638578c7-5a74-4d80-94d8-48055322c335;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 8d614306-2a00-41ab-9ac9-b97a24edb692;
					- m_sourceType = 'F';
					- m_pTarget = GUID 1fbe2af1-87d0-48e8-a67a-d7869d0f571b;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 7;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 1083 541 ;
					- m_TargetPort = 164 527 ;
					- m_pInverseModelObject = { IAssociationEndHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "View Monthly Events";
						- _name = "itsUser";
						- _id = GUID e9930ae2-373f-4177-b243-88f001ce8a7d;
					}
					- m_pInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_pInverseInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_bShowSourceMultiplicity = 0;
					- m_bShowSourceRole = 0;
					- m_bShowTargetMultiplicity = 0;
					- m_bShowTargetRole = 0;
					- m_bShowLinkName = 1;
					- m_bShowSpecificType = 0;
					- m_bInstance = 0;
					- m_bShowQualifier1 = 0;
					- m_bShowQualifier2 = 0;
					- m_sourceRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 2;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 3;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 4;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 5;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 6;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_targetQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 7;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_specificType = symmetric_type;
				}
				{ CGIAssociationEnd 
					- _id = GUID 0bdc0728-2764-491d-bdf1-5de3ce32f6bd;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "ShowLabels";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 94;
					- m_pModelObject = { IHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "User";
						- _name = "itsDelete Event";
						- _id = GUID 7288140f-2fc5-43cc-9585-f5f94fcdc2ac;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 8d614306-2a00-41ab-9ac9-b97a24edb692;
					- m_sourceType = 'F';
					- m_pTarget = GUID 3b5ab271-ef4c-42d6-8fb4-2697f709bdf9;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 7;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 1045 557 ;
					- m_TargetPort = 182 587 ;
					- m_pInverseModelObject = { IAssociationEndHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "Delete Event";
						- _name = "itsUser";
						- _id = GUID 12784f56-3190-4d6e-a00a-207b36fbb79c;
					}
					- m_pInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_pInverseInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_bShowSourceMultiplicity = 0;
					- m_bShowSourceRole = 0;
					- m_bShowTargetMultiplicity = 0;
					- m_bShowTargetRole = 0;
					- m_bShowLinkName = 1;
					- m_bShowSpecificType = 0;
					- m_bInstance = 0;
					- m_bShowQualifier1 = 0;
					- m_bShowQualifier2 = 0;
					- m_sourceRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 2;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 3;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 4;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 5;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 6;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_targetQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 7;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_specificType = symmetric_type;
				}
				{ CGIBox 
					- _id = GUID a6e748e7-6dd1-4f5e-9cfe-ad1bcbfdaa95;
					- m_type = 123;
					- m_pModelObject = { IHandle 
						- _m2Class = "";
					}
					- m_pParent = GUID 9cd26027-ee67-45c2-a75b-c8de7dede15d;
					- m_name = { CGIText 
						- m_str = "Account:  Meeting Coordinator";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 0.170195 0 0 0.255645 792 72 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 1240  1228 1240  1228 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIAssociationEnd 
					- _id = GUID 2e411dec-4213-417d-9f6c-5ac6825e2365;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "ShowLabels";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 94;
					- m_pModelObject = { IHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "User";
						- _name = "itsRegister Account";
						- _id = GUID 310038e2-8490-4fc2-93c3-aff811170c31;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 90394056-f787-465c-bc6a-00b26cc48d59;
					- m_sourceType = 'F';
					- m_pTarget = GUID 2788cce8-2c18-4371-9e1f-5bfa849efa4f;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 7;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 1032 541 ;
					- m_TargetPort = 156 286 ;
					- m_pInverseModelObject = { IAssociationEndHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "Register Account";
						- _name = "itsUser";
						- _id = GUID 8f6d06dd-14dd-4b37-a8ab-7896db72dffb;
					}
					- m_pInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_pInverseInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_bShowSourceMultiplicity = 0;
					- m_bShowSourceRole = 0;
					- m_bShowTargetMultiplicity = 0;
					- m_bShowTargetRole = 0;
					- m_bShowLinkName = 1;
					- m_bShowSpecificType = 0;
					- m_bInstance = 0;
					- m_bShowQualifier1 = 0;
					- m_bShowQualifier2 = 0;
					- m_sourceRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 2;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 3;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 4;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 5;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 6;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_targetQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 7;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_specificType = symmetric_type;
				}
				{ CGIAssociationEnd 
					- _id = GUID 057763b5-3c16-4211-a6c1-a6fee0484dfe;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "ShowLabels";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 94;
					- m_pModelObject = { IHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "User";
						- _name = "itsLog In";
						- _id = GUID 186c9cab-130a-499b-96ee-67d8a9d6fb5b;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 90394056-f787-465c-bc6a-00b26cc48d59;
					- m_sourceType = 'F';
					- m_pTarget = GUID 818e54de-1bf0-4928-88fd-84bec8d8fcc3;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 7;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 1070 565 ;
					- m_TargetPort = 252 225 ;
					- m_pInverseModelObject = { IAssociationEndHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "Log In";
						- _name = "itsUser";
						- _id = GUID 92a84eb3-2950-4a07-b3be-38e50ac1acf1;
					}
					- m_pInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_pInverseInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_bShowSourceMultiplicity = 0;
					- m_bShowSourceRole = 0;
					- m_bShowTargetMultiplicity = 0;
					- m_bShowTargetRole = 0;
					- m_bShowLinkName = 1;
					- m_bShowSpecificType = 0;
					- m_bInstance = 0;
					- m_bShowQualifier1 = 0;
					- m_bShowQualifier2 = 0;
					- m_sourceRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 2;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 3;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 4;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 5;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 6;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_targetQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 7;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_specificType = symmetric_type;
				}
				{ CGIAssociationEnd 
					- _id = GUID 4646a552-c257-4d8b-b030-ec0513c12b7c;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "ShowLabels";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 94;
					- m_pModelObject = { IHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "User";
						- _name = "itsLog Out";
						- _id = GUID e19defbf-2a5a-43e6-8c07-6ee21ca00ae7;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 90394056-f787-465c-bc6a-00b26cc48d59;
					- m_sourceType = 'F';
					- m_pTarget = GUID d4cbd070-4d98-450b-86c6-2f201cbfc9ae;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 7;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 1032 549 ;
					- m_TargetPort = 226 753 ;
					- m_pInverseModelObject = { IAssociationEndHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "Log Out";
						- _name = "itsUser";
						- _id = GUID 66d0cb4b-b9b0-4919-b272-d5695124368d;
					}
					- m_pInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_pInverseInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_bShowSourceMultiplicity = 0;
					- m_bShowSourceRole = 0;
					- m_bShowTargetMultiplicity = 0;
					- m_bShowTargetRole = 0;
					- m_bShowLinkName = 1;
					- m_bShowSpecificType = 0;
					- m_bInstance = 0;
					- m_bShowQualifier1 = 0;
					- m_bShowQualifier2 = 0;
					- m_sourceRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 2;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 3;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 4;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 5;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 6;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_targetQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 7;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_specificType = symmetric_type;
				}
				{ CGIBasicClass 
					- _id = GUID 79612ead-488b-48bc-a2e0-28115b33d1b0;
					- m_type = 124;
					- m_pModelObject = { IHandle 
						- _m2Class = "IActor";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "";
						- _name = "User";
						- _id = GUID c2cd4230-a025-4f9e-a8d1-db8c9e026725;
					}
					- m_pParent = GUID 9cd26027-ee67-45c2-a75b-c8de7dede15d;
					- m_name = { CGIText 
						- m_str = "User";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 5;
					}
					- m_drawBehavior = 2056;
					- m_transform = 0.077634 0 0 0.123909 1074.89 146.023 ;
					- m_bIsPreferencesInitialized = 1;
					- m_AdditionalLabel = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 1;
					}
					- m_polygon = 4 40 250  40 1396  1122 1396  1122 250  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIBox 
					- _id = GUID e0c503be-1413-49d9-8850-bf6e07989730;
					- m_type = 123;
					- m_pModelObject = { IHandle 
						- _m2Class = "";
					}
					- m_pParent = GUID 9cd26027-ee67-45c2-a75b-c8de7dede15d;
					- m_name = { CGIText 
						- m_str = "Utilities:  Meeting Coordinator";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 4096;
					- m_transform = 0.166938 0 0 0.154032 1224 132 ;
					- m_bIsPreferencesInitialized = 1;
					- m_polygon = 4 0 0  0 1240  1228 1240  1228 0  ;
					- m_nNameFormat = 0;
					- m_nIsNameFormat = 0;
				}
				{ CGIAssociationEnd 
					- _id = GUID e7ab9cb9-3ca9-45ef-b926-81c65d5d9c4a;
					- _properties = { IPropertyContainer 
						- Subjects = { IRPYRawContainer 
							- size = 1;
							- value = 
							{ IPropertySubject 
								- _Name = "General";
								- Metaclasses = { IRPYRawContainer 
									- size = 1;
									- value = 
									{ IPropertyMetaclass 
										- _Name = "Graphics";
										- Properties = { IRPYRawContainer 
											- size = 1;
											- value = 
											{ IProperty 
												- _Name = "ShowLabels";
												- _Value = "False";
												- _Type = Bool;
											}
										}
									}
								}
							}
						}
					}
					- m_type = 94;
					- m_pModelObject = { IHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "User";
						- _name = "itsCreate Room";
						- _id = GUID 855feb24-cdef-4657-923c-f54484b52640;
					}
					- m_pParent = ;
					- m_name = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_drawBehavior = 0;
					- m_bIsPreferencesInitialized = 1;
					- m_pSource = GUID 79612ead-488b-48bc-a2e0-28115b33d1b0;
					- m_sourceType = 'F';
					- m_pTarget = GUID 31cef3db-34e6-411a-94ce-1be7c0f9bf92;
					- m_targetType = 'T';
					- m_direction = ' ';
					- m_rpn = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 0;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 7;
					}
					- m_anglePoint1 = 0 0 ;
					- m_anglePoint2 = 0 0 ;
					- m_line_style = 0;
					- m_SourcePort = 1045 565 ;
					- m_TargetPort = 269 436 ;
					- m_pInverseModelObject = { IAssociationEndHandle 
						- _m2Class = "IAssociationEnd";
						- _filename = "Default.sbs";
						- _subsystem = "Default";
						- _class = "Create Room";
						- _name = "itsUser";
						- _id = GUID 308ba8c3-5424-4bcc-9997-dd8802d06c0f;
					}
					- m_pInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_pInverseInstance = { IObjectLinkHandle 
						- _m2Class = "";
					}
					- m_bShowSourceMultiplicity = 0;
					- m_bShowSourceRole = 0;
					- m_bShowTargetMultiplicity = 0;
					- m_bShowTargetRole = 0;
					- m_bShowLinkName = 1;
					- m_bShowSpecificType = 0;
					- m_bInstance = 0;
					- m_bShowQualifier1 = 0;
					- m_bShowQualifier2 = 0;
					- m_sourceRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 2;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetRole = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 3;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 4;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_targetMultiplicity = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 5;
						- m_bImplicitSetRectPoints = 0;
					}
					- m_sourceQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 6;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_targetQualifier = { CGIText 
						- m_str = "";
						- m_style = "Arial" 10 0 0 0 1 ;
						- m_color = { IColor 
							- m_fgColor = 0;
							- m_bgColor = 0;
							- m_bgFlag = 0;
						}
						- m_position = 1 0 0  ;
						- m_nIdent = 7;
						- m_bImplicitSetRectPoints = 0;
						- m_nOrientationCtrlPt = 8;
					}
					- m_specificType = symmetric_type;
				}
				
				- m_access = 'Z';
				- m_modified = 'N';
				- m_fileVersion = "";
				- m_nModifyDate = 0;
				- m_nCreateDate = 0;
				- m_creator = "";
				- m_bScaleWithZoom = 1;
				- m_arrowStyle = 'S';
				- m_pRoot = GUID 9cd26027-ee67-45c2-a75b-c8de7dede15d;
				- m_currentLeftTop = 0 0 ;
				- m_currentRightBottom = 0 0 ;
			}
			- _defaultSubsystem = { IHandle 
				- _m2Class = "ISubsystem";
				- _filename = "Default.sbs";
				- _subsystem = "";
				- _class = "";
				- _name = "Default";
				- _id = GUID db711d46-d3d0-4582-8438-74b52839d3e6;
			}
		}
	}
}

