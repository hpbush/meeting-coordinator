﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Contact : Participant
    {
        private int contactID;
        private String firstName;
        private String lastName;
        private String email;
        private String phoneNumber;

        public Contact(String firstName, String lastName, String email, String phoneNumber) : base()
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.phoneNumber = phoneNumber;
        }

        public Contact() : base()
        {

        }

        public String getFirstName()
        {
            return firstName;
        }

        public String getLastname()
        {
            return lastName;
        }

        public String getEmail()
        {
            return email;
        }

        public String getPhoneNumber()
        {
            return phoneNumber;
        }

        public void setFirstName(String newFirstName)
        {
            firstName = newFirstName;
        }

        public void setLastName(String newLastName)
        {
            lastName = newLastName;
        }

        public void setEmail(String newEmail)
        {
            email = newEmail;
        }

        public void setPhoneNumber(String newPhoneNumber)
        {
            phoneNumber = newPhoneNumber;
        }

        public void setContactID(int id)
        {
            contactID = id;
        }

        public int getContactID()
        {
            return contactID;
        }
    }
}
