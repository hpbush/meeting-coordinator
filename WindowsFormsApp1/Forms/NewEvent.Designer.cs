﻿namespace WindowsFormsApp1
{
    partial class NewEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.eTitle = new System.Windows.Forms.TextBox();
            this.eDurationHour = new System.Windows.Forms.TextBox();
            this.eDetails = new System.Windows.Forms.TextBox();
            this.eAttendeesList = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.eNotAttendingList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.saveButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.eAddParticipant = new System.Windows.Forms.Button();
            this.eRemoveParticipant = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.eStartTimeLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.eDate = new System.Windows.Forms.DateTimePicker();
            this.eStartTime = new System.Windows.Forms.DateTimePicker();
            this.eDurationMinute = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.eRoom = new System.Windows.Forms.ComboBox();
            this.customTimePicker = new WindowsFormsApp1.Forms.CustomTimePicker();
            this.SuspendLayout();
            // 
            // eTitle
            // 
            this.eTitle.Location = new System.Drawing.Point(149, 15);
            this.eTitle.Name = "eTitle";
            this.eTitle.Size = new System.Drawing.Size(373, 31);
            this.eTitle.TabIndex = 0;
            // 
            // eDurationHour
            // 
            this.eDurationHour.Location = new System.Drawing.Point(382, 653);
            this.eDurationHour.Name = "eDurationHour";
            this.eDurationHour.Size = new System.Drawing.Size(313, 31);
            this.eDurationHour.TabIndex = 2;
            this.eDurationHour.Text = "0";
            this.eDurationHour.TextChanged += new System.EventHandler(this.eventTimeBlockChanged);
            // 
            // eDetails
            // 
            this.eDetails.Location = new System.Drawing.Point(149, 108);
            this.eDetails.Multiline = true;
            this.eDetails.Name = "eDetails";
            this.eDetails.Size = new System.Drawing.Size(651, 212);
            this.eDetails.TabIndex = 9;
            // 
            // eAttendeesList
            // 
            this.eAttendeesList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4});
            this.eAttendeesList.FullRowSelect = true;
            this.eAttendeesList.Location = new System.Drawing.Point(811, 107);
            this.eAttendeesList.Name = "eAttendeesList";
            this.eAttendeesList.Size = new System.Drawing.Size(437, 213);
            this.eAttendeesList.TabIndex = 12;
            this.eAttendeesList.UseCompatibleStateImageBehavior = false;
            this.eAttendeesList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "First Name";
            this.columnHeader3.Width = 142;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Last Name";
            this.columnHeader4.Width = 133;
            // 
            // eNotAttendingList
            // 
            this.eNotAttendingList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.eNotAttendingList.FullRowSelect = true;
            this.eNotAttendingList.Location = new System.Drawing.Point(1444, 107);
            this.eNotAttendingList.Name = "eNotAttendingList";
            this.eNotAttendingList.Size = new System.Drawing.Size(438, 213);
            this.eNotAttendingList.TabIndex = 13;
            this.eNotAttendingList.UseCompatibleStateImageBehavior = false;
            this.eNotAttendingList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "First Name";
            this.columnHeader1.Width = 124;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Last Name";
            this.columnHeader2.Width = 144;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(1283, 680);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(267, 64);
            this.saveButton.TabIndex = 10;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveEvent_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(1615, 680);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(267, 64);
            this.cancelButton.TabIndex = 11;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelEvent);
            // 
            // eAddParticipant
            // 
            this.eAddParticipant.Location = new System.Drawing.Point(1263, 108);
            this.eAddParticipant.Name = "eAddParticipant";
            this.eAddParticipant.Size = new System.Drawing.Size(166, 64);
            this.eAddParticipant.TabIndex = 4;
            this.eAddParticipant.Text = "<- Add";
            this.eAddParticipant.UseVisualStyleBackColor = true;
            this.eAddParticipant.Click += new System.EventHandler(this.toggleParticipant_Click);
            // 
            // eRemoveParticipant
            // 
            this.eRemoveParticipant.Location = new System.Drawing.Point(1263, 243);
            this.eRemoveParticipant.Name = "eRemoveParticipant";
            this.eRemoveParticipant.Size = new System.Drawing.Size(166, 64);
            this.eRemoveParticipant.TabIndex = 5;
            this.eRemoveParticipant.Text = "Remove ->";
            this.eRemoveParticipant.UseVisualStyleBackColor = true;
            this.eRemoveParticipant.Click += new System.EventHandler(this.toggleParticipant_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 25);
            this.label1.TabIndex = 12;
            this.label1.Text = "Title";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 25);
            this.label2.TabIndex = 13;
            this.label2.Text = "Details";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(756, 657);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 25);
            this.label4.TabIndex = 15;
            this.label4.Text = "Room";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(377, 614);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 25);
            this.label5.TabIndex = 16;
            this.label5.Text = "Duration";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(806, 68);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 25);
            this.label6.TabIndex = 17;
            this.label6.Text = "Attendees";
            // 
            // eStartTimeLabel
            // 
            this.eStartTimeLabel.AutoSize = true;
            this.eStartTimeLabel.Location = new System.Drawing.Point(7, 614);
            this.eStartTimeLabel.Name = "eStartTimeLabel";
            this.eStartTimeLabel.Size = new System.Drawing.Size(110, 25);
            this.eStartTimeLabel.TabIndex = 18;
            this.eStartTimeLabel.Text = "Start Time";
            this.eStartTimeLabel.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(71, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 25);
            this.label8.TabIndex = 32;
            this.label8.Text = "Date\r\n";
            // 
            // eDate
            // 
            this.eDate.Location = new System.Drawing.Point(149, 62);
            this.eDate.Name = "eDate";
            this.eDate.Size = new System.Drawing.Size(373, 31);
            this.eDate.TabIndex = 1;
            this.eDate.Value = new System.DateTime(2017, 9, 29, 0, 0, 0, 0);
            this.eDate.ValueChanged += new System.EventHandler(this.eventTimeBlockChanged);
            // 
            // eStartTime
            // 
            this.eStartTime.CustomFormat = "h:mm tt";
            this.eStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.eStartTime.Location = new System.Drawing.Point(12, 651);
            this.eStartTime.Name = "eStartTime";
            this.eStartTime.ShowUpDown = true;
            this.eStartTime.Size = new System.Drawing.Size(199, 31);
            this.eStartTime.TabIndex = 6;
            this.eStartTime.ValueChanged += new System.EventHandler(this.eventTimeBlockChanged);
            // 
            // eDurationMinute
            // 
            this.eDurationMinute.Location = new System.Drawing.Point(382, 697);
            this.eDurationMinute.Name = "eDurationMinute";
            this.eDurationMinute.Size = new System.Drawing.Size(313, 31);
            this.eDurationMinute.TabIndex = 3;
            this.eDurationMinute.Text = "30";
            this.eDurationMinute.TextChanged += new System.EventHandler(this.eventTimeBlockChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(294, 654);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 25);
            this.label9.TabIndex = 38;
            this.label9.Text = "Hours";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(275, 700);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 25);
            this.label10.TabIndex = 39;
            this.label10.Text = "Minutes";
            // 
            // eRoom
            // 
            this.eRoom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.eRoom.FormattingEnabled = true;
            this.eRoom.Location = new System.Drawing.Point(843, 651);
            this.eRoom.Name = "eRoom";
            this.eRoom.Size = new System.Drawing.Size(313, 33);
            this.eRoom.TabIndex = 41;
            // 
            // customTimePicker
            // 
            this.customTimePicker.Location = new System.Drawing.Point(12, 342);
            this.customTimePicker.Name = "customTimePicker";
            this.customTimePicker.Size = new System.Drawing.Size(1870, 229);
            this.customTimePicker.TabIndex = 40;
            this.customTimePicker.Text = "customTimePicker1";
            // 
            // NewEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1922, 829);
            this.Controls.Add(this.eRoom);
            this.Controls.Add(this.customTimePicker);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.eDurationMinute);
            this.Controls.Add(this.eStartTime);
            this.Controls.Add(this.eDate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.eStartTimeLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.eRemoveParticipant);
            this.Controls.Add(this.eAddParticipant);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.eNotAttendingList);
            this.Controls.Add(this.eAttendeesList);
            this.Controls.Add(this.eDetails);
            this.Controls.Add(this.eDurationHour);
            this.Controls.Add(this.eTitle);
            this.Name = "NewEvent";
            this.Text = "New Event";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox eTitle;
        private System.Windows.Forms.TextBox eDurationHour;
        private System.Windows.Forms.TextBox eDetails;
        private System.Windows.Forms.ListView eAttendeesList;
        private System.Windows.Forms.ListView eNotAttendingList;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button eAddParticipant;
        private System.Windows.Forms.Button eRemoveParticipant;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label eStartTimeLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker eDate;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.DateTimePicker eStartTime;
        private System.Windows.Forms.TextBox eDurationMinute;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Forms.CustomTimePicker customTimePicker;
        private System.Windows.Forms.ComboBox eRoom;
    }
}