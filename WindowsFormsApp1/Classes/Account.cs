﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Account
    {
        private String userName;
        private String password;
        private Contact myContact;

        public Account(String userName, String password)
        {
            this.userName = userName;
            this.password = password;  
        }

        public void setUserName(String name)
        {
            userName = name;
        }

        public void setPassword (String pass)
        {
            password = pass;
        }

        public String getUserName()
        {
            return userName;
        }

        public String getPassword()
        {
            return password;
        }

        public Contact getMyContact()
        {
            return myContact;
        }

        public void setContact(Contact c)
        {
            myContact = c;
        }
    }
}
