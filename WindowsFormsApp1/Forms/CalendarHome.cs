﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class CalendarHome : Form
    {
        public CalendarHome()
        {
            InitializeComponent();
            printSelectedDaysEvents(calendar.SelectionRange.Start);
        }

        private void newEventButton_Click(object sender, EventArgs e)
        {
            Controller.openForm(new NewEvent(calendar.SelectionRange.Start.Date), this);
        }

        private void printSelectedDaysEvents(DateTime selectedDay)
        {
            eventList.Items.Clear();
            selectedDay = selectedDay.Date;
            Contact activeContact = Controller.loggedInAccount.getMyContact();
            List<Event> eventsToPrint = activeContact.getEventsOn(selectedDay);

            foreach(Event e in eventsToPrint)
            {
                addEventToList(e);
            }
        }

        private void addEventToList(Event e)
        {
            ListViewItem li = new ListViewItem();
            li.Tag = e;
            li.Text = e.getTitle();
            li.SubItems.Add(e.getTimeBlock().getStart().ToString("h:mm tt"));
            eventList.Items.Add(li);
        }

        private void dateChanged(object sender, DateRangeEventArgs e)
        {
            printSelectedDaysEvents(calendar.SelectionRange.Start);
        }

        private void printEventList(object sender, EventArgs e)
        {
            printSelectedDaysEvents(calendar.SelectionRange.Start);
        }

        private void selectEvent(object sender, MouseEventArgs e)
        {
            Controller.openForm(new NewEvent(((Event)eventList.SelectedItems[0].Tag)), this);
        }

        private void viewMonthlyEvents(object sender, EventArgs e)
        {
            Controller.openForm(new MonthlyView(calendar.SelectionRange.Start), this);
        }
    }
}
