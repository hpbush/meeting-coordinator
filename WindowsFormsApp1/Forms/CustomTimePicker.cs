﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1.Forms
{
    public partial class CustomTimePicker : Control
    {
        private List<TimeBlock> unavailableTimes;
        private TimeBlock desiredTimeBlock;
        private bool timeAvailable;

        public CustomTimePicker()
        {
            InitializeComponent();
            unavailableTimes = new List<TimeBlock>();
            desiredTimeBlock = null;
            timeAvailable = true;
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            drawBackground(pe);

            timeAvailable = true;
            foreach(TimeBlock tb in unavailableTimes)
            {
                drawTimeBlock(tb, pe, Color.Black, 6);
                if (desiredTimeBlock != null && timeAvailable)
                    timeAvailable = !tb.checkConflict(desiredTimeBlock);
            }

            if(desiredTimeBlock != null)
                if( timeAvailable )
                    drawTimeBlock(desiredTimeBlock, pe, Color.Green, 2);
                else
                    drawTimeBlock(desiredTimeBlock, pe, Color.Red, 2);
        }

        public void drawBackground(PaintEventArgs pe)
        {
            Graphics gfx = pe.Graphics;
            Rectangle rect = ClientRectangle;
            Font myFont = new Font("calibri", 11);
            StringFormat stringFormat = new StringFormat();
            stringFormat.Alignment = StringAlignment.Near;
            Pen myPen = new Pen(Color.DarkGray);
            int textOffSet = -20;

            rect.Width -= 1; //shrink height and width so border is drawn
            rect.Height -= 1;

            gfx.DrawRectangle(myPen, rect);

            int yTop = rect.Y;
            int rectBottom = rect.Y + rect.Height;
            double x = rect.X;
            double offset = rect.Width / 24.0;

            for (int i = 0; i < 24; i++)
            {
                Point p1, p2;
                int yBottom = rectBottom;
                if(i%2 != 0)
                {
                    yBottom -= 15;
                    String text = i%12 + ":00";
                    if (i / 12 > 0)
                        text += "PM";
                    else
                        text += "AM";
                    gfx.DrawString(text, myFont, new SolidBrush(Color.Black), new PointF((float)x+textOffSet, (float)yBottom));
                }

                p1 = new Point((int)x, yTop);
                p2 = new Point((int)x, yBottom);
                x += offset;

                gfx.DrawLine(myPen, p1, p2);
            }
        }

        private void drawTimeBlock(TimeBlock tb, PaintEventArgs pe, Color c, int vertOffSet)
        {
            Graphics gfx = pe.Graphics;
            Rectangle rect = ClientRectangle;
            DateTime start = tb.getStart();
            DateTime end = tb.getEnd();

            int startHr = start.Hour;
            double startMinInHrs = start.Minute / 60.0;
            double startTime = startHr + startMinInHrs;

            int endHr = end.Hour;
            double endMinInHrs = end.Minute / 60.0;
            double endTime = endHr + endMinInHrs;

            double offset = rect.Width / 24.0;

            double xStart = rect.X + startTime * offset;
            double xEnd = rect.X + endTime * offset;
            double rWidth = xEnd - xStart;
            double rHeight = 40;
            int y = rect.Y + (rect.Height / vertOffSet);

            Rectangle r = new Rectangle((int)xStart, y, (int)rWidth, (int)rHeight);
            gfx.FillRectangle(new SolidBrush(c), r);
        }

        public List<TimeBlock> getUnavailableTimes()
        {
            return unavailableTimes;
        }

        public void setUnavailableTimes(List<TimeBlock> times)
        {
            unavailableTimes = times;
            Invalidate();
        }

        public TimeBlock getDesiredTimeBlock()
        {
            return desiredTimeBlock;
        }

        public void setDesiredTimeBlock(TimeBlock tb)
        {
            desiredTimeBlock = tb;
            Invalidate();
        }

        public bool selectedTimeIsValid()
        {
            return timeAvailable;
        }
    }
}
