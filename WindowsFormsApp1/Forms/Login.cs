﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            passwordField.UseSystemPasswordChar = true;
        }

        private void registerButton_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Controller.openForm(new Register(), this);
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            String username = usernameField.Text;
            String password = passwordField.Text;

            if (Controller.logUserIn(username, password))
            {
                usernameField.Text = null;
                passwordField.Text = null;

                Controller.openForm(new CalendarHome(), this);
            }
            else
            {
                MessageBox.Show("Invalid username or password");
            }
        }

        private void showPasswordToggle(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                passwordField.UseSystemPasswordChar = false;
            else
                passwordField.UseSystemPasswordChar = true;
        }
    }
}
