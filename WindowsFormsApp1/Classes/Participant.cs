﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Participant
    {
        private List<Event> calendar;

        public Participant()
        {
            calendar = new List<Event>(); 
        }

        public List<Event> getCalendar()
        {
            return calendar;
        }

        public void setEventInCalendar(Event eventToAdd)
        {
            calendar.Add(eventToAdd);
        }

        public List<Event> getEventsOn(DateTime d)
        {
            List<Event> eList = new List<Event>();
            foreach(Event e in calendar)
            {
                if(e.getTimeBlock().getStart().Date == d)
                {
                    eList.Add(e);
                }
            }
            return eList;
        }

        public List<Event> getEventsIn(DateTime month)
        {
            List<Event> eList = new List<Event>();
            foreach (Event e in calendar)
            {
                if (e.getTimeBlock().getStart().Month == month.Month)
                {
                    eList.Add(e);
                }
            }
            return eList;
        }
    }
}
