﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class NewEvent : Form
    {
        private Event myNewEvent;
        private bool eventEditor;
        private List<Contact> removedContacts;
        private List<Contact> addedContacts;

        private List<TimeBlock> unavailableTimes;

        public NewEvent(DateTime initialDate)
        {
            InitializeComponent();
            myNewEvent = new Event();
            eventEditor = false;
            unavailableTimes = new List<TimeBlock>();
            buildBothContactLists();
            eDate.Value = initialDate;
        }

        public NewEvent(Event eventEditor)
        {
            InitializeComponent();
            this.Text = "Edit "+ eventEditor.getTitle();
            myNewEvent = eventEditor;
            TimeBlock tb = eventEditor.getTimeBlock();
            removedContacts = new List<Contact>();
            addedContacts = new List<Contact>();
            unavailableTimes = new List<TimeBlock>();
            buildBothContactLists();
            eDate.Value = tb.getStart().Date;
            eStartTime.Value = new DateTime(tb.getStart().Ticks);
            eTitle.Text = eventEditor.getTitle();
            eDetails.Text = eventEditor.getDetails();
            this.eventEditor = true;
            TimeSpan ts = tb.getEnd().Subtract(tb.getStart());
            eDurationHour.Text = ts.Hours.ToString();
            eDurationMinute.Text = ts.Minutes.ToString();
            eRoom.SelectedIndex = eRoom.Items.IndexOf(Controller.getRoomWithID(eventEditor.getRoomID()));
        }

        private void addContactToList(ListView list, Contact c)
        {
            ListViewItem li = new ListViewItem();
            li.Tag = c;
            li.Text = c.getFirstName();
            li.SubItems.Add(c.getLastname());
            list.Items.Add(li);

            Console.WriteLine(Controller.getAllRooms());
        }

        private void buildUnatendingContactList()
        {
            eNotAttendingList.Items.Clear();
            foreach (Contact globalContact in Controller.getAllContacts())
            {
                foreach (Contact eventContact in myNewEvent.getMyParticipants())
                    if(globalContact == eventContact)
                        goto next;

                addContactToList(eNotAttendingList, globalContact);
                next:;
            }
        }

        private void buildAttendingContactList()
        {
            eAttendeesList.Items.Clear();
            foreach( Contact c in myNewEvent.getMyParticipants())
            {
                addContactToList(eAttendeesList, c);
            }
        }

        private void buildBothContactLists()
        {
            buildAttendingContactList();
            buildUnatendingContactList();
        }

        private void toggleParticipant_Click(object sender, EventArgs e)
        {
            ListView addTo = eNotAttendingList;
            ListView removeFrom = eAttendeesList;

            if (sender == eAddParticipant)
            {
                addTo = eAttendeesList;
                removeFrom = eNotAttendingList;
            }
            

            foreach (ListViewItem item in removeFrom.SelectedItems)
            {
                if(sender == eRemoveParticipant && eventEditor)
                {
                    removedContacts.Add(((Contact) item.Tag));
                    addedContacts.Remove(((Contact)item.Tag));
                }else if (sender == eAddParticipant && eventEditor)
                {
                    removedContacts.Remove(((Contact)item.Tag));
                    addedContacts.Add(((Contact)item.Tag));
                }

                removeFrom.Items.Remove(item);
                addTo.Items.Add(item);
            }
            generateUnavailableTimes();
            customTimePicker.setUnavailableTimes(unavailableTimes);
        }

        private void generateUnavailableTimes()
        {
            unavailableTimes.Clear();
            List<Contact> attendingContacts = new List<Contact>();
            foreach(ListViewItem item in eAttendeesList.Items)
            {
                attendingContacts.Add(((Contact)item.Tag));
            }
            foreach (Contact activeContact in attendingContacts)
            {
                foreach( Event activeEvent in activeContact.getEventsOn(eDate.Value )) {
                    List<TimeBlock> conflictTrail = new List<TimeBlock>();
                    foreach(TimeBlock tb in unavailableTimes)
                        if ( tb.checkConflict(activeEvent.getTimeBlock()) && activeEvent != myNewEvent)
                        {
                            conflictTrail.Add(tb);
                            conflictTrail.Add(activeEvent.getTimeBlock());
                        }
                    if (conflictTrail.Count == 0 && activeEvent != myNewEvent)
                        unavailableTimes.Add(activeEvent.getTimeBlock());
                    else if(conflictTrail.Count > 0)
                        resolveConflictTrail(conflictTrail);
                }
            }
            //unavailableTimes.Sort((x, y) => x.getStart().CompareTo(y.getStart()));
        }

        private void resolveConflictTrail(List<TimeBlock> conflictTrail)
        {
            DateTime start = conflictTrail[0].getStart();
            DateTime end = conflictTrail[0].getEnd();
            foreach (TimeBlock tb in conflictTrail)
            {
                if (tb.getStart() < start)
                    start = tb.getStart();
                if (tb.getEnd() > end)
                    end = tb.getEnd();
                unavailableTimes.Remove(tb);
            }
            TimeBlock resolvedTimeBlock = TimeBlock.createTimeBlock(start, end);
            if (resolvedTimeBlock != null)
                unavailableTimes.Add(resolvedTimeBlock);
            else
                Console.WriteLine("Strange error");
        }

        private int getDurationMin()
        {
            int myDurationMin = 0;
            if (Int32.TryParse(eDurationMinute.Text, out myDurationMin))
            {

            }
            return myDurationMin;
        }

        private int getDurationHours()
        {
            int myDurationHours;
            if (Int32.TryParse(eDurationHour.Text, out myDurationHours))
            {

            }
            return myDurationHours;
        }

        private bool validDuration()
        {
            if (eDurationHour.Text != "" && eDurationMinute.Text != "")
                if (getDurationHours() > 0 || getDurationMin() > 0)
                {
                    return true;
                }
            return false;
        }

        private void eventTimeBlockChanged(object sender, EventArgs e)
        {
            generateUnavailableTimes();
            customTimePicker.setUnavailableTimes(unavailableTimes);
            DateTime startTime = new DateTime(eDate.Value.Date.Ticks);
            startTime = startTime.AddHours(eStartTime.Value.Hour).AddMinutes(eStartTime.Value.Minute);
            DateTime endTime = new DateTime(startTime.Ticks);
            endTime = endTime.AddHours(getDurationHours()).AddMinutes(getDurationMin());
            TimeBlock desiredTimeBlock = TimeBlock.createTimeBlock(startTime, endTime);
            if(desiredTimeBlock != null)
            {
                customTimePicker.setDesiredTimeBlock(desiredTimeBlock);
                generateAvailableRoomList(desiredTimeBlock);
            }
        }

        private void generateAvailableRoomList(TimeBlock desiredTimeBlock)
        {
            Room previouslySelectedRoom = (Room)eRoom.SelectedItem;
            eRoom.Items.Clear();
            foreach (Room r in Controller.getAllRooms())
            {
                List<Event> rEvents = r.getEventsOn(desiredTimeBlock.getStart().Date);
                if (rEvents.Count == 0)
                    eRoom.Items.Add(r);
                else
                {
                    bool addToList = true;
                    foreach (Event ev in rEvents)
                    {
                        if (ev.getTimeBlock().checkConflict(desiredTimeBlock) && ev != myNewEvent)
                        {
                            addToList = false;
                            break;
                        }
                    }
                    if (addToList)
                        eRoom.Items.Add(r);
                }
            }
            if (previouslySelectedRoom != null)
                eRoom.SelectedItem = previouslySelectedRoom;
        }

        private void saveEvent_Click(object sender, EventArgs e)
        {
            if (customTimePicker.selectedTimeIsValid() && eRoom.SelectedItem != null)
            {
                Room eventRoom = ((Room)eRoom.SelectedItem);
                myNewEvent.setEventTimeBlock(customTimePicker.getDesiredTimeBlock());
                myNewEvent.setDetails(eDetails.Text);
                myNewEvent.setTitle(eTitle.Text);
               
                if (eventEditor)
                {
                    Room oldRoom = Controller.getRoomWithID(myNewEvent.getRoomID());
                    oldRoom.getCalendar().Remove(myNewEvent);

                    foreach(Contact c in removedContacts)
                    {
                        c.getCalendar().Remove(myNewEvent);
                        myNewEvent.getMyParticipants().Remove(c);
                    }

                    foreach(Contact c in addedContacts)
                    {
                        c.setEventInCalendar(myNewEvent);
                        myNewEvent.addParticipant(c);
                    }
                }
                else
                {
                    foreach (ListViewItem item in eAttendeesList.Items)
                    {
                        Contact c = (Contact)item.Tag;
                        c.setEventInCalendar(myNewEvent);
                        myNewEvent.addParticipant(c);
                    }
                    Controller.addEvent(myNewEvent);
                }
                myNewEvent.setRoomID(eventRoom.getroomID());
                eventRoom.setEventInCalendar(myNewEvent);
                
                Controller.saveEventToDB(myNewEvent, eventEditor);
                this.Close();
            }
            else
            {
                MessageBox.Show("Invalid event time");
            }
        }

        private void cancelEvent(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
