﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Login());
            
        }

        public static void createContacts()
        {
            for ( int i = 0; i < 5; i++)
            {
                Contact c = new Contact("fname" + i, "lname" + i, "email" + i, "phoneNumber" + i);
                Controller.addContact(c);
            }
        }   
    }
}
